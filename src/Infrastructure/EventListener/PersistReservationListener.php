<?php

namespace Infrastructure\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Domain\Reservation\Entity\Reservation;
use Domain\Show\Service\BindTakenSeatsToShowServiceInterface;

/**
 * Class PersistReservationListener
 * @package Infrastructure\EventListener
 */
final class PersistReservationListener
{
    /**
     * @var BindTakenSeatsToShowServiceInterface
     */
    private $bindTakenSeatsToShowService;

    /**
     * PersistReservationListener constructor.
     * @param BindTakenSeatsToShowServiceInterface $bindTakenSeatsToShowService
     */
    public function __construct(BindTakenSeatsToShowServiceInterface $bindTakenSeatsToShowService)
    {
        $this->bindTakenSeatsToShowService = $bindTakenSeatsToShowService;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args): void
    {
        $reservation = $args->getObject();

        if ($reservation instanceof Reservation) {
            $this->bindTakenSeatsToShowService->bind($reservation->getShow(), $reservation->getTime());
        }
    }
}
