<?php

namespace Infrastructure\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Domain\Reservation\Entity\Reservation;
use Domain\Show\Service\BindTakenSeatsToShowServiceInterface;

/**
 * Class LoadReservationListener
 * @package Infrastructure\EventListener
 */
final class LoadReservationListener
{
    /**
     * @var BindTakenSeatsToShowServiceInterface
     */
    private $bindTakenSeatsToShowService;

    /**
     * LoadReservationListener constructor.
     * @param BindTakenSeatsToShowServiceInterface $bindTakenSeatsToShowService
     */
    public function __construct(BindTakenSeatsToShowServiceInterface $bindTakenSeatsToShowService)
    {
        $this->bindTakenSeatsToShowService = $bindTakenSeatsToShowService;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postLoad(LifecycleEventArgs $args): void
    {
        $reservation = $args->getObject();

        if ($reservation instanceof Reservation) {
            $this->bindTakenSeatsToShowService->bind($reservation->getShow(), $reservation->getTime());
        }
    }
}
