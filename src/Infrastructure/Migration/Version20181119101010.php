<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181119101010
 * @package DoctrineMigrations
 */
final class Version20181119101010 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('
            INSERT INTO place_room(id, name, place_id, seats_rows, seats_columns) 
            VALUES(1, "Arizona", 1, 10, 10)
        ');

        $this->addSql('
            INSERT INTO place_room(id, name, place_id, seats_rows, seats_columns) 
            VALUES(2, "New York", 1, 11, 11)
        ');

        $this->addSql('
            INSERT INTO place_room(id, name, place_id, seats_rows, seats_columns) 
            VALUES(3, "Los Angeles", 1, 8, 8)
        ');

        $this->addSql('
            INSERT INTO place_room(id, name, place_id, seats_rows, seats_columns) 
            VALUES(4, "Utah", 1, 16, 16)
        ');

        $this->addSql('
            INSERT INTO place_room(id, name, place_id, seats_rows, seats_columns) 
            VALUES(5, "Frankfurt", 2, 14, 14)
        ');

        $this->addSql('
            INSERT INTO place_room(id, name, place_id, seats_rows, seats_columns) 
            VALUES(6, "Berlin", 2, 15, 15)
        ');

        $this->addSql('
            INSERT INTO place_room(id, name, place_id, seats_rows, seats_columns) 
            VALUES(7, "Bali", 3, 6, 6)
        ');

        $this->addSql('
            INSERT INTO place_room(id, name, place_id, seats_rows, seats_columns) 
            VALUES(8, "Roma", 3, 13, 13)
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
