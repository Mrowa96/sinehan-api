<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181117101010
 * @package DoctrineMigrations
 */
final class Version20181117101010 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('
            INSERT INTO ticket(id, name, price) 
            VALUES(1, "Standard", 19.99)
        ');

        $this->addSql('
            INSERT INTO ticket(id, name, price) 
            VALUES(2, "Students", 16.99)
        ');

        $this->addSql('
            INSERT INTO ticket(id, name, price) 
            VALUES(3, "Retirees", 14.99)
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
