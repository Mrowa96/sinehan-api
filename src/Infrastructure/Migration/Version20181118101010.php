<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181118101010
 * @package DoctrineMigrations
 */
final class Version20181118101010 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('
            INSERT INTO place(id, name, latitude, longitude) 
            VALUES(1, "Cracow - Bonarka", 50.027790, 19.950097)
        ');

        $this->addSql('
            INSERT INTO place(id, name, latitude, longitude) 
            VALUES(2, "Cracow - Zakopianka", 50.016375, 19.931240)
        ');

        $this->addSql('
            INSERT INTO place(id, name, latitude, longitude) 
            VALUES(3, "Warsaw - Złote Tarasy", 52.229948, 21.002068)
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
