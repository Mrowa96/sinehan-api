<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181115201522
 * @package DoctrineMigrations
 */
final class Version20181115201522 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('
            INSERT INTO news_category(id, name) 
            VALUES(1, "Movies")
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
