<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181121101010
 * @package DoctrineMigrations
 */
final class Version20181121101010 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-19", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-20", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-21", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-22", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-23", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-24", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-25", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-26", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-27", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-28", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-29", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 1, "2018-11-30", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 2, "2018-12-01", "9:00,11:00,13:00,15:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 3, "2018-12-02", "9:00,11:00,13:00,15:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 4, "2018-12-03", "9:00,11:00,13:00,15:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(1, 1, 5, "2018-12-04", "9:00,11:00,13:00,15:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(2, 2, 5, "2018-11-21", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(2, 2, 5, "2018-11-22", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(2, 2, 5, "2018-11-23", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(2, 2, 5, "2018-11-24", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(2, 2, 5, "2018-11-25", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(2, 2, 6, "2018-11-26", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(2, 2, 6, "2018-11-27", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(2, 2, 6, "2018-11-28", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(2, 2, 6, "2018-11-29", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(2, 2, 6, "2018-11-30", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(5, 3, 7, "2018-11-21", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(5, 3, 7, "2018-11-22", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(5, 3, 7, "2018-11-23", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(5, 3, 7, "2018-11-24", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(5, 3, 7, "2018-11-25", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(5, 3, 8, "2018-11-26", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(5, 3, 8, "2018-11-27", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(5, 3, 8, "2018-11-28", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(5, 3, 8, "2018-11-29", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');

        $this->addSql('
            INSERT INTO shows(event_id, place_id, room_id, date, start_times) 
            VALUES(5, 3, 8, "2018-11-30", "9:00,11:00,13:00,15:00,17:00,19:00,21:00")');
    }

    public function down(Schema $schema): void
    {
    }
}
