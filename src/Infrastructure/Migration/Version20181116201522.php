<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181116201522
 * @package DoctrineMigrations
 */
final class Version20181116201522 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        // @codingStandardsIgnoreStart
        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Richard Linklater Directing Biopic on Comedian Bill Hicks for Focus", 
                "/images/news_1.jpg", 
                "Richard Linklater will write and direct an untitled film based on the life of comedian Bill Hicks for Focus Features.<br><br>Hicks was raised as a Southern Baptist and began performing comedy in the late 1970s in Texas. He toured extensively during the 1980s, specializing in dark humor and social commentary. A typical joke: \'I never got along with my dad. Kids used to come up to me and say, \'My dad can beat up your dad.\' I’d say \'Yeah? When?\'\'<br><br>Hicks died of pancreatic cancer in 1994 at the age of 32. Linklater has said publicly that he regretted that he was not able to work with Hicks, who was a fan of Linklater’s Texas-based coming-of-age movie <i>Dazed and Confused</i>.", 
                "2018-11-19 14:23", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Leonardo DiCaprio and Martin Scorsese to Team up Again for ‘Killers of the Flower Moon’",
                "/images/news_2.jpg", 
                "<p>Leonardo DiCaprio and Martin Scorsese are teaming up again, to star in and direct respectively, for Imperative Entertainment’s “Killers of the Flower Moon,” which is based on the New York Times best-selling book by David Grann, individuals with knowledge of the project told TheWrap.</p><p>Eric Roth wrote the screenplay. Imperative Entertainment acquired the film rights to the book in 2016. Scorsese will produce alongside Imperative Entertainment’s Dan Friedkin, Bradley Thomas, Sikelia Productions’ Emma Tillinger Koskoff, and Appian Way Productions.</p><p>Set in 1920s Oklahoma, the Osage Nation discover oil under their land only to find themselves being murdered one by one. As the death toll rises, the newly created FBI takes up the case and unravels a chilling conspiracy and one of the most monstrous crimes in American history.</p><p>“When I read David Grann’s book, I immediately started seeing it–the people, the settings, the action–and I knew that I had to make it into a movie,” said Scorsese in a statement. “I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen.”</p><p>“I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen,” the director added.</p><p>30WEST will arrange the financing and distribution for the project.</p>",
                "2018-10-18 12:56", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "James Karen, Actor in \'Poltergeist\' and So Much More, Dies at 94",
                "/images/news_3.jpg", 
                "<p>James Karen, the instantly recognizable character actor who moved the cemetery’s headstones — but not the bodies — as the developer Mr. Teague in the modern horror classic <em>Poltergeist</em>, has died. He was 94.</p><p>The incredibly prolific Karen, who also was noteworthy in such films as <em>The China Syndrome</em> (1979) and <em>The Return of the Living Dead </em>(1985) and on the finale of NBC’s <em>Little House</em> <em>on the Prairie</em> — he’s the dastardly reason the town of Walnut Grove was blown up — died Tuesday at his Los Angeles home, his wife, Alba, said.</p><p>Karen, who usually portrayed authority figures, good and bad, during his seven-decade career, also appeared in three films for director Oliver Stone: as Lynch, the office manager at Jackson Steinem, where Charlie Sheen’s stockbroker character works, in <em>Wall Street</em> (1987); as Secretary of State William Rogers in <em>Nixon</em> (1995); and as a Miami Sharks executive in <em>Any Given Sunday</em> (1999).</p><p>The genial Karen studied under legendary acting teacher Sanford Meisner at the Neighborhood Playhouse in New York and made his Broadway debut in Elia Kazan’s original 1947 production of <em>A Streetcar Named Desire</em>, where he was the understudy to Karl Malden and worked alongside Marlon Brando.</p><p>He has a whopping 204 acting credits&nbsp;listed on IMDb. “People don’t know my name, but they know my face because I’ve done so damn much work,” Karen once said.</p><p>That work includes originating the role of rich attorney Lincoln Tyler in 1970 on the ABC soap opera <em>All My Children</em> and appearing in thousands of commercials as the TV and radio spokesman (“Why pay more?”) for Pathmark, an East Coast supermarket chain that was liquidated in 2015.</p><p>Karen was married to the late Susan Reed, the renowned folk singer and zither player, from 1958 until their divorce in 1967, and the godfather of their son, Reed, was the one and only Buster Keaton.</p><p>He married Alba in 1986.</p><p>Karen was born Jacob Karnofsky on Nov. 28, 1923, in Wilkes-Barre, Pennsylvania, the son of Russian-born Jewish immigrants. He was taken to the movies to read the titles on the silent films for his father, an illiterate coal miner.</p><p>As he walked past the Little Theatre in town on his way home from Union Street School, Karen was spotted by future U.S. Rep. Dan Flood, who asked him if he were a Boy Scout. When he said he was, Karen was given a small role in a comedy at the theater.</p><p>“That’s when I began working,” Karen recalled in a 2013. “It was terrific for me. It gave me a real reason to exist, and to live. I was doing exactly what I wanted to do.”</p><p>He stayed with the theater for several plays, joined the Air Force during World War II and then went to New York, landing with the Neighborhood Playhouse in 1939. He went on to appear in the original Broadway productions of <em>Who’s Afraid of Virginia Woolf?</em> and <em>Cactus Flower.</em></p><p>Karen met Keaton in 1956, and the following year they performed in a touring production of George S. Kaufman and Marc Connelly’s comic play <em>Merton of the Movies</em>. They paired again for <em>Film</em> (1965), a 20-minute, virtually silent picture written by Nobel Prize-winning playwright Samuel Beckett (<em>Waiting for Godot</em>). Keaton died in 1966.</p>",
                "2018-10-17 16:12", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "James Karen, Actor in \'Poltergeist\' and So Much More, Dies at 94",
                "/images/news_3.jpg", 
                "<p>James Karen, the instantly recognizable character actor who moved the cemetery’s headstones — but not the bodies — as the developer Mr. Teague in the modern horror classic <em>Poltergeist</em>, has died. He was 94.</p><p>The incredibly prolific Karen, who also was noteworthy in such films as <em>The China Syndrome</em> (1979) and <em>The Return of the Living Dead </em>(1985) and on the finale of NBC’s <em>Little House</em> <em>on the Prairie</em> — he’s the dastardly reason the town of Walnut Grove was blown up — died Tuesday at his Los Angeles home, his wife, Alba, said.</p><p>Karen, who usually portrayed authority figures, good and bad, during his seven-decade career, also appeared in three films for director Oliver Stone: as Lynch, the office manager at Jackson Steinem, where Charlie Sheen’s stockbroker character works, in <em>Wall Street</em> (1987); as Secretary of State William Rogers in <em>Nixon</em> (1995); and as a Miami Sharks executive in <em>Any Given Sunday</em> (1999).</p><p>The genial Karen studied under legendary acting teacher Sanford Meisner at the Neighborhood Playhouse in New York and made his Broadway debut in Elia Kazan’s original 1947 production of <em>A Streetcar Named Desire</em>, where he was the understudy to Karl Malden and worked alongside Marlon Brando.</p><p>He has a whopping 204 acting credits&nbsp;listed on IMDb. “People don’t know my name, but they know my face because I’ve done so damn much work,” Karen once said.</p><p>That work includes originating the role of rich attorney Lincoln Tyler in 1970 on the ABC soap opera <em>All My Children</em> and appearing in thousands of commercials as the TV and radio spokesman (“Why pay more?”) for Pathmark, an East Coast supermarket chain that was liquidated in 2015.</p><p>Karen was married to the late Susan Reed, the renowned folk singer and zither player, from 1958 until their divorce in 1967, and the godfather of their son, Reed, was the one and only Buster Keaton.</p><p>He married Alba in 1986.</p><p>Karen was born Jacob Karnofsky on Nov. 28, 1923, in Wilkes-Barre, Pennsylvania, the son of Russian-born Jewish immigrants. He was taken to the movies to read the titles on the silent films for his father, an illiterate coal miner.</p><p>As he walked past the Little Theatre in town on his way home from Union Street School, Karen was spotted by future U.S. Rep. Dan Flood, who asked him if he were a Boy Scout. When he said he was, Karen was given a small role in a comedy at the theater.</p><p>“That’s when I began working,” Karen recalled in a 2013. “It was terrific for me. It gave me a real reason to exist, and to live. I was doing exactly what I wanted to do.”</p><p>He stayed with the theater for several plays, joined the Air Force during World War II and then went to New York, landing with the Neighborhood Playhouse in 1939. He went on to appear in the original Broadway productions of <em>Who’s Afraid of Virginia Woolf?</em> and <em>Cactus Flower.</em></p><p>Karen met Keaton in 1956, and the following year they performed in a touring production of George S. Kaufman and Marc Connelly’s comic play <em>Merton of the Movies</em>. They paired again for <em>Film</em> (1965), a 20-minute, virtually silent picture written by Nobel Prize-winning playwright Samuel Beckett (<em>Waiting for Godot</em>). Keaton died in 1966.</p>",
                "2018-10-17 16:12", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Leonardo DiCaprio and Martin Scorsese to Team up Again for ‘Killers of the Flower Moon’",
                "/images/news_2.jpg", 
                "<p>Leonardo DiCaprio and Martin Scorsese are teaming up again, to star in and direct respectively, for Imperative Entertainment’s “Killers of the Flower Moon,” which is based on the New York Times best-selling book by David Grann, individuals with knowledge of the project told TheWrap.</p><p>Eric Roth wrote the screenplay. Imperative Entertainment acquired the film rights to the book in 2016. Scorsese will produce alongside Imperative Entertainment’s Dan Friedkin, Bradley Thomas, Sikelia Productions’ Emma Tillinger Koskoff, and Appian Way Productions.</p><p>Set in 1920s Oklahoma, the Osage Nation discover oil under their land only to find themselves being murdered one by one. As the death toll rises, the newly created FBI takes up the case and unravels a chilling conspiracy and one of the most monstrous crimes in American history.</p><p>“When I read David Grann’s book, I immediately started seeing it–the people, the settings, the action–and I knew that I had to make it into a movie,” said Scorsese in a statement. “I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen.”</p><p>“I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen,” the director added.</p><p>30WEST will arrange the financing and distribution for the project.</p>",
                "2018-10-18 12:56", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Richard Linklater Directing Biopic on Comedian Bill Hicks for Focus", 
                "/images/news_1.jpg", 
                "Richard Linklater will write and direct an untitled film based on the life of comedian Bill Hicks for Focus Features.<br><br>Hicks was raised as a Southern Baptist and began performing comedy in the late 1970s in Texas. He toured extensively during the 1980s, specializing in dark humor and social commentary. A typical joke: \'I never got along with my dad. Kids used to come up to me and say, \'My dad can beat up your dad.\' I’d say \'Yeah? When?\'\'<br><br>Hicks died of pancreatic cancer in 1994 at the age of 32. Linklater has said publicly that he regretted that he was not able to work with Hicks, who was a fan of Linklater’s Texas-based coming-of-age movie <i>Dazed and Confused</i>.", 
                "2018-11-19 14:23", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Richard Linklater Directing Biopic on Comedian Bill Hicks for Focus", 
                "/images/news_1.jpg", 
                "Richard Linklater will write and direct an untitled film based on the life of comedian Bill Hicks for Focus Features.<br><br>Hicks was raised as a Southern Baptist and began performing comedy in the late 1970s in Texas. He toured extensively during the 1980s, specializing in dark humor and social commentary. A typical joke: \'I never got along with my dad. Kids used to come up to me and say, \'My dad can beat up your dad.\' I’d say \'Yeah? When?\'\'<br><br>Hicks died of pancreatic cancer in 1994 at the age of 32. Linklater has said publicly that he regretted that he was not able to work with Hicks, who was a fan of Linklater’s Texas-based coming-of-age movie <i>Dazed and Confused</i>.", 
                "2018-11-19 14:23", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Leonardo DiCaprio and Martin Scorsese to Team up Again for ‘Killers of the Flower Moon’",
                "/images/news_2.jpg", 
                "<p>Leonardo DiCaprio and Martin Scorsese are teaming up again, to star in and direct respectively, for Imperative Entertainment’s “Killers of the Flower Moon,” which is based on the New York Times best-selling book by David Grann, individuals with knowledge of the project told TheWrap.</p><p>Eric Roth wrote the screenplay. Imperative Entertainment acquired the film rights to the book in 2016. Scorsese will produce alongside Imperative Entertainment’s Dan Friedkin, Bradley Thomas, Sikelia Productions’ Emma Tillinger Koskoff, and Appian Way Productions.</p><p>Set in 1920s Oklahoma, the Osage Nation discover oil under their land only to find themselves being murdered one by one. As the death toll rises, the newly created FBI takes up the case and unravels a chilling conspiracy and one of the most monstrous crimes in American history.</p><p>“When I read David Grann’s book, I immediately started seeing it–the people, the settings, the action–and I knew that I had to make it into a movie,” said Scorsese in a statement. “I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen.”</p><p>“I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen,” the director added.</p><p>30WEST will arrange the financing and distribution for the project.</p>",
                "2018-10-18 12:56", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "James Karen, Actor in \'Poltergeist\' and So Much More, Dies at 94",
                "/images/news_3.jpg", 
                "<p>James Karen, the instantly recognizable character actor who moved the cemetery’s headstones — but not the bodies — as the developer Mr. Teague in the modern horror classic <em>Poltergeist</em>, has died. He was 94.</p><p>The incredibly prolific Karen, who also was noteworthy in such films as <em>The China Syndrome</em> (1979) and <em>The Return of the Living Dead </em>(1985) and on the finale of NBC’s <em>Little House</em> <em>on the Prairie</em> — he’s the dastardly reason the town of Walnut Grove was blown up — died Tuesday at his Los Angeles home, his wife, Alba, said.</p><p>Karen, who usually portrayed authority figures, good and bad, during his seven-decade career, also appeared in three films for director Oliver Stone: as Lynch, the office manager at Jackson Steinem, where Charlie Sheen’s stockbroker character works, in <em>Wall Street</em> (1987); as Secretary of State William Rogers in <em>Nixon</em> (1995); and as a Miami Sharks executive in <em>Any Given Sunday</em> (1999).</p><p>The genial Karen studied under legendary acting teacher Sanford Meisner at the Neighborhood Playhouse in New York and made his Broadway debut in Elia Kazan’s original 1947 production of <em>A Streetcar Named Desire</em>, where he was the understudy to Karl Malden and worked alongside Marlon Brando.</p><p>He has a whopping 204 acting credits&nbsp;listed on IMDb. “People don’t know my name, but they know my face because I’ve done so damn much work,” Karen once said.</p><p>That work includes originating the role of rich attorney Lincoln Tyler in 1970 on the ABC soap opera <em>All My Children</em> and appearing in thousands of commercials as the TV and radio spokesman (“Why pay more?”) for Pathmark, an East Coast supermarket chain that was liquidated in 2015.</p><p>Karen was married to the late Susan Reed, the renowned folk singer and zither player, from 1958 until their divorce in 1967, and the godfather of their son, Reed, was the one and only Buster Keaton.</p><p>He married Alba in 1986.</p><p>Karen was born Jacob Karnofsky on Nov. 28, 1923, in Wilkes-Barre, Pennsylvania, the son of Russian-born Jewish immigrants. He was taken to the movies to read the titles on the silent films for his father, an illiterate coal miner.</p><p>As he walked past the Little Theatre in town on his way home from Union Street School, Karen was spotted by future U.S. Rep. Dan Flood, who asked him if he were a Boy Scout. When he said he was, Karen was given a small role in a comedy at the theater.</p><p>“That’s when I began working,” Karen recalled in a 2013. “It was terrific for me. It gave me a real reason to exist, and to live. I was doing exactly what I wanted to do.”</p><p>He stayed with the theater for several plays, joined the Air Force during World War II and then went to New York, landing with the Neighborhood Playhouse in 1939. He went on to appear in the original Broadway productions of <em>Who’s Afraid of Virginia Woolf?</em> and <em>Cactus Flower.</em></p><p>Karen met Keaton in 1956, and the following year they performed in a touring production of George S. Kaufman and Marc Connelly’s comic play <em>Merton of the Movies</em>. They paired again for <em>Film</em> (1965), a 20-minute, virtually silent picture written by Nobel Prize-winning playwright Samuel Beckett (<em>Waiting for Godot</em>). Keaton died in 1966.</p>",
                "2018-10-17 16:12", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "James Karen, Actor in \'Poltergeist\' and So Much More, Dies at 94",
                "/images/news_3.jpg", 
                "<p>James Karen, the instantly recognizable character actor who moved the cemetery’s headstones — but not the bodies — as the developer Mr. Teague in the modern horror classic <em>Poltergeist</em>, has died. He was 94.</p><p>The incredibly prolific Karen, who also was noteworthy in such films as <em>The China Syndrome</em> (1979) and <em>The Return of the Living Dead </em>(1985) and on the finale of NBC’s <em>Little House</em> <em>on the Prairie</em> — he’s the dastardly reason the town of Walnut Grove was blown up — died Tuesday at his Los Angeles home, his wife, Alba, said.</p><p>Karen, who usually portrayed authority figures, good and bad, during his seven-decade career, also appeared in three films for director Oliver Stone: as Lynch, the office manager at Jackson Steinem, where Charlie Sheen’s stockbroker character works, in <em>Wall Street</em> (1987); as Secretary of State William Rogers in <em>Nixon</em> (1995); and as a Miami Sharks executive in <em>Any Given Sunday</em> (1999).</p><p>The genial Karen studied under legendary acting teacher Sanford Meisner at the Neighborhood Playhouse in New York and made his Broadway debut in Elia Kazan’s original 1947 production of <em>A Streetcar Named Desire</em>, where he was the understudy to Karl Malden and worked alongside Marlon Brando.</p><p>He has a whopping 204 acting credits&nbsp;listed on IMDb. “People don’t know my name, but they know my face because I’ve done so damn much work,” Karen once said.</p><p>That work includes originating the role of rich attorney Lincoln Tyler in 1970 on the ABC soap opera <em>All My Children</em> and appearing in thousands of commercials as the TV and radio spokesman (“Why pay more?”) for Pathmark, an East Coast supermarket chain that was liquidated in 2015.</p><p>Karen was married to the late Susan Reed, the renowned folk singer and zither player, from 1958 until their divorce in 1967, and the godfather of their son, Reed, was the one and only Buster Keaton.</p><p>He married Alba in 1986.</p><p>Karen was born Jacob Karnofsky on Nov. 28, 1923, in Wilkes-Barre, Pennsylvania, the son of Russian-born Jewish immigrants. He was taken to the movies to read the titles on the silent films for his father, an illiterate coal miner.</p><p>As he walked past the Little Theatre in town on his way home from Union Street School, Karen was spotted by future U.S. Rep. Dan Flood, who asked him if he were a Boy Scout. When he said he was, Karen was given a small role in a comedy at the theater.</p><p>“That’s when I began working,” Karen recalled in a 2013. “It was terrific for me. It gave me a real reason to exist, and to live. I was doing exactly what I wanted to do.”</p><p>He stayed with the theater for several plays, joined the Air Force during World War II and then went to New York, landing with the Neighborhood Playhouse in 1939. He went on to appear in the original Broadway productions of <em>Who’s Afraid of Virginia Woolf?</em> and <em>Cactus Flower.</em></p><p>Karen met Keaton in 1956, and the following year they performed in a touring production of George S. Kaufman and Marc Connelly’s comic play <em>Merton of the Movies</em>. They paired again for <em>Film</em> (1965), a 20-minute, virtually silent picture written by Nobel Prize-winning playwright Samuel Beckett (<em>Waiting for Godot</em>). Keaton died in 1966.</p>",
                "2018-10-17 16:12", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Leonardo DiCaprio and Martin Scorsese to Team up Again for ‘Killers of the Flower Moon’",
                "/images/news_2.jpg", 
                "<p>Leonardo DiCaprio and Martin Scorsese are teaming up again, to star in and direct respectively, for Imperative Entertainment’s “Killers of the Flower Moon,” which is based on the New York Times best-selling book by David Grann, individuals with knowledge of the project told TheWrap.</p><p>Eric Roth wrote the screenplay. Imperative Entertainment acquired the film rights to the book in 2016. Scorsese will produce alongside Imperative Entertainment’s Dan Friedkin, Bradley Thomas, Sikelia Productions’ Emma Tillinger Koskoff, and Appian Way Productions.</p><p>Set in 1920s Oklahoma, the Osage Nation discover oil under their land only to find themselves being murdered one by one. As the death toll rises, the newly created FBI takes up the case and unravels a chilling conspiracy and one of the most monstrous crimes in American history.</p><p>“When I read David Grann’s book, I immediately started seeing it–the people, the settings, the action–and I knew that I had to make it into a movie,” said Scorsese in a statement. “I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen.”</p><p>“I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen,” the director added.</p><p>30WEST will arrange the financing and distribution for the project.</p>",
                "2018-10-18 12:56", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Richard Linklater Directing Biopic on Comedian Bill Hicks for Focus", 
                "/images/news_1.jpg", 
                "Richard Linklater will write and direct an untitled film based on the life of comedian Bill Hicks for Focus Features.<br><br>Hicks was raised as a Southern Baptist and began performing comedy in the late 1970s in Texas. He toured extensively during the 1980s, specializing in dark humor and social commentary. A typical joke: \'I never got along with my dad. Kids used to come up to me and say, \'My dad can beat up your dad.\' I’d say \'Yeah? When?\'\'<br><br>Hicks died of pancreatic cancer in 1994 at the age of 32. Linklater has said publicly that he regretted that he was not able to work with Hicks, who was a fan of Linklater’s Texas-based coming-of-age movie <i>Dazed and Confused</i>.", 
                "2018-11-19 14:23", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Richard Linklater Directing Biopic on Comedian Bill Hicks for Focus", 
                "/images/news_1.jpg", 
                "Richard Linklater will write and direct an untitled film based on the life of comedian Bill Hicks for Focus Features.<br><br>Hicks was raised as a Southern Baptist and began performing comedy in the late 1970s in Texas. He toured extensively during the 1980s, specializing in dark humor and social commentary. A typical joke: \'I never got along with my dad. Kids used to come up to me and say, \'My dad can beat up your dad.\' I’d say \'Yeah? When?\'\'<br><br>Hicks died of pancreatic cancer in 1994 at the age of 32. Linklater has said publicly that he regretted that he was not able to work with Hicks, who was a fan of Linklater’s Texas-based coming-of-age movie <i>Dazed and Confused</i>.", 
                "2018-11-19 14:23", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Leonardo DiCaprio and Martin Scorsese to Team up Again for ‘Killers of the Flower Moon’",
                "/images/news_2.jpg", 
                "<p>Leonardo DiCaprio and Martin Scorsese are teaming up again, to star in and direct respectively, for Imperative Entertainment’s “Killers of the Flower Moon,” which is based on the New York Times best-selling book by David Grann, individuals with knowledge of the project told TheWrap.</p><p>Eric Roth wrote the screenplay. Imperative Entertainment acquired the film rights to the book in 2016. Scorsese will produce alongside Imperative Entertainment’s Dan Friedkin, Bradley Thomas, Sikelia Productions’ Emma Tillinger Koskoff, and Appian Way Productions.</p><p>Set in 1920s Oklahoma, the Osage Nation discover oil under their land only to find themselves being murdered one by one. As the death toll rises, the newly created FBI takes up the case and unravels a chilling conspiracy and one of the most monstrous crimes in American history.</p><p>“When I read David Grann’s book, I immediately started seeing it–the people, the settings, the action–and I knew that I had to make it into a movie,” said Scorsese in a statement. “I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen.”</p><p>“I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen,” the director added.</p><p>30WEST will arrange the financing and distribution for the project.</p>",
                "2018-10-18 12:56", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "James Karen, Actor in \'Poltergeist\' and So Much More, Dies at 94",
                "/images/news_3.jpg", 
                "<p>James Karen, the instantly recognizable character actor who moved the cemetery’s headstones — but not the bodies — as the developer Mr. Teague in the modern horror classic <em>Poltergeist</em>, has died. He was 94.</p><p>The incredibly prolific Karen, who also was noteworthy in such films as <em>The China Syndrome</em> (1979) and <em>The Return of the Living Dead </em>(1985) and on the finale of NBC’s <em>Little House</em> <em>on the Prairie</em> — he’s the dastardly reason the town of Walnut Grove was blown up — died Tuesday at his Los Angeles home, his wife, Alba, said.</p><p>Karen, who usually portrayed authority figures, good and bad, during his seven-decade career, also appeared in three films for director Oliver Stone: as Lynch, the office manager at Jackson Steinem, where Charlie Sheen’s stockbroker character works, in <em>Wall Street</em> (1987); as Secretary of State William Rogers in <em>Nixon</em> (1995); and as a Miami Sharks executive in <em>Any Given Sunday</em> (1999).</p><p>The genial Karen studied under legendary acting teacher Sanford Meisner at the Neighborhood Playhouse in New York and made his Broadway debut in Elia Kazan’s original 1947 production of <em>A Streetcar Named Desire</em>, where he was the understudy to Karl Malden and worked alongside Marlon Brando.</p><p>He has a whopping 204 acting credits&nbsp;listed on IMDb. “People don’t know my name, but they know my face because I’ve done so damn much work,” Karen once said.</p><p>That work includes originating the role of rich attorney Lincoln Tyler in 1970 on the ABC soap opera <em>All My Children</em> and appearing in thousands of commercials as the TV and radio spokesman (“Why pay more?”) for Pathmark, an East Coast supermarket chain that was liquidated in 2015.</p><p>Karen was married to the late Susan Reed, the renowned folk singer and zither player, from 1958 until their divorce in 1967, and the godfather of their son, Reed, was the one and only Buster Keaton.</p><p>He married Alba in 1986.</p><p>Karen was born Jacob Karnofsky on Nov. 28, 1923, in Wilkes-Barre, Pennsylvania, the son of Russian-born Jewish immigrants. He was taken to the movies to read the titles on the silent films for his father, an illiterate coal miner.</p><p>As he walked past the Little Theatre in town on his way home from Union Street School, Karen was spotted by future U.S. Rep. Dan Flood, who asked him if he were a Boy Scout. When he said he was, Karen was given a small role in a comedy at the theater.</p><p>“That’s when I began working,” Karen recalled in a 2013. “It was terrific for me. It gave me a real reason to exist, and to live. I was doing exactly what I wanted to do.”</p><p>He stayed with the theater for several plays, joined the Air Force during World War II and then went to New York, landing with the Neighborhood Playhouse in 1939. He went on to appear in the original Broadway productions of <em>Who’s Afraid of Virginia Woolf?</em> and <em>Cactus Flower.</em></p><p>Karen met Keaton in 1956, and the following year they performed in a touring production of George S. Kaufman and Marc Connelly’s comic play <em>Merton of the Movies</em>. They paired again for <em>Film</em> (1965), a 20-minute, virtually silent picture written by Nobel Prize-winning playwright Samuel Beckett (<em>Waiting for Godot</em>). Keaton died in 1966.</p>",
                "2018-10-17 16:12", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "James Karen, Actor in \'Poltergeist\' and So Much More, Dies at 94",
                "/images/news_3.jpg", 
                "<p>James Karen, the instantly recognizable character actor who moved the cemetery’s headstones — but not the bodies — as the developer Mr. Teague in the modern horror classic <em>Poltergeist</em>, has died. He was 94.</p><p>The incredibly prolific Karen, who also was noteworthy in such films as <em>The China Syndrome</em> (1979) and <em>The Return of the Living Dead </em>(1985) and on the finale of NBC’s <em>Little House</em> <em>on the Prairie</em> — he’s the dastardly reason the town of Walnut Grove was blown up — died Tuesday at his Los Angeles home, his wife, Alba, said.</p><p>Karen, who usually portrayed authority figures, good and bad, during his seven-decade career, also appeared in three films for director Oliver Stone: as Lynch, the office manager at Jackson Steinem, where Charlie Sheen’s stockbroker character works, in <em>Wall Street</em> (1987); as Secretary of State William Rogers in <em>Nixon</em> (1995); and as a Miami Sharks executive in <em>Any Given Sunday</em> (1999).</p><p>The genial Karen studied under legendary acting teacher Sanford Meisner at the Neighborhood Playhouse in New York and made his Broadway debut in Elia Kazan’s original 1947 production of <em>A Streetcar Named Desire</em>, where he was the understudy to Karl Malden and worked alongside Marlon Brando.</p><p>He has a whopping 204 acting credits&nbsp;listed on IMDb. “People don’t know my name, but they know my face because I’ve done so damn much work,” Karen once said.</p><p>That work includes originating the role of rich attorney Lincoln Tyler in 1970 on the ABC soap opera <em>All My Children</em> and appearing in thousands of commercials as the TV and radio spokesman (“Why pay more?”) for Pathmark, an East Coast supermarket chain that was liquidated in 2015.</p><p>Karen was married to the late Susan Reed, the renowned folk singer and zither player, from 1958 until their divorce in 1967, and the godfather of their son, Reed, was the one and only Buster Keaton.</p><p>He married Alba in 1986.</p><p>Karen was born Jacob Karnofsky on Nov. 28, 1923, in Wilkes-Barre, Pennsylvania, the son of Russian-born Jewish immigrants. He was taken to the movies to read the titles on the silent films for his father, an illiterate coal miner.</p><p>As he walked past the Little Theatre in town on his way home from Union Street School, Karen was spotted by future U.S. Rep. Dan Flood, who asked him if he were a Boy Scout. When he said he was, Karen was given a small role in a comedy at the theater.</p><p>“That’s when I began working,” Karen recalled in a 2013. “It was terrific for me. It gave me a real reason to exist, and to live. I was doing exactly what I wanted to do.”</p><p>He stayed with the theater for several plays, joined the Air Force during World War II and then went to New York, landing with the Neighborhood Playhouse in 1939. He went on to appear in the original Broadway productions of <em>Who’s Afraid of Virginia Woolf?</em> and <em>Cactus Flower.</em></p><p>Karen met Keaton in 1956, and the following year they performed in a touring production of George S. Kaufman and Marc Connelly’s comic play <em>Merton of the Movies</em>. They paired again for <em>Film</em> (1965), a 20-minute, virtually silent picture written by Nobel Prize-winning playwright Samuel Beckett (<em>Waiting for Godot</em>). Keaton died in 1966.</p>",
                "2018-10-17 16:12", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Leonardo DiCaprio and Martin Scorsese to Team up Again for ‘Killers of the Flower Moon’",
                "/images/news_2.jpg", 
                "<p>Leonardo DiCaprio and Martin Scorsese are teaming up again, to star in and direct respectively, for Imperative Entertainment’s “Killers of the Flower Moon,” which is based on the New York Times best-selling book by David Grann, individuals with knowledge of the project told TheWrap.</p><p>Eric Roth wrote the screenplay. Imperative Entertainment acquired the film rights to the book in 2016. Scorsese will produce alongside Imperative Entertainment’s Dan Friedkin, Bradley Thomas, Sikelia Productions’ Emma Tillinger Koskoff, and Appian Way Productions.</p><p>Set in 1920s Oklahoma, the Osage Nation discover oil under their land only to find themselves being murdered one by one. As the death toll rises, the newly created FBI takes up the case and unravels a chilling conspiracy and one of the most monstrous crimes in American history.</p><p>“When I read David Grann’s book, I immediately started seeing it–the people, the settings, the action–and I knew that I had to make it into a movie,” said Scorsese in a statement. “I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen.”</p><p>“I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen,” the director added.</p><p>30WEST will arrange the financing and distribution for the project.</p>",
                "2018-10-18 12:56", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Richard Linklater Directing Biopic on Comedian Bill Hicks for Focus", 
                "/images/news_1.jpg", 
                "Richard Linklater will write and direct an untitled film based on the life of comedian Bill Hicks for Focus Features.<br><br>Hicks was raised as a Southern Baptist and began performing comedy in the late 1970s in Texas. He toured extensively during the 1980s, specializing in dark humor and social commentary. A typical joke: \'I never got along with my dad. Kids used to come up to me and say, \'My dad can beat up your dad.\' I’d say \'Yeah? When?\'\'<br><br>Hicks died of pancreatic cancer in 1994 at the age of 32. Linklater has said publicly that he regretted that he was not able to work with Hicks, who was a fan of Linklater’s Texas-based coming-of-age movie <i>Dazed and Confused</i>.", 
                "2018-11-19 14:23", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Richard Linklater Directing Biopic on Comedian Bill Hicks for Focus", 
                "/images/news_1.jpg", 
                "Richard Linklater will write and direct an untitled film based on the life of comedian Bill Hicks for Focus Features.<br><br>Hicks was raised as a Southern Baptist and began performing comedy in the late 1970s in Texas. He toured extensively during the 1980s, specializing in dark humor and social commentary. A typical joke: \'I never got along with my dad. Kids used to come up to me and say, \'My dad can beat up your dad.\' I’d say \'Yeah? When?\'\'<br><br>Hicks died of pancreatic cancer in 1994 at the age of 32. Linklater has said publicly that he regretted that he was not able to work with Hicks, who was a fan of Linklater’s Texas-based coming-of-age movie <i>Dazed and Confused</i>.", 
                "2018-11-19 14:23", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Leonardo DiCaprio and Martin Scorsese to Team up Again for ‘Killers of the Flower Moon’",
                "/images/news_2.jpg", 
                "<p>Leonardo DiCaprio and Martin Scorsese are teaming up again, to star in and direct respectively, for Imperative Entertainment’s “Killers of the Flower Moon,” which is based on the New York Times best-selling book by David Grann, individuals with knowledge of the project told TheWrap.</p><p>Eric Roth wrote the screenplay. Imperative Entertainment acquired the film rights to the book in 2016. Scorsese will produce alongside Imperative Entertainment’s Dan Friedkin, Bradley Thomas, Sikelia Productions’ Emma Tillinger Koskoff, and Appian Way Productions.</p><p>Set in 1920s Oklahoma, the Osage Nation discover oil under their land only to find themselves being murdered one by one. As the death toll rises, the newly created FBI takes up the case and unravels a chilling conspiracy and one of the most monstrous crimes in American history.</p><p>“When I read David Grann’s book, I immediately started seeing it–the people, the settings, the action–and I knew that I had to make it into a movie,” said Scorsese in a statement. “I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen.”</p><p>“I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen,” the director added.</p><p>30WEST will arrange the financing and distribution for the project.</p>",
                "2018-10-18 12:56", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "James Karen, Actor in \'Poltergeist\' and So Much More, Dies at 94",
                "/images/news_3.jpg", 
                "<p>James Karen, the instantly recognizable character actor who moved the cemetery’s headstones — but not the bodies — as the developer Mr. Teague in the modern horror classic <em>Poltergeist</em>, has died. He was 94.</p><p>The incredibly prolific Karen, who also was noteworthy in such films as <em>The China Syndrome</em> (1979) and <em>The Return of the Living Dead </em>(1985) and on the finale of NBC’s <em>Little House</em> <em>on the Prairie</em> — he’s the dastardly reason the town of Walnut Grove was blown up — died Tuesday at his Los Angeles home, his wife, Alba, said.</p><p>Karen, who usually portrayed authority figures, good and bad, during his seven-decade career, also appeared in three films for director Oliver Stone: as Lynch, the office manager at Jackson Steinem, where Charlie Sheen’s stockbroker character works, in <em>Wall Street</em> (1987); as Secretary of State William Rogers in <em>Nixon</em> (1995); and as a Miami Sharks executive in <em>Any Given Sunday</em> (1999).</p><p>The genial Karen studied under legendary acting teacher Sanford Meisner at the Neighborhood Playhouse in New York and made his Broadway debut in Elia Kazan’s original 1947 production of <em>A Streetcar Named Desire</em>, where he was the understudy to Karl Malden and worked alongside Marlon Brando.</p><p>He has a whopping 204 acting credits&nbsp;listed on IMDb. “People don’t know my name, but they know my face because I’ve done so damn much work,” Karen once said.</p><p>That work includes originating the role of rich attorney Lincoln Tyler in 1970 on the ABC soap opera <em>All My Children</em> and appearing in thousands of commercials as the TV and radio spokesman (“Why pay more?”) for Pathmark, an East Coast supermarket chain that was liquidated in 2015.</p><p>Karen was married to the late Susan Reed, the renowned folk singer and zither player, from 1958 until their divorce in 1967, and the godfather of their son, Reed, was the one and only Buster Keaton.</p><p>He married Alba in 1986.</p><p>Karen was born Jacob Karnofsky on Nov. 28, 1923, in Wilkes-Barre, Pennsylvania, the son of Russian-born Jewish immigrants. He was taken to the movies to read the titles on the silent films for his father, an illiterate coal miner.</p><p>As he walked past the Little Theatre in town on his way home from Union Street School, Karen was spotted by future U.S. Rep. Dan Flood, who asked him if he were a Boy Scout. When he said he was, Karen was given a small role in a comedy at the theater.</p><p>“That’s when I began working,” Karen recalled in a 2013. “It was terrific for me. It gave me a real reason to exist, and to live. I was doing exactly what I wanted to do.”</p><p>He stayed with the theater for several plays, joined the Air Force during World War II and then went to New York, landing with the Neighborhood Playhouse in 1939. He went on to appear in the original Broadway productions of <em>Who’s Afraid of Virginia Woolf?</em> and <em>Cactus Flower.</em></p><p>Karen met Keaton in 1956, and the following year they performed in a touring production of George S. Kaufman and Marc Connelly’s comic play <em>Merton of the Movies</em>. They paired again for <em>Film</em> (1965), a 20-minute, virtually silent picture written by Nobel Prize-winning playwright Samuel Beckett (<em>Waiting for Godot</em>). Keaton died in 1966.</p>",
                "2018-10-17 16:12", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "James Karen, Actor in \'Poltergeist\' and So Much More, Dies at 94",
                "/images/news_3.jpg", 
                "<p>James Karen, the instantly recognizable character actor who moved the cemetery’s headstones — but not the bodies — as the developer Mr. Teague in the modern horror classic <em>Poltergeist</em>, has died. He was 94.</p><p>The incredibly prolific Karen, who also was noteworthy in such films as <em>The China Syndrome</em> (1979) and <em>The Return of the Living Dead </em>(1985) and on the finale of NBC’s <em>Little House</em> <em>on the Prairie</em> — he’s the dastardly reason the town of Walnut Grove was blown up — died Tuesday at his Los Angeles home, his wife, Alba, said.</p><p>Karen, who usually portrayed authority figures, good and bad, during his seven-decade career, also appeared in three films for director Oliver Stone: as Lynch, the office manager at Jackson Steinem, where Charlie Sheen’s stockbroker character works, in <em>Wall Street</em> (1987); as Secretary of State William Rogers in <em>Nixon</em> (1995); and as a Miami Sharks executive in <em>Any Given Sunday</em> (1999).</p><p>The genial Karen studied under legendary acting teacher Sanford Meisner at the Neighborhood Playhouse in New York and made his Broadway debut in Elia Kazan’s original 1947 production of <em>A Streetcar Named Desire</em>, where he was the understudy to Karl Malden and worked alongside Marlon Brando.</p><p>He has a whopping 204 acting credits&nbsp;listed on IMDb. “People don’t know my name, but they know my face because I’ve done so damn much work,” Karen once said.</p><p>That work includes originating the role of rich attorney Lincoln Tyler in 1970 on the ABC soap opera <em>All My Children</em> and appearing in thousands of commercials as the TV and radio spokesman (“Why pay more?”) for Pathmark, an East Coast supermarket chain that was liquidated in 2015.</p><p>Karen was married to the late Susan Reed, the renowned folk singer and zither player, from 1958 until their divorce in 1967, and the godfather of their son, Reed, was the one and only Buster Keaton.</p><p>He married Alba in 1986.</p><p>Karen was born Jacob Karnofsky on Nov. 28, 1923, in Wilkes-Barre, Pennsylvania, the son of Russian-born Jewish immigrants. He was taken to the movies to read the titles on the silent films for his father, an illiterate coal miner.</p><p>As he walked past the Little Theatre in town on his way home from Union Street School, Karen was spotted by future U.S. Rep. Dan Flood, who asked him if he were a Boy Scout. When he said he was, Karen was given a small role in a comedy at the theater.</p><p>“That’s when I began working,” Karen recalled in a 2013. “It was terrific for me. It gave me a real reason to exist, and to live. I was doing exactly what I wanted to do.”</p><p>He stayed with the theater for several plays, joined the Air Force during World War II and then went to New York, landing with the Neighborhood Playhouse in 1939. He went on to appear in the original Broadway productions of <em>Who’s Afraid of Virginia Woolf?</em> and <em>Cactus Flower.</em></p><p>Karen met Keaton in 1956, and the following year they performed in a touring production of George S. Kaufman and Marc Connelly’s comic play <em>Merton of the Movies</em>. They paired again for <em>Film</em> (1965), a 20-minute, virtually silent picture written by Nobel Prize-winning playwright Samuel Beckett (<em>Waiting for Godot</em>). Keaton died in 1966.</p>",
                "2018-10-17 16:12", 
                1,
                1
            )
        ');

        $this->addSql('
            INSERT INTO news(title, photo, content, create_date, category_id, author_id) 
            VALUES(
                "Leonardo DiCaprio and Martin Scorsese to Team up Again for ‘Killers of the Flower Moon’",
                "/images/news_2.jpg", 
                "<p>Leonardo DiCaprio and Martin Scorsese are teaming up again, to star in and direct respectively, for Imperative Entertainment’s “Killers of the Flower Moon,” which is based on the New York Times best-selling book by David Grann, individuals with knowledge of the project told TheWrap.</p><p>Eric Roth wrote the screenplay. Imperative Entertainment acquired the film rights to the book in 2016. Scorsese will produce alongside Imperative Entertainment’s Dan Friedkin, Bradley Thomas, Sikelia Productions’ Emma Tillinger Koskoff, and Appian Way Productions.</p><p>Set in 1920s Oklahoma, the Osage Nation discover oil under their land only to find themselves being murdered one by one. As the death toll rises, the newly created FBI takes up the case and unravels a chilling conspiracy and one of the most monstrous crimes in American history.</p><p>“When I read David Grann’s book, I immediately started seeing it–the people, the settings, the action–and I knew that I had to make it into a movie,” said Scorsese in a statement. “I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen.”</p><p>“I’m so excited to be working with Eric Roth and reuniting with Leo DiCaprio to bring this truly unsettling American story to the screen,” the director added.</p><p>30WEST will arrange the financing and distribution for the project.</p>",
                "2018-10-18 12:56", 
                1,
                1
            )
        ');
        // @codingStandardsIgnoreEnd
    }

    public function down(Schema $schema): void
    {
    }
}
