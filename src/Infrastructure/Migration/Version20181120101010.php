<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181120101010
 * @package DoctrineMigrations
 */
final class Version20181120101010 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        // @codingStandardsIgnoreStart
        $this->addSql('
            INSERT INTO event(id, title, original_title, description, director_name,production_countries, production_year,
                age_limit, release_date, duration, poster_url, trailer_url) 
            VALUES(1, "303 Squadron", "Dywizjon 303", 
            "This is the story of the highly regarded fighter squadron in the history of aerial combat and their heroic defence of England during WW2, Battle of Britain against Nazi attacks. The 303 Squadron shot three times more Luftwaffe planes than any other allied",
            "Denis Delić", "Poland,UK", 2018, NULL, "2018-08-31", 99, 
            "https://m.media-amazon.com/images/M/MV5BMDYwMGEwNDAtZDM0NC00OGVmLWE2NjMtYmQ2NTkwNjgyMDM5XkEyXkFqcGdeQXVyNjQzNDI3NzY@._V1_SY400_CR0,0,275,400_AL_.jpg",
            "https://www.youtube.com/embed/yrmwDyZMcZo")
        ');

        $this->addSql('
            INSERT INTO event(id, title, original_title, description, director_name,production_countries, production_year,
                age_limit, release_date, duration, poster_url, trailer_url) 
            VALUES(2, "Bad times at El Royale", "Bad times at El Royale", 
            "Seven strangers, each with a secret to bury, meet at Lake Tahoe\'s El Royale, a rundown hotel with a dark past. Over the course of one fateful night, everyone will have a last shot at redemption... before everything goes to hell. Jeff Bridges, Chris Hemsworth, Jon Hamm, Dakota Johnson and Cynthia Erivo lead an all-star cast in BAD TIMES AT THE EL ROYALE.",
            "Drew Goddard", "USA", 2018, NULL, "2018-10-12", 141, 
            "https://m.media-amazon.com/images/M/MV5BOTk1Nzk1MDc1MF5BMl5BanBnXkFtZTgwNjU2NDExNjM@._V1_SY410_CR0,0,275,400_AL_.jpg",
            "https://www.youtube.com/embed/y7wzBVARwaU")
        ');

        $this->addSql('
            INSERT INTO event(id, title, original_title, description, director_name,production_countries, production_year,
                age_limit, release_date, duration, poster_url, trailer_url) 
            VALUES(3, "Cinderella and the Secret Prince", "Cinderella and the Secret Prince", 
            "Cinderella finds out that the prince at the Royal Ball is a fraud and that an evil witch has turned the real prince into a mouse. She and her friends then go on an adventure to rescue the prince as well as defeat the evil witch.",
            "Lynne Southerland", "USA", 2018, NULL, "2018-10-05", 87, 
            "https://m.media-amazon.com/images/M/MV5BNjk3NDkxNTAtN2ZiMS00OWE2LWI0ZDYtNDU4MzhkMTkxODc5XkEyXkFqcGdeQXVyNjI1MTgwMzE@._V1_SY410_CR0,0,275,400_AL_.jpg",
            "https://www.youtube.com/embed/mGkXtsa5vTM")
        ');

        $this->addSql('
            INSERT INTO event(id, title, original_title, description, director_name,production_countries, production_year,
                age_limit, release_date, duration, poster_url, trailer_url) 
            VALUES(4, "First Man", "First Man", 
            "On the heels of their six-time Academy Award (R)-winning smash, La La Land, Oscar (R)-winning director Damien Chazelle and star Ryan Gosling reteam for Universal Pictures\' First Man, the riveting story of NASA\'s mission to land a man on the moon, focusing on Neil Armstrong and the years 1961-1969. A visceral, first-person account, based on the book by James R. Hansen, the movie will explore the sacrifices and the cost on Armstrong and on the nation of one of the most dangerous missions in history.   ",
            "Damien Chazelle", "USA", 2018, NULL, "2018-10-19", 141, 
            "https://m.media-amazon.com/images/M/MV5BMDBhOTMxN2UtYjllYS00NWNiLWE1MzAtZjg3NmExODliMDQ0XkEyXkFqcGdeQXVyMjMxOTE0ODA@._V1_SY435_CR0,0,275,400_AL_.jpg",
            "https://www.youtube.com/embed/PSoRx87OO6k")
        ');

        $this->addSql('
            INSERT INTO event(id, title, original_title, description, director_name,production_countries, production_year,
                age_limit, release_date, duration, poster_url, trailer_url) 
            VALUES(5, "Bohemian Rhapsody", "Bohemian Rhapsody", 
            "Bohemian Rhapsody is a foot-stomping celebration of Queen, their music and their extraordinary lead singer Freddie Mercury. Freddie defied stereotypes and shattered convention to become one of the most beloved entertainers on the planet. The film traces the meteoric rise of the band through their iconic songs and revolutionary sound. They reach unparalleled success, but in an unexpected turn Freddie, surrounded by darker influences, shuns Queen in pursuit of his solo career. Having suffered greatly without the collaboration of Queen, Freddie manages to reunite with his bandmates just in time for Live Aid. While bravely facing a recent AIDS diagnosis, Freddie leads the band in one of the greatest performances in the history of rock music. Queen cements a legacy that continues to inspire outsiders, dreamers and music lovers to this day.",
            "Bryan Singer", "USA,UK", 2018, NULL, "2018-11-02", 135, 
            "https://m.media-amazon.com/images/M/MV5BNDg2NjIxMDUyNF5BMl5BanBnXkFtZTgwMzEzNTE1NTM@._V1_SY437_CR0,0,275,400_AL_.jpg",
            "https://www.youtube.com/embed/mP0VHJYFOAU")
        ');
        // @codingStandardsIgnoreEnd
    }

    public function down(Schema $schema): void
    {
    }
}
