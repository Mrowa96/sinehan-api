<?php

namespace Infrastructure\Filter;

use Doctrine\ORM\QueryBuilder;
use QueryFilter\Filter\AbstractFilter;

/**
 * Class EventPlaceIdFilter
 * @package Infrastructure\Filter
 */
final class EventPlaceIdFilter extends AbstractFilter
{
    /**
     * @inheritdoc
     */
    public function getQueryName(): string
    {
        return 'placeId';
    }

    /**
     * @inheritdoc
     */
    public function applyFilter(QueryBuilder $queryBuilder, array $filters): QueryBuilder
    {
        $value = (int)$this->getValue();

        if ($value > 0) {
            if (!in_array('s', $queryBuilder->getAllAliases())) {
                $queryBuilder->leftJoin('e.shows', 's');
            }

            $queryBuilder->andWhere('s.place = :placeId ')
                ->setParameter('placeId', $value);
        }

        return $queryBuilder;
    }
}
