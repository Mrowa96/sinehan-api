<?php

namespace Infrastructure\Filter;

use Doctrine\ORM\QueryBuilder;
use QueryFilter\Filter\AbstractFilter;

/**
 * Class EventPlaceDateFilter
 * @package Infrastructure\Filter
 */
final class EventDateFilter extends AbstractFilter
{
    /**
     * @inheritdoc
     */
    public function getQueryName(): string
    {
        return 'date';
    }

    /**
     * @inheritdoc
     */
    public function applyFilter(QueryBuilder $queryBuilder, array $filters): QueryBuilder
    {
        $value = new \DateTime($this->getValue());

        if ($value) {
            if (!in_array('s', $queryBuilder->getAllAliases())) {
                $queryBuilder->leftJoin('e.shows', 's');
            }

            $queryBuilder->andWhere('s.date = :date')
                ->setParameter('date', $value->format('Y-m-d'));
        }

        return $queryBuilder;
    }
}
