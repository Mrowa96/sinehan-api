<?php

namespace Infrastructure\Domain\ReservationTicket\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Reservation\Entity\Reservation;
use Domain\ReservationTicket\Entity\ReservationTicket;
use Domain\ReservationTicket\Factory\ReservationTicketFactoryInterface;
use Domain\ReservationTicket\Exception\CreateReservationTicketException;
use Domain\Ticket\Entity\Ticket;

/**
 * Class ReservationTicketFactory
 * @package Infrastructure\Domain\ReservationTicket\Factory
 */
final class ReservationTicketFactory implements ReservationTicketFactoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ReservationTicketFactory constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritdoc
     */
    public function create(array $data): ReservationTicket
    {
        try {
            $reservationTicket = new ReservationTicket();

            $reservationTicket->setPrice($data['price']);
            $reservationTicket->setQuantity($data['quantity']);
            $reservationTicket->setReservation(
                $this->entityManager->getReference(Reservation::class, $data['reservationId'])
            );
            $reservationTicket->setTicket(
                $this->entityManager->getReference(Ticket::class, $data['ticketId'])
            );

            return $reservationTicket;
        } catch (\Exception $exception) {
            throw new CreateReservationTicketException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
