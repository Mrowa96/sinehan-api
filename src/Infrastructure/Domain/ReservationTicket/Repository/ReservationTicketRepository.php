<?php

namespace Infrastructure\Domain\ReservationTicket\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Reservation\Entity\Reservation;
use Domain\ReservationTicket\Entity\ReservationTicket;
use Domain\ReservationTicket\Exception\InvalidReservationTicketException;
use Domain\ReservationTicket\Repository\ReservationTicketRepositoryInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ReservationTicketRepository
 * @package Infrastructure\Domain\ReservationTicket\Repository
 */
final class ReservationTicketRepository implements ReservationTicketRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ReservationTicketRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     */
    public function findFewByReservationId(string $reservationId): array
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('rt')
            ->from(ReservationTicket::class, 'rt')
            ->leftJoin('rt.reservation', 'r')
            ->where('r.id = :reservationId')
            ->setParameter('reservationId', $reservationId)
            ->getQuery()
            ->getResult();
    }

    /**
     * @inheritdoc
     */
    public function saveOne(ReservationTicket $reservationTicket, bool $isNew = false): void
    {
        $errors = $this->validator->validate($reservationTicket);

        if ($errors->count()) {
            throw new InvalidReservationTicketException($errors);
        }

        if ($isNew) {
            $this->entityManager->persist($reservationTicket);
        }

        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function removeFew(array $ids): void
    {
        $this->entityManager
            ->createQueryBuilder()
            ->delete(ReservationTicket::class, 'rt')
            ->where('rt.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->execute();
    }
}
