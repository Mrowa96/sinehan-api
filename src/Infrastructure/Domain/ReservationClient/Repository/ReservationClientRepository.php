<?php

namespace Infrastructure\Domain\ReservationClient\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Domain\ReservationClient\Entity\ReservationClient;
use Domain\ReservationClient\Exception\InvalidReservationClientException;
use Domain\ReservationClient\Repository\ReservationClientRepositoryInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ReservationClientRepository
 * @package Infrastructure\Domain\ReservationClient\Repository
 */
final class ReservationClientRepository implements ReservationClientRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ReservationClientRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     */
    public function saveOne(ReservationClient $reservationClient, bool $isNew = false): void
    {
        $errors = $this->validator->validate($reservationClient);

        if ($errors->count()) {
            throw new InvalidReservationClientException($errors);
        }

        if ($isNew) {
            $this->entityManager->persist($reservationClient);
        }

        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function clearOneByReservationId(string $reservationId): void
    {
        try {
            /** @var ReservationClient $reservationClient */
            $reservationClient = $this->entityManager
                ->createQueryBuilder()
                ->select('rc')
                ->from(ReservationClient::class, 'rc')
                ->leftJoin('rc.reservation', 'r')
                ->where('r.id = :reservationId')
                ->setParameter('reservationId', $reservationId)
                ->getQuery()
                ->getOneOrNullResult();

            if ($reservationClient) {
                $reservationClient->setEmail('');
                $reservationClient->setName('');
                $reservationClient->setSurname('');
                $reservationClient->setPhone('');

                $this->saveOne($reservationClient);
            }
        } catch (NonUniqueResultException $e) {
        }
    }
}
