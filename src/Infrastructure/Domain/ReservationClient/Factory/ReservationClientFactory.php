<?php

namespace Infrastructure\Domain\ReservationClient\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Reservation\Entity\Reservation;
use Domain\ReservationClient\Entity\ReservationClient;
use Domain\ReservationClient\Factory\ReservationClientFactoryInterface;
use Domain\ReservationClient\Exception\CreateReservationClientException;

/**
 * Class ReservationClientFactory
 * @package Infrastructure\Domain\ReservationClient\Factory
 */
final class ReservationClientFactory implements ReservationClientFactoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ReservationClientFactory constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritdoc
     */
    public function create(array $data): ReservationClient
    {
        try {
            $reservationClient = new ReservationClient();

            $reservationClient->setEmail($data['email']);
            $reservationClient->setName($data['name']);
            $reservationClient->setSurname($data['surname']);
            $reservationClient->setPhone($data['phone']);
            $reservationClient->setReservation(
                $this->entityManager->getReference(Reservation::class, $data['reservationId'])
            );

            return $reservationClient;
        } catch (\Exception $exception) {
            throw new CreateReservationClientException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
