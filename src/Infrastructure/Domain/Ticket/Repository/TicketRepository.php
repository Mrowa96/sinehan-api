<?php

namespace Infrastructure\Domain\Ticket\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Domain\Ticket\Entity\Ticket;
use Domain\Ticket\Exception\AddTicketException;
use Domain\Ticket\Exception\CreateTicketException;
use Domain\Ticket\Exception\NotFoundTicketException;
use Domain\Ticket\Exception\UpdateTicketException;
use Domain\Ticket\Factory\TicketFactoryInterface;
use Domain\Ticket\Repository\TicketRepositoryInterface;
use Domain\Ticket\Service\TicketUpdaterServiceInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class TicketRepository
 * @package Infrastructure\Domain\Ticket\Repository
 */
final class TicketRepository implements TicketRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var TicketFactoryInterface
     */
    private $ticketFactory;

    /**
     * @var TicketUpdaterServiceInterface
     */
    private $ticketUpdaterService;

    /**
     * TicketRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param TicketFactoryInterface $ticketFactory
     * @param TicketUpdaterServiceInterface $ticketUpdaterService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        TicketFactoryInterface $ticketFactory,
        TicketUpdaterServiceInterface $ticketUpdaterService
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->ticketFactory = $ticketFactory;
        $this->ticketUpdaterService = $ticketUpdaterService;
    }

    /**
     * @inheritdoc
     */
    public function add(array $data): Ticket
    {
        try {
            $ticket = $this->ticketFactory->create($data);

            $errors = $this->validator->validate($ticket);

            if ($errors->count()) {
                throw new AddTicketException($errors);
            }

            $this->entityManager->persist($ticket);
            $this->entityManager->flush();

            return $ticket;
        } catch (CreateTicketException | ORMException $exception) {
            throw new AddTicketException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @inheritdoc
     */
    public function update(int $id, array $data): Ticket
    {
        $ticket = $this->findOne($id);

        if (!$ticket) {
            throw new NotFoundTicketException("Cannot found ticket with id ${id}");
        }

        $ticket = $this->ticketUpdaterService->update($ticket, $data);
        $errors = $this->validator->validate($ticket);

        if ($errors->count()) {
            throw new UpdateTicketException($errors);
        }

        $this->entityManager->flush();

        return $ticket;
    }

    /**
     * @inheritdoc
     */
    public function remove(int $id): void
    {
        $ticket = $this->findOne($id);

        if (!$ticket) {
            throw new NotFoundTicketException("Cannot found ticket with id ${id}");
        }

        $this->entityManager->remove($ticket);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function findOne(int $id): ?Ticket
    {
        try {
            $ticket = $this->entityManager
                ->createQueryBuilder()
                ->select('t')
                ->from(Ticket::class, 't')
                ->where('t.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();

            return $ticket;
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function findAll(): array
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('t')
            ->from(Ticket::class, 't')
            ->getQuery()
            ->getResult();
    }
}
