<?php

namespace Infrastructure\Domain\Ticket\Service;

use Domain\Ticket\Entity\Ticket;
use Domain\Ticket\Service\TicketUpdaterServiceInterface;

/**
 * Class TicketUpdaterService
 * @package Infrastructure\Domain\Ticket\Service
 */
final class TicketUpdaterService implements TicketUpdaterServiceInterface
{
    /**
     * @inheritdoc
     */
    public function update(Ticket $ticket, array $data): Ticket
    {
        if (array_key_exists('name', $data)) {
            $ticket->setName($data['name']);
        }

        if (array_key_exists('price', $data)) {
            $ticket->setPrice($data['price']);
        }

        return $ticket;
    }
}
