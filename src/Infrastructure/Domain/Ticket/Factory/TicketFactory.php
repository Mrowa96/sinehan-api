<?php

namespace Infrastructure\Domain\Ticket\Factory;

use Domain\Ticket\Entity\Ticket;
use Domain\Ticket\Factory\TicketFactoryInterface;
use Domain\Ticket\Exception\CreateTicketException;

/**
 * Class TicketFactory
 * @package Infrastructure\Domain\Ticket\Factory
 */
final class TicketFactory implements TicketFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(array $data): Ticket
    {
        try {
            $ticket = new Ticket();

            $ticket->setName($data['name']);
            $ticket->setPrice($data['price']);

            return $ticket;
        } catch (\Exception $exception) {
            throw new CreateTicketException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
