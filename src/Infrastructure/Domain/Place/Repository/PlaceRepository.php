<?php

namespace Infrastructure\Domain\Place\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Domain\Place\Entity\Place;
use Domain\Place\Exception\AddPlaceException;
use Domain\Place\Exception\CreatePlaceException;
use Domain\Place\Exception\NotFoundPlaceException;
use Domain\Place\Exception\UpdatePlaceException;
use Domain\Place\Factory\PlaceFactoryInterface;
use Domain\Place\Repository\PlaceRepositoryInterface;
use Domain\Place\Service\PlaceUpdaterServiceInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class PlaceRepository
 * @package Infrastructure\Domain\Place\Repository
 */
final class PlaceRepository implements PlaceRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var PlaceFactoryInterface
     */
    private $placeFactory;

    /**
     * @var PlaceUpdaterServiceInterface
     */
    private $placeUpdaterService;

    /**
     * PlaceRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param PlaceFactoryInterface $placeFactory
     * @param PlaceUpdaterServiceInterface $placeUpdaterService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        PlaceFactoryInterface $placeFactory,
        PlaceUpdaterServiceInterface $placeUpdaterService
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->placeFactory = $placeFactory;
        $this->placeUpdaterService = $placeUpdaterService;
    }

    /**
     * @inheritdoc
     */
    public function add(array $data): Place
    {
        try {
            $place = $this->placeFactory->create($data);

            $errors = $this->validator->validate($place);

            if ($errors->count()) {
                throw new AddPlaceException($errors);
            }

            $this->entityManager->persist($place);
            $this->entityManager->flush();

            return $place;
        } catch (CreatePlaceException | ORMException $exception) {
            throw new AddPlaceException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @inheritdoc
     */
    public function update(int $id, array $data): Place
    {
        $place = $this->findOne($id);

        if (!$place) {
            throw new NotFoundPlaceException("Cannot found place with id ${id}");
        }

        $place = $this->placeUpdaterService->update($place, $data);
        $errors = $this->validator->validate($place);

        if ($errors->count()) {
            throw new UpdatePlaceException($errors);
        }

        $this->entityManager->flush();

        return $place;
    }

    /**
     * @inheritdoc
     */
    public function remove(int $id): void
    {
        $place = $this->findOne($id);

        if (!$place) {
            throw new NotFoundPlaceException("Cannot found place with id ${id}");
        }

        $this->entityManager->remove($place);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function findOne(int $id): ?Place
    {
        try {
            $place = $this->entityManager
                ->createQueryBuilder()
                ->select('p')
                ->from(Place::class, 'p')
                ->where('p.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();

            return $place;
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function findAll(): array
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('p')
            ->from(Place::class, 'p')
            ->getQuery()
            ->getResult();
    }
}
