<?php

namespace Infrastructure\Domain\Place\Factory;

use Domain\Place\Entity\Place;
use Domain\Place\Factory\PlaceFactoryInterface;
use Domain\Place\Exception\CreatePlaceException;

/**
 * Class PlaceFactory
 * @package Infrastructure\Domain\Place\Factory
 */
final class PlaceFactory implements PlaceFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(array $data): Place
    {
        try {
            $place = new Place();

            $place->setName($data['name']);
            $place->setLatitude($data['latitude']);
            $place->setLongitude($data['longitude']);

            return $place;
        } catch (\Exception $exception) {
            throw new CreatePlaceException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
