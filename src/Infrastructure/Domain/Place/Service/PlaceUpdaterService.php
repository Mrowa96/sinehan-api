<?php

namespace Infrastructure\Domain\Place\Service;

use Domain\Place\Entity\Place;
use Domain\Place\Service\PlaceUpdaterServiceInterface;

/**
 * Class PlaceUpdaterService
 * @package Infrastructure\Domain\Place\Service
 */
final class PlaceUpdaterService implements PlaceUpdaterServiceInterface
{
    /**
     * @inheritdoc
     */
    public function update(Place $place, array $data): Place
    {
        if (array_key_exists('name', $data)) {
            $place->setName($data['name']);
        }

        if (array_key_exists('latitude', $data)) {
            $place->setLatitude($data['latitude']);
        }

        if (array_key_exists('longitude', $data)) {
            $place->setLongitude($data['longitude']);
        }

        return $place;
    }
}
