<?php

namespace Infrastructure\Domain\PlaceRoom\Factory;

use Domain\PlaceRoom\Entity\PlaceRoom;
use Domain\PlaceRoom\Factory\PlaceRoomFactoryInterface;
use Domain\PlaceRoom\Exception\CreatePlaceRoomException;

/**
 * Class PlaceRoomFactory
 * @package Infrastructure\Domain\PlaceRoom\Factory
 */
final class PlaceRoomFactory implements PlaceRoomFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(array $data): PlaceRoom
    {
        try {
            $placeRoom = new PlaceRoom();

            $placeRoom->setName($data['name']);
            $placeRoom->setSeatsRows($data['seatsRows']);
            $placeRoom->setSeatsColumns($data['seatsColumns']);
            $placeRoom->setPlace($data['place']);

            return $placeRoom;
        } catch (\Exception $exception) {
            throw new CreatePlaceRoomException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
