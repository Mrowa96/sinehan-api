<?php

namespace Infrastructure\Domain\PlaceRoom\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Domain\Place\Entity\Place;
use Domain\PlaceRoom\Entity\PlaceRoom;
use Domain\PlaceRoom\Exception\AddPlaceRoomException;
use Domain\PlaceRoom\Exception\CreatePlaceRoomException;
use Domain\PlaceRoom\Exception\NotFoundPlaceRoomException;
use Domain\PlaceRoom\Exception\UpdatePlaceRoomException;
use Domain\PlaceRoom\Factory\PlaceRoomFactoryInterface;
use Domain\PlaceRoom\Repository\PlaceRoomRepositoryInterface;
use Domain\PlaceRoom\Service\PlaceRoomUpdaterServiceInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class PlaceRoomRepository
 * @package Infrastructure\Domain\PlaceRoom\Repository
 */
final class PlaceRoomRepository implements PlaceRoomRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var PlaceRoomFactoryInterface
     */
    private $placeRoomFactory;

    /**
     * @var PlaceRoomUpdaterServiceInterface
     */
    private $placeRoomUpdaterService;

    /**
     * PlaceRoomRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param PlaceRoomFactoryInterface $placeRoomFactory
     * @param PlaceRoomUpdaterServiceInterface $placeRoomUpdaterService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        PlaceRoomFactoryInterface $placeRoomFactory,
        PlaceRoomUpdaterServiceInterface $placeRoomUpdaterService
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->placeRoomFactory = $placeRoomFactory;
        $this->placeRoomUpdaterService = $placeRoomUpdaterService;
    }

    /**
     * @inheritdoc
     */
    public function add(array $data): PlaceRoom
    {
        try {
            $data['place'] = $this->entityManager->getReference(Place::class, $data['placeId']);

            $placeRoom = $this->placeRoomFactory->create($data);

            $errors = $this->validator->validate($placeRoom);

            if ($errors->count()) {
                throw new AddPlaceRoomException($errors);
            }

            $this->entityManager->persist($placeRoom);
            $this->entityManager->flush();

            return $placeRoom;
        } catch (CreatePlaceRoomException | ORMException $exception) {
            throw new AddPlaceRoomException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @inheritdoc
     */
    public function update(int $id, array $data): PlaceRoom
    {
        $placeRoom = $this->findOne($id);

        if (!$placeRoom) {
            throw new NotFoundPlaceRoomException("Cannot found place room with id ${id}");
        }

        try {
            if (!empty($data['placeId'])) {
                $data['place'] = $this->entityManager->getReference(Place::class, $data['placeId']);
            }
        } catch (ORMException $exception) {
            throw new UpdatePlaceRoomException($exception->getMessage(), $exception->getCode(), $exception);
        }

        $placeRoom = $this->placeRoomUpdaterService->update($placeRoom, $data);
        $errors = $this->validator->validate($placeRoom);

        if ($errors->count()) {
            throw new UpdatePlaceRoomException($errors);
        }

        $this->entityManager->flush();

        return $placeRoom;
    }

    /**
     * @inheritdoc
     */
    public function remove(int $id): void
    {
        $placeRoom = $this->findOne($id);

        if (!$placeRoom) {
            throw new NotFoundPlaceRoomException("Cannot found place room with id ${id}");
        }

        $this->entityManager->remove($placeRoom);
        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @return PlaceRoom|null
     */
    public function findOne(int $id): ?PlaceRoom
    {
        try {
            $placeRoom = $this->entityManager
                ->createQueryBuilder()
                ->select('r')
                ->from(PlaceRoom::class, 'r')
                ->where('r.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();

            return $placeRoom;
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }
}
