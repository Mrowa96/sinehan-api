<?php

namespace Infrastructure\Domain\PlaceRoom\Service;

use Domain\PlaceRoom\Entity\PlaceRoom;
use Domain\PlaceRoom\Service\PlaceRoomUpdaterServiceInterface;

/**
 * Class PlaceRoomUpdaterService
 * @package Infrastructure\Domain\PlaceRoom\Service
 */
final class PlaceRoomUpdaterService implements PlaceRoomUpdaterServiceInterface
{
    /**
     * @inheritdoc
     */
    public function update(PlaceRoom $placeRoom, array $data): PlaceRoom
    {
        if (array_key_exists('name', $data)) {
            $placeRoom->setName($data['name']);
        }

        if (array_key_exists('seatsRows', $data)) {
            $placeRoom->setSeatsRows($data['seatsRows']);
        }

        if (array_key_exists('seatsColumns', $data)) {
            $placeRoom->setSeatsColumns($data['seatsColumns']);
        }

        if (array_key_exists('place', $data)) {
            $placeRoom->setPlace($data['place']);
        }

        return $placeRoom;
    }
}
