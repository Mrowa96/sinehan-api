<?php

namespace Infrastructure\Domain\NewsCategory\Factory;

use Domain\NewsCategory\Entity\NewsCategory;
use Domain\NewsCategory\Factory\NewsCategoryFactoryInterface;
use Domain\NewsCategory\Exception\CreateNewsCategoryException;

/**
 * Class NewsCategoryFactory
 * @package Infrastructure\Domain\NewsCategory\Factory
 */
final class NewsCategoryFactory implements NewsCategoryFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(array $data): NewsCategory
    {
        try {
            $newsCategory = new NewsCategory();

            $newsCategory->setName($data['name']);

            return $newsCategory;
        } catch (\Exception $exception) {
            throw new CreateNewsCategoryException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
