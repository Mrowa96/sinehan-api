<?php

namespace Infrastructure\Domain\NewsCategory\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Domain\NewsCategory\Entity\NewsCategory;
use Domain\NewsCategory\Exception\AddNewsCategoryException;
use Domain\NewsCategory\Exception\CreateNewsCategoryException;
use Domain\NewsCategory\Exception\NotFoundNewsCategoryException;
use Domain\NewsCategory\Exception\UpdateNewsCategoryException;
use Domain\NewsCategory\Factory\NewsCategoryFactoryInterface;
use Domain\NewsCategory\Repository\NewsCategoryRepositoryInterface;
use Domain\NewsCategory\Service\NewsCategoryUpdaterServiceInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class NewsCategoryRepository
 * @package Infrastructure\Domain\NewsCategory\Repository
 */
final class NewsCategoryRepository implements NewsCategoryRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var NewsCategoryFactoryInterface
     */
    private $newsCategoryFactory;

    /**
     * @var NewsCategoryUpdaterServiceInterface
     */
    private $newsCategoryUpdaterService;

    /**
     * NewsCategoryRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param NewsCategoryFactoryInterface $newsCategoryFactory
     * @param NewsCategoryUpdaterServiceInterface $newsCategoryUpdaterService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        NewsCategoryFactoryInterface $newsCategoryFactory,
        NewsCategoryUpdaterServiceInterface $newsCategoryUpdaterService
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->newsCategoryFactory = $newsCategoryFactory;
        $this->newsCategoryUpdaterService = $newsCategoryUpdaterService;
    }

    /**
     * @inheritdoc
     */
    public function add(array $data): NewsCategory
    {
        try {
            $newsCategory = $this->newsCategoryFactory->create($data);

            $errors = $this->validator->validate($newsCategory);

            if ($errors->count()) {
                throw new AddNewsCategoryException($errors);
            }

            $this->entityManager->persist($newsCategory);
            $this->entityManager->flush();

            return $newsCategory;
        } catch (CreateNewsCategoryException | ORMException $exception) {
            throw new AddNewsCategoryException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @inheritdoc
     */
    public function update(int $id, array $data): NewsCategory
    {
        $newsCategory = $this->findOne($id);

        if (!$newsCategory) {
            throw new NotFoundNewsCategoryException("Cannot found news category with id ${id}");
        }

        $newsCategory = $this->newsCategoryUpdaterService->update($newsCategory, $data);
        $errors = $this->validator->validate($newsCategory);

        if ($errors->count()) {
            throw new UpdateNewsCategoryException($errors);
        }

        $this->entityManager->flush();

        return $newsCategory;
    }

    /**
     * @inheritdoc
     */
    public function remove(int $id): void
    {
        $newsCategory = $this->findOne($id);

        if (!$newsCategory) {
            throw new NotFoundNewsCategoryException("Cannot found news category with id ${id}");
        }

        $this->entityManager->remove($newsCategory);
        $this->entityManager->flush();
    }


    /**
     * @param int $id
     * @return NewsCategory|null
     */
    public function findOne(int $id): ?NewsCategory
    {
        try {
            return $this->entityManager
                ->createQueryBuilder()
                ->select('nc')
                ->from(NewsCategory::class, 'nc')
                ->where('nc.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }
}
