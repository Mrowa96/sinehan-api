<?php

namespace Infrastructure\Domain\NewsCategory\Service;

use Domain\NewsCategory\Entity\NewsCategory;
use Domain\NewsCategory\Service\NewsCategoryUpdaterServiceInterface;

/**
 * Class NewsCategoryUpdaterService
 * @package Infrastructure\Domain\NewsCategory\Service
 */
final class NewsCategoryUpdaterService implements NewsCategoryUpdaterServiceInterface
{
    /**
     * @inheritdoc
     */
    public function update(NewsCategory $newsCategory, array $data): NewsCategory
    {
        if (array_key_exists('name', $data)) {
            $newsCategory->setName($data['name']);
        }

        return $newsCategory;
    }
}
