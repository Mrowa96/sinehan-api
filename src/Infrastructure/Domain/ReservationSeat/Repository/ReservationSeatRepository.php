<?php

namespace Infrastructure\Domain\ReservationSeat\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Domain\ReservationSeat\Entity\ReservationSeat;
use Domain\ReservationSeat\Exception\InvalidReservationSeatException;
use Domain\ReservationSeat\Repository\ReservationSeatRepositoryInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ReservationSeatRepository
 * @package Infrastructure\Domain\ReservationSeat\Repository
 */
final class ReservationSeatRepository implements ReservationSeatRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ReservationSeatRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     */
    public function findOneByParameters(
        string $reservationId,
        int $showId,
        string $time,
        int $row,
        int $column
    ): ?ReservationSeat {
        try {
            return $this->entityManager
                ->createQueryBuilder()
                ->select('rs')
                ->from(ReservationSeat::class, 'rs')
                ->leftJoin('rs.reservation', 'r')
                ->leftJoin('r.show', 's')
                ->where('s.id = :showId')
                ->andWhere('r.id != :reservationId')
                ->andWhere('r.time = :time')
                ->andWhere('rs.seatColumn = :column')
                ->andWhere('rs.seatRow = :row')
                ->setParameters([
                    'reservationId' => $reservationId,
                    'showId' => $showId,
                    'time' => $time,
                    'column' => $column,
                    'row' => $row
                ])
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function findFewByReservationId(string $reservationId): array
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('rs')
            ->from(ReservationSeat::class, 'rs')
            ->leftJoin('rs.reservation', 'r')
            ->where('r.id = :reservationId')
            ->setParameter('reservationId', $reservationId)
            ->getQuery()
            ->getResult();
    }

    /**
     * @inheritdoc
     */
    public function findFewByShowIdAndTime(int $showId, string $time): array
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('rs')
            ->from(ReservationSeat::class, 'rs')
            ->leftJoin('rs.reservation', 'r')
            ->leftJoin('r.show', 's')
            ->where('s.id = :showId')
            ->andWhere('r.time = :time')
            ->setParameters(['showId' => $showId, 'time' => $time])
            ->getQuery()
            ->getResult();
    }

    /**
     * @inheritdoc
     */
    public function saveFew(array $reservationSeats, bool $isNew = false): void
    {
        foreach ($reservationSeats as $reservationSeat) {
            $errors = $this->validator->validate($reservationSeat);

            if ($errors->count()) {
                throw new InvalidReservationSeatException($errors);
            }
        }

        foreach ($reservationSeats as $reservationSeat) {
            if ($isNew) {
                $this->entityManager->persist($reservationSeat);
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function removeFew(array $ids): void
    {
        $this->entityManager
            ->createQueryBuilder()
            ->delete(ReservationSeat::class, 'rs')
            ->where('rs.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->execute();
    }

    /**
     * @inheritdoc
     */
    public function removeFewByReservationId(string $reservationId): void
    {
        $reservationSeatIds = array_map(function (ReservationSeat $reservationSeat) {
            return $reservationSeat->getId();
        }, $this->findFewByReservationId($reservationId));

        $this->entityManager
            ->createQueryBuilder()
            ->delete(ReservationSeat::class, 'rs')
            ->where('rs.id IN (:ids)')
            ->setParameter('ids', $reservationSeatIds)
            ->getQuery()
            ->execute();
    }
}
