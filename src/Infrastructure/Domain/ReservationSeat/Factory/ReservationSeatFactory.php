<?php

namespace Infrastructure\Domain\ReservationSeat\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Reservation\Entity\Reservation;
use Domain\ReservationSeat\Entity\ReservationSeat;
use Domain\ReservationSeat\Factory\ReservationSeatFactoryInterface;
use Domain\ReservationSeat\Exception\CreateReservationSeatException;

/**
 * Class ReservationSeatFactory
 * @package Infrastructure\Domain\ReservationSeat\Factory
 */
final class ReservationSeatFactory implements ReservationSeatFactoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ReservationSeatFactory constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritdoc
     */
    public function create(array $data): ReservationSeat
    {
        try {
            $reservationSeat = new ReservationSeat();

            $reservationSeat->setSeatColumn($data['column']);
            $reservationSeat->setSeatRow($data['row']);
            $reservationSeat->setReservation(
                $this->entityManager->getReference(Reservation::class, $data['reservationId'])
            );

            return $reservationSeat;
        } catch (\Exception $exception) {
            throw new CreateReservationSeatException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
