<?php

namespace Infrastructure\Domain\News\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Domain\News\Collection\NewsCollection;
use Domain\News\Entity\News;
use Domain\News\Exception\AddNewsException;
use Domain\News\Exception\CreateNewsException;
use Domain\News\Exception\NotFoundNewsException;
use Domain\News\Exception\UpdateNewsException;
use Domain\News\Factory\NewsFactoryInterface;
use Domain\News\Repository\NewsRepositoryInterface;
use Domain\News\Service\NewsUpdaterServiceInterface;
use Domain\NewsAuthor\Entity\NewsAuthor;
use Domain\NewsCategory\Entity\NewsCategory;
use QueryFilter\Service\QueryFilterServiceInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class NewsRepository
 * @package Infrastructure\Domain\News\Repository
 */
final class NewsRepository implements NewsRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var QueryFilterServiceInterface
     */
    private $queryFilterService;

    /**
     * @var NewsFactoryInterface
     */
    private $newsFactory;

    /**
     * @var NewsUpdaterServiceInterface
     */
    private $newsUpdaterService;

    /**
     * NewsRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param QueryFilterServiceInterface $queryFilterService
     * @param NewsFactoryInterface $newsFactory
     * @param NewsUpdaterServiceInterface $newsUpdaterService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        QueryFilterServiceInterface $queryFilterService,
        NewsFactoryInterface $newsFactory,
        NewsUpdaterServiceInterface $newsUpdaterService
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->queryFilterService = $queryFilterService;
        $this->newsFactory = $newsFactory;
        $this->newsUpdaterService = $newsUpdaterService;
    }

    /**
     * @inheritdoc
     */
    public function add(array $data): News
    {
        try {
            if (!empty($data['categoryId'])) {
                $data['category'] = $this->entityManager
                    ->getReference(NewsCategory::class, $data['categoryId']);
            }

            if (!empty($data['authorId'])) {
                $data['author'] = $this->entityManager
                    ->getReference(NewsAuthor::class, $data['authorId']);
            }

            $news = $this->newsFactory->create($data);

            $errors = $this->validator->validate($news);

            if ($errors->count()) {
                throw new AddNewsException($errors);
            }

            $this->entityManager->persist($news);
            $this->entityManager->flush();

            return $news;
        } catch (CreateNewsException | ORMException $exception) {
            throw new AddNewsException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }


    /**
     * @inheritdoc
     */
    public function update(int $id, array $data): News
    {
        $news = $this->findOne($id);

        if (!$news) {
            throw new NotFoundNewsException("Cannot found news with id ${id}");
        }
        try {
            if (!empty($data['categoryId'])) {
                $data['category'] = $this->entityManager
                    ->getReference(NewsCategory::class, $data['categoryId']);
            }

            if (!empty($data['authorId'])) {
                $data['author'] = $this->entityManager
                    ->getReference(NewsAuthor::class, $data['authorId']);
            }
        } catch (ORMException $exception) {
            throw new UpdateNewsException($exception->getMessage(), $exception->getCode(), $exception);
        }

        $news = $this->newsUpdaterService->update($news, $data);
        $errors = $this->validator->validate($news);

        if ($errors->count()) {
            throw new UpdateNewsException($errors);
        }

        $this->entityManager->flush();

        return $news;
    }

    /**
     * @inheritdoc
     */
    public function remove(int $id): void
    {
        $news = $this->findOne($id);

        if (!$news) {
            throw new NotFoundNewsException("Cannot found news with id ${id}");
        }

        $this->entityManager->remove($news);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function findOne(int $id): ?News
    {
        try {
            return $this->entityManager
                ->createQueryBuilder()
                ->select('n')
                ->from(News::class, 'n')
                ->where('n.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function findAll(): NewsCollection
    {
        try {
            $totalQuantity = $this->entityManager->createQueryBuilder()
                ->select('COUNT(n)')
                ->from(News::class, 'n')
                ->getQuery()->getSingleScalarResult();
        } catch (NonUniqueResultException $exception) {
            $totalQuantity = 0;
        }

        $queryBuilder = $this->entityManager
            ->createQueryBuilder()
            ->select('n')
            ->from(News::class, 'n');

        $this->queryFilterService->applyFilters($queryBuilder);

        return new NewsCollection($queryBuilder->getQuery()->getResult(), $totalQuantity);
    }
}
