<?php

namespace Infrastructure\Domain\News\Service;

use Domain\News\Entity\News;
use Domain\News\Service\NewsUpdaterServiceInterface;

/**
 * Class NewsUpdaterService
 * @package Infrastructure\Domain\News\Service
 */
final class NewsUpdaterService implements NewsUpdaterServiceInterface
{
    /**
     * @inheritdoc
     */
    public function update(News $news, array $data): News
    {
        if (array_key_exists('title', $data)) {
            $news->setTitle($data['title']);
        }

        if (array_key_exists('photo', $data)) {
            $news->setPhoto($data['photo']);
        }

        if (array_key_exists('content', $data)) {
            $news->setContent($data['content']);
        }

        if (array_key_exists('createDate', $data)) {
            $news->setCreateDate(new \DateTime($data['createDate']));
        }

        if (array_key_exists('category', $data)) {
            $news->setCategory($data['category']);
        }

        if (array_key_exists('author', $data)) {
            $news->setAuthor($data['author']);
        }

        return $news;
    }
}
