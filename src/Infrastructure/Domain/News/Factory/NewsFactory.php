<?php

namespace Infrastructure\Domain\News\Factory;

use Domain\News\Entity\News;
use Domain\News\Factory\NewsFactoryInterface;
use Domain\News\Exception\CreateNewsException;
use Domain\NewsCategory\Entity\NewsCategory;

/**
 * Class NewsFactory
 * @package Infrastructure\Domain\News\Factory
 */
final class NewsFactory implements NewsFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(array $data): News
    {
        try {
            $news = new News();

            $news->setTitle($data['title']);
            $news->setContent($data['content']);
            $news->setPhoto($data['photo']);
            $news->setCreateDate(new \DateTime($data['createDate']));

            if (!empty($data['category'])) {
                $news->setCategory($data['category']);
            }

            if (!empty($data['author'])) {
                $news->setAuthor($data['author']);
            }

            return $news;
        } catch (\Exception $exception) {
            throw new CreateNewsException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
