<?php

namespace Infrastructure\Domain\Event\Factory;

use Domain\Event\Entity\Event;
use Domain\Event\Factory\EventFactoryInterface;
use Domain\Event\Exception\CreateEventException;

/**
 * Class EventFactory
 * @package Infrastructure\Domain\Event\Factory
 */
final class EventFactory implements EventFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(array $data): Event
    {
        try {
            $event = new Event();

            $event->setTitle($data['title']);
            $event->setOriginalTitle($data['originalTitle']);
            $event->setDescription($data['description']);
            $event->setDirectorName($data['directorName']);
            $event->setProductionCountries($data['productionCountries']);
            $event->setProductionYear($data['productionYear']);
            $event->setAgeLimit($data['ageLimit']);
            $event->setReleaseDate(new \DateTime($data['releaseDate']));
            $event->setDuration($data['duration']);
            $event->setPosterUrl($data['posterUrl']);
            $event->setTrailerUrl($data['trailerUrl']);

            return $event;
        } catch (\Exception $exception) {
            throw new CreateEventException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
