<?php

namespace Infrastructure\Domain\Event\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Domain\Event\Collection\EventCollection;
use Domain\Event\Entity\Event;
use Domain\Event\Exception\AddEventException;
use Domain\Event\Exception\CreateEventException;
use Domain\Event\Exception\NotFoundEventException;
use Domain\Event\Exception\UpdateEventException;
use Domain\Event\Factory\EventFactoryInterface;
use Domain\Event\Repository\EventRepositoryInterface;
use Domain\Event\Service\EventUpdaterServiceInterface;
use Infrastructure\Filter\EventDateFilter;
use Infrastructure\Filter\EventPlaceIdFilter;
use QueryFilter\Filter\Limit;
use QueryFilter\Service\QueryFilterServiceInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class EventRepository
 * @package Infrastructure\Domain\Event\Repository
 */
final class EventRepository implements EventRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var QueryFilterServiceInterface
     */
    private $queryFilterService;

    /**
     * @var EventFactoryInterface
     */
    private $eventFactory;

    /**
     * @var EventUpdaterServiceInterface
     */
    private $eventUpdaterService;

    /**
     * EventRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param QueryFilterServiceInterface $queryFilterService
     * @param EventFactoryInterface $eventFactory
     * @param EventUpdaterServiceInterface $eventUpdaterService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        QueryFilterServiceInterface $queryFilterService,
        EventFactoryInterface $eventFactory,
        EventUpdaterServiceInterface $eventUpdaterService
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->queryFilterService = $queryFilterService;
        $this->eventFactory = $eventFactory;
        $this->eventUpdaterService = $eventUpdaterService;
    }

    /**
     * @inheritdoc
     */
    public function add(array $data): Event
    {
        try {
            $event = $this->eventFactory->create($data);

            $errors = $this->validator->validate($event);

            if ($errors->count()) {
                throw new AddEventException($errors);
            }

            $this->entityManager->persist($event);
            $this->entityManager->flush();

            return $event;
        } catch (CreateEventException | ORMException $exception) {
            throw new AddEventException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @inheritdoc
     */
    public function update(int $id, array $data): Event
    {
        $event = $this->findOne($id);

        if (!$event) {
            throw new NotFoundEventException("Cannot found event with id ${id}");
        }

        $event = $this->eventUpdaterService->update($event, $data);
        $errors = $this->validator->validate($event);

        if ($errors->count()) {
            throw new UpdateEventException($errors);
        }

        $this->entityManager->flush();

        return $event;
    }

    /**
     * @inheritdoc
     */
    public function remove(int $id): void
    {
        $event = $this->findOne($id);

        if (!$event) {
            throw new NotFoundEventException("Cannot found event with id ${id}");
        }

        $this->entityManager->remove($event);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function findOne(int $id): ?Event
    {
        try {
            $event = $this->entityManager
                ->createQueryBuilder()
                ->select('e')
                ->from(Event::class, 'e')
                ->where('e.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();

            return $event;
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function findAll(): EventCollection
    {
        $this->queryFilterService->enableFilters([EventPlaceIdFilter::class, EventDateFilter::class]);

        try {
            $totalQuantity = $this->entityManager->createQueryBuilder()
                ->select('COUNT(e)')
                ->from(Event::class, 'e')
                ->getQuery()->getSingleScalarResult();
        } catch (NonUniqueResultException $exception) {
            $totalQuantity = 0;
        }

        $queryBuilder = $this->entityManager
            ->createQueryBuilder()
            ->select('e')
            ->from(Event::class, 'e');

        $this->queryFilterService->applyFilters($queryBuilder);

        return new EventCollection($queryBuilder->getQuery()->getResult(), $totalQuantity);
    }
}
