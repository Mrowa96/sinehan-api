<?php

namespace Infrastructure\Domain\Event\Service;

use Domain\Event\Entity\Event;
use Domain\Event\Service\EventUpdaterServiceInterface;

/**
 * Class EventUpdaterService
 * @package Infrastructure\Domain\Event\Service
 */
final class EventUpdaterService implements EventUpdaterServiceInterface
{
    /**
     * @inheritdoc
     */
    public function update(Event $event, array $data): Event
    {
        if (array_key_exists('title', $data)) {
            $event->setTitle($data['title']);
        }

        if (array_key_exists('originalTitle', $data)) {
            $event->setOriginalTitle($data['originalTitle']);
        }

        if (array_key_exists('description', $data)) {
            $event->setDescription($data['description']);
        }

        if (array_key_exists('directorName', $data)) {
            $event->setDirectorName($data['directorName']);
        }

        if (array_key_exists('productionCountries', $data)) {
            $event->setProductionCountries($data['productionCountries']);
        }

        if (array_key_exists('productionYear', $data)) {
            $event->setProductionYear($data['productionYear']);
        }

        if (array_key_exists('ageLimit', $data)) {
            $event->setAgeLimit($data['ageLimit']);
        }

        if (array_key_exists('releaseDate', $data)) {
            $event->setReleaseDate(new \DateTime($data['releaseDate']));
        }

        if (array_key_exists('duration', $data)) {
            $event->setDuration($data['duration']);
        }

        if (array_key_exists('posterUrl', $data)) {
            $event->setPosterUrl($data['posterUrl']);
        }

        if (array_key_exists('trailerUrl', $data)) {
            $event->setTrailerUrl($data['trailerUrl']);
        }

        return $event;
    }
}
