<?php

namespace Infrastructure\Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Exception\ReservationClientUpdateFailure;
use Domain\Reservation\Service\ReservationClientUpdaterServiceInterface;
use Domain\ReservationClient\Factory\ReservationClientFactoryInterface;
use Domain\ReservationClient\Repository\ReservationClientRepositoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ReservationClientUpdaterService
 * @package Infrastructure\Domain\Reservation\Service
 */
final class ReservationClientUpdaterService implements ReservationClientUpdaterServiceInterface
{
    /**
     * @var ReservationClientRepositoryInterface
     */
    private $reservationClientRepository;

    /**
     * @var ReservationClientFactoryInterface
     */
    private $reservationClientFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ReservationClientUpdaterService constructor.
     * @param ReservationClientRepositoryInterface $reservationClientRepository
     * @param ReservationClientFactoryInterface $reservationClientFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        ReservationClientRepositoryInterface $reservationClientRepository,
        ReservationClientFactoryInterface $reservationClientFactory,
        LoggerInterface $logger
    ) {
        $this->reservationClientRepository = $reservationClientRepository;
        $this->reservationClientFactory = $reservationClientFactory;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function update(Reservation $reservation, array $clientData)
    {
        try {
            if (!$reservation->hasClient()) {
                $clientData['reservationId'] = $reservation->getId();

                $reservationClient = $this->reservationClientFactory->create($clientData);

                $isNew = true;
            } else {
                $reservationClient = $reservation->getClient();

                $reservationClient->setEmail($clientData['email']);
                $reservationClient->setName($clientData['name']);
                $reservationClient->setSurname($clientData['surname']);
                $reservationClient->setPhone($clientData['phone']);

                $isNew = false;
            }

            $this->reservationClientRepository->saveOne($reservationClient, $isNew);
        } catch (\Exception $exception) {
            $this->logger->error($exception);

            throw new ReservationClientUpdateFailure('Error occurred during reservation client update');
        }
    }
}
