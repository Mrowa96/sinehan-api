<?php

namespace Infrastructure\Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Enum\ReservationStatus;
use Domain\Reservation\Exception\ReservationClientUpdateFailure;
use Domain\Reservation\Exception\ReservationSeatsUpdateFailure;
use Domain\Reservation\Exception\ReservationTicketsUpdateFailure;
use Domain\Reservation\Exception\ReservationUpdateFailure;
use Domain\Reservation\Repository\ReservationRepositoryInterface;
use Domain\Reservation\Service\ReservationClientUpdaterServiceInterface;
use Domain\Reservation\Service\ReservationSeatsUpdaterServiceInterface;
use Domain\Reservation\Service\ReservationTicketsUpdaterServiceInterface;
use Domain\Reservation\Service\ReservationUpdaterServiceInterface;
use Domain\ReservationClient\Repository\ReservationClientRepositoryInterface;
use Domain\ReservationSeat\Repository\ReservationSeatRepositoryInterface;
use Domain\ReservationTicket\Entity\ReservationTicket;
use Domain\ReservationTicket\Repository\ReservationTicketRepositoryInterface;

/**
 * Class ReservationUpdaterService
 * @package Infrastructure\Domain\Reservation\Service
 */
final class ReservationUpdaterService implements ReservationUpdaterServiceInterface
{
    /**
     * @var ReservationTicketsUpdaterServiceInterface
     */
    private $reservationTicketsUpdaterService;

    /**
     * @var ReservationSeatsUpdaterServiceInterface
     */
    private $reservationSeatsUpdaterService;

    /**
     * @var ReservationClientUpdaterServiceInterface
     */
    private $reservationClientUpdaterService;

    /**
     * @var ReservationRepositoryInterface
     */
    private $reservationRepository;

    /**
     * @var ReservationSeatRepositoryInterface
     */
    private $reservationSeatRepository;

    /**
     * @var ReservationTicketRepositoryInterface
     */
    private $reservationTicketRepository;

    /**
     * @var ReservationClientRepositoryInterface
     */
    private $reservationClientRepository;

    /**
     * ReservationUpdaterService constructor.
     * @param ReservationTicketsUpdaterServiceInterface $reservationTicketsUpdaterService
     * @param ReservationSeatsUpdaterServiceInterface $reservationSeatsUpdaterService
     * @param ReservationClientUpdaterServiceInterface $reservationClientUpdaterService
     * @param ReservationRepositoryInterface $reservationRepository
     * @param ReservationSeatRepositoryInterface $reservationSeatRepository
     * @param ReservationTicketRepositoryInterface $reservationTicketRepository
     * @param ReservationClientRepositoryInterface $reservationClientRepository
     */
    public function __construct(
        ReservationTicketsUpdaterServiceInterface $reservationTicketsUpdaterService,
        ReservationSeatsUpdaterServiceInterface $reservationSeatsUpdaterService,
        ReservationClientUpdaterServiceInterface $reservationClientUpdaterService,
        ReservationRepositoryInterface $reservationRepository,
        ReservationSeatRepositoryInterface $reservationSeatRepository,
        ReservationTicketRepositoryInterface $reservationTicketRepository,
        ReservationClientRepositoryInterface $reservationClientRepository
    ) {
        $this->reservationTicketsUpdaterService = $reservationTicketsUpdaterService;
        $this->reservationSeatsUpdaterService = $reservationSeatsUpdaterService;
        $this->reservationClientUpdaterService = $reservationClientUpdaterService;
        $this->reservationRepository = $reservationRepository;
        $this->reservationSeatRepository = $reservationSeatRepository;
        $this->reservationTicketRepository = $reservationTicketRepository;
        $this->reservationClientRepository = $reservationClientRepository;
    }

    /**
     * @param Reservation $reservation
     * @param array $data
     */
    public function update(Reservation $reservation, array $data): void
    {
        if ($reservation->getStatus() === ReservationStatus::COMPLETE) {
            throw new ReservationUpdateFailure('Reservation is completed');
        }

        try {
            if (array_key_exists('tickets', $data)) {
                $this->handleTickets($reservation, $data);
            }

            if (array_key_exists('seats', $data)) {
                $this->handleSeats($reservation, $data);
            }

            if (array_key_exists('agreements', $data)) {
                $this->handleAgreements($reservation, $data);
            }

            if (array_key_exists('clientData', $data)) {
                $this->handleClientData($reservation, $data);
            }

            if (array_key_exists('complete', $data)) {
                $this->handleComplete($reservation);
            }
        } catch (ReservationSeatsUpdateFailure|
        ReservationTicketsUpdateFailure|
        ReservationClientUpdateFailure|
        \Exception $exception
        ) {
            throw new ReservationUpdateFailure($exception->getMessage());
        }
    }

    /**
     * @param Reservation $reservation
     * @param array $data
     */
    private function handleTickets(Reservation $reservation, array $data): void
    {
        $this->reservationTicketsUpdaterService->update($reservation, $data['tickets']);
        $this->reservationSeatRepository->removeFewByReservationId($reservation->getId());
    }

    /**
     * @param Reservation $reservation
     * @param array $data
     * @throws ReservationUpdateFailure
     */
    private function handleSeats(Reservation $reservation, array $data): void
    {
        $reservationTickets = $this->reservationTicketRepository->findFewByReservationId($reservation->getId());
        $ticketsQuantity = array_reduce(
            $reservationTickets,
            function (int $carry, ReservationTicket $reservationTicket) {
                $carry += $reservationTicket->getQuantity();

                return $carry;
            },
            0
        );

        if ($ticketsQuantity < count($data['seats'])) {
            throw new ReservationUpdateFailure('Too much seats were chosen');
        }

        $this->reservationSeatsUpdaterService->update($reservation, $data['seats']);
    }

    /**
     * @param Reservation $reservation
     * @param array $data
     * @throws ReservationUpdateFailure
     */
    private function handleClientData(Reservation $reservation, array $data): void
    {
        if ($reservation->isDataProcessingAccepted()) {
            $this->reservationClientUpdaterService->update($reservation, $data['clientData']);
        } else {
            throw new ReservationUpdateFailure(
                'Data processing agreement must be accepted in order to save client data'
            );
        }
    }

    /**
     * @param Reservation $reservation
     * @param array $data
     * @throws \Exception
     */
    private function handleAgreements(Reservation $reservation, array $data): void
    {
        $reservation->setDataProcessingAccepted($data['agreements']['dataProcessing']);

        if ($data['agreements']['dataProcessing'] === false) {
            $this->reservationClientRepository->clearOneByReservationId($reservation->getId());
        }

        $this->saveReservation($reservation);
    }

    /**
     * @param Reservation $reservation
     * @throws ReservationUpdateFailure
     * @throws \Exception
     */
    private function handleComplete(Reservation $reservation): void
    {
        if (!$reservation->hasTickets()) {
            throw new ReservationUpdateFailure('Tickets was not chosen');
        }

        if (!$reservation->hasSeats()) {
            throw new ReservationUpdateFailure('Seats was not chosen');
        }

        if (!$reservation->isDataProcessingAccepted()) {
            throw new ReservationUpdateFailure('Data processing agreement must be accepted');
        }

        if (!$reservation->hasClient()) {
            throw new ReservationUpdateFailure('Client for reservation does not exists');
        }

        if (!$reservation->hasClientWithInformation()) {
            throw new ReservationUpdateFailure('Not all client information were provided');
        }

        $reservation->setStatus(ReservationStatus::COMPLETE);

        $this->saveReservation($reservation);
    }

    /**
     * @param Reservation $reservation
     * @throws \Exception
     */
    private function saveReservation(Reservation $reservation): void
    {
        $reservation->setUpdateDate(new \DateTimeImmutable());

        $this->reservationRepository->saveOne($reservation);
    }
}
