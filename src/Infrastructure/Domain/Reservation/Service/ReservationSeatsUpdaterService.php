<?php

namespace Infrastructure\Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Exception\ReservationSeatsUpdateFailure;
use Domain\Reservation\Service\ReservationSeatsUpdaterServiceInterface;
use Domain\ReservationSeat\Entity\ReservationSeat;
use Domain\ReservationSeat\Factory\ReservationSeatFactoryInterface;
use Domain\ReservationSeat\Repository\ReservationSeatRepositoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ReservationSeatsUpdaterService
 * @package Infrastructure\Domain\Reservation\Service
 */
final class ReservationSeatsUpdaterService implements ReservationSeatsUpdaterServiceInterface
{
    /**
     * @var ReservationSeatRepositoryInterface
     */
    private $reservationSeatRepository;

    /**
     * @var ReservationSeatFactoryInterface
     */
    private $reservationSeatFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ReservationSeatsUpdaterService constructor.
     * @param ReservationSeatRepositoryInterface $reservationSeatRepository
     * @param ReservationSeatFactoryInterface $reservationSeatFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        ReservationSeatRepositoryInterface $reservationSeatRepository,
        ReservationSeatFactoryInterface $reservationSeatFactory,
        LoggerInterface $logger
    ) {
        $this->reservationSeatRepository = $reservationSeatRepository;
        $this->reservationSeatFactory = $reservationSeatFactory;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function update(Reservation $reservation, array $seatsData)
    {
        foreach ($seatsData as $seatData) {
            if (!is_null($this->reservationSeatRepository->findOneByParameters(
                $reservation->getId(),
                $reservation->getShow()->getId(),
                $reservation->getTime()->format('H:i:s'),
                $seatData['row'],
                $seatData['column']
            ))
            ) {
                throw new ReservationSeatsUpdateFailure('Seat is taken');
            }
        }

        try {
            $existingReservationSeats = $this->reservationSeatRepository->findFewByReservationId($reservation->getId());
            $reservationSeats = [];

            foreach ($seatsData as $seatData) {
                $seatData['reservationId'] = $reservation->getId();

                $reservationSeats[] = $this->reservationSeatFactory->create($seatData);
            }

            if (count($reservationSeats)) {
                $this->reservationSeatRepository->saveFew($reservationSeats, true);
            }

            if (count($existingReservationSeats) > 0) {
                $this->reservationSeatRepository->removeFew(
                    array_map(function (ReservationSeat $reservationSeat) {
                        return $reservationSeat->getId();
                    }, $existingReservationSeats)
                );
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception);

            throw new ReservationSeatsUpdateFailure('Error occurred during reservation seats update');
        }
    }
}
