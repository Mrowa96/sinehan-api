<?php

namespace Infrastructure\Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Exception\ReservationTicketsUpdateFailure;
use Domain\Reservation\Service\ReservationTicketsUpdaterServiceInterface;
use Domain\ReservationTicket\Entity\ReservationTicket;
use Domain\ReservationTicket\Factory\ReservationTicketFactoryInterface;
use Domain\ReservationTicket\Repository\ReservationTicketRepositoryInterface;
use Domain\Ticket\Repository\TicketRepositoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ReservationTicketsUpdaterService
 * @package Infrastructure\Domain\Reservation\Service
 */
final class ReservationTicketsUpdaterService implements ReservationTicketsUpdaterServiceInterface
{
    /**
     * @var ReservationTicketRepositoryInterface
     */
    private $reservationTicketRepository;

    /**
     * @var ReservationTicketFactoryInterface
     */
    private $reservationTicketFactory;

    /**
     * @var TicketRepositoryInterface
     */
    private $ticketRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ReservationTicketsUpdaterService constructor.
     * @param ReservationTicketRepositoryInterface $reservationTicketRepository
     * @param ReservationTicketFactoryInterface $reservationTicketFactory
     * @param TicketRepositoryInterface $ticketRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        ReservationTicketRepositoryInterface $reservationTicketRepository,
        ReservationTicketFactoryInterface $reservationTicketFactory,
        TicketRepositoryInterface $ticketRepository,
        LoggerInterface $logger
    ) {
        $this->reservationTicketRepository = $reservationTicketRepository;
        $this->reservationTicketFactory = $reservationTicketFactory;
        $this->ticketRepository = $ticketRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function update(Reservation $reservation, array $ticketsData): void
    {
        try {
            $reservationTickets = $this->reservationTicketRepository->findFewByReservationId($reservation->getId());

            foreach ($ticketsData as $ticketData) {
                $existingReservationTicket = null;

                foreach ($reservationTickets as $index => $reservationTicket) {
                    if ($reservationTicket->getTicket()->getId() === $ticketData['id']) {
                        $existingReservationTicket = $reservationTicket;
                        unset($reservationTickets[$index]);

                        break;
                    }
                }

                if ($existingReservationTicket) {
                    if ($existingReservationTicket->getQuantity() !== $ticketData['quantity']) {
                        $existingReservationTicket->setQuantity($ticketData['quantity']);

                        $this->reservationTicketRepository->saveOne($existingReservationTicket);
                    }
                } else {
                    $ticket = $this->ticketRepository->findOne($ticketData['id']);

                    if ($ticket) {
                        $ticketData['price'] = $ticket->getPrice();
                    }

                    $ticketData['reservationId'] = $reservation->getId();
                    $ticketData['ticketId'] = $ticketData['id'];

                    unset($ticketData['id']);

                    $this->reservationTicketRepository->saveOne(
                        $this->reservationTicketFactory->create($ticketData),
                        true
                    );
                }
            }

            if (!empty($reservationTickets)) {
                $this->reservationTicketRepository->removeFew(
                    array_map(function (ReservationTicket $reservationTicket) {
                        return $reservationTicket->getId();
                    }, $reservationTickets)
                );
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception);

            throw new ReservationTicketsUpdateFailure('Error occurred during reservation tickets update');
        }
    }
}
