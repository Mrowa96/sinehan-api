<?php

namespace Infrastructure\Domain\Reservation\Service;

use Domain\Reservation\Repository\ReservationRepositoryInterface;
use Domain\Reservation\Service\NewReservationValidatorServiceInterface;
use Domain\Show\Repository\ShowRepositoryInterface;

/**
 * Class NewReservationValidatorService
 * @package Infrastructure\Domain\Reservation\Service
 */
final class NewReservationValidatorService implements NewReservationValidatorServiceInterface
{
    private const MAX_QUANTITY_PER_IP = 3;

    /**
     * @var ShowRepositoryInterface
     */
    private $showRepository;

    /**
     * @var ReservationRepositoryInterface
     */
    private $reservationRepository;

    /**
     * @var string[]
     */
    private $errors;

    /**
     * NewsReservationValidatorService constructor.
     * @param ShowRepositoryInterface $showRepository
     * @param ReservationRepositoryInterface $reservationRepository
     */
    public function __construct(
        ShowRepositoryInterface $showRepository,
        ReservationRepositoryInterface $reservationRepository
    ) {
        $this->showRepository = $showRepository;
        $this->reservationRepository = $reservationRepository;
        $this->errors = [];
    }

    /**
     * @inheritdoc
     */
    public function validate(array $data): bool
    {
        $show = $this->showRepository->findOne($data['showId']);

        if ($show) {
            $showDateTime = new \DateTime($show->getDate()->format('Y-m-d') . ' ' . $data['time']);
            $currentDataTime = !empty($data['currentDate']) ? $data['currentDate'] : new \DateTime();

            if (!in_array($data['time'], $show->getStartTimes())) {
                $this->addError('Show has not start at given time');

                return false;
            }

            if ($showDateTime < $currentDataTime) {
                $this->addError('Cannot reserve show with past date');

                return false;
            }

            if ($showDateTime->sub(new \DateInterval('PT30M')) < $currentDataTime) {
                $this->addError('Cannot reserve show 30 minutes before start');

                return false;
            }

            if (count($this->reservationRepository->findFewByIpAddressAndCreateDate(
                $data['ipAddress'],
                $currentDataTime
            )) >= self::MAX_QUANTITY_PER_IP
            ) {
                $this->addError('Creating more than 3 reservations per day is not allowed');

                return false;
            }

            return true;
        } else {
            $this->addError('Show does not exists');
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function hasErrors(): bool
    {
        return !empty($this->errors);
    }

    /**
     * @inheritdoc
     */
    public function getErrorsAsString(): string
    {
        return implode(", ", $this->errors);
    }

    /**
     * @param string $error
     */
    private function addError(string $error): void
    {
        $this->errors[] = $error;
    }
}
