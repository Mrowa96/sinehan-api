<?php

namespace Infrastructure\Domain\Reservation\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Factory\ReservationFactoryInterface;
use Domain\Reservation\Exception\CreateReservationException;
use Domain\Show\Entity\Show;

/**
 * Class ReservationFactory
 * @package Infrastructure\Domain\Reservation\Factory
 */
final class ReservationFactory implements ReservationFactoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ReservationFactory constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritdoc
     */
    public function create(array $data): Reservation
    {
        try {
            $reservation = new Reservation();

            /** @var Show $show */
            $show = $this->entityManager->getReference(Show::class, $data['showId']);

            $reservation->setShow($show);
            $reservation->setTime(new \DateTimeImmutable($data['time']));
            $reservation->setIpAddress($data['ipAddress']);

            return $reservation;
        } catch (\Exception $exception) {
            throw new CreateReservationException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
