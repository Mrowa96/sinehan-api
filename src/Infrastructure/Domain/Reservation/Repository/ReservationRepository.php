<?php

namespace Infrastructure\Domain\Reservation\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Enum\ReservationStatus;
use Domain\Reservation\Exception\InvalidReservationException;
use Domain\Reservation\Repository\ReservationRepositoryInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ReservationRepository
 * @package Infrastructure\Domain\Reservation\Repository
 */
final class ReservationRepository implements ReservationRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ReservationRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     */
    public function saveOne(Reservation $reservation, bool $isNew = false): void
    {
        $errors = $this->validator->validate($reservation);

        if ($errors->count()) {
            throw new InvalidReservationException($errors);
        }

        if ($isNew) {
            $this->entityManager->persist($reservation);
        }

        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function saveMany(array $reservations): void
    {
        foreach ($reservations as $reservation) {
            $errors = $this->validator->validate($reservation);

            if ($errors->count()) {
                throw new InvalidReservationException($errors);
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function deleteOne(Reservation $reservation): void
    {
        if ($reservation->getStatus() !== ReservationStatus::COMPLETE) {
            $this->entityManager->remove($reservation);
            $this->entityManager->flush();
        }
    }

    /**
     * @inheritdoc
     */
    public function findOne(string $id): ?Reservation
    {
        try {
            $reservation = $this->entityManager
                ->createQueryBuilder()
                ->select('r')
                ->from(Reservation::class, 'r')
                ->where('r.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();

            return $reservation;
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function findFewByIpAddressAndCreateDate(string $ipAddress, \DateTime $date): array
    {
        $startDate = $date->setTime(0, 0, 0);
        $endDate = clone $date;
        $endDate->setTime(23, 59, 59);

        return $this->entityManager
            ->createQueryBuilder()
            ->select('r')
            ->from(Reservation::class, 'r')
            ->where('r.ipAddress = :ipAddress')
            ->andWhere('r.createDate >= :startDate')
            ->andWhere('r.createDate <= :endDate')
            ->setParameters(['ipAddress' => $ipAddress, 'startDate' => $startDate, 'endDate' => $endDate])
            ->getQuery()
            ->getResult();
    }
}
