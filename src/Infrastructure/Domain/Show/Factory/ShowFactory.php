<?php

namespace Infrastructure\Domain\Show\Factory;

use Domain\Show\Entity\Show;
use Domain\Show\Factory\ShowFactoryInterface;
use Domain\Show\Exception\CreateShowException;

/**
 * Class ShowFactory
 * @package Infrastructure\Domain\Show\Factory
 */
final class ShowFactory implements ShowFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(array $data): Show
    {
        try {
            $show = new Show();

            $show->setEvent($data['event']);
            $show->setPlace($data['place']);
            $show->setRoom($data['room']);
            $show->setDate(new \DateTime($data['date']));
            $show->setStartTimes($data['startTimes']);

            return $show;
        } catch (\Exception $exception) {
            throw new CreateShowException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
