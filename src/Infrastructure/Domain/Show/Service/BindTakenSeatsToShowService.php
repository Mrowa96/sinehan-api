<?php

namespace Infrastructure\Domain\Show\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Domain\ReservationSeat\Entity\ReservationSeat;
use Domain\ReservationSeat\Repository\ReservationSeatRepositoryInterface;
use Domain\Show\Entity\Show;
use Domain\Show\Service\BindTakenSeatsToShowServiceInterface;
use Domain\TakenSeat\Model\TakenSeat;

/**
 * Class BindTakenSeatsToShowService
 * @package Infrastructure\Domain\Show\Service
 */
final class BindTakenSeatsToShowService implements BindTakenSeatsToShowServiceInterface
{
    /**
     * @var ReservationSeatRepositoryInterface
     */
    private $reservationSeatRepository;

    /**
     * BindTakenSeatsToShowService constructor.
     * @param ReservationSeatRepositoryInterface $reservationSeatRepository
     */
    public function __construct(ReservationSeatRepositoryInterface $reservationSeatRepository)
    {
        $this->reservationSeatRepository = $reservationSeatRepository;
    }

    /**
     * @inheritdoc
     */
    public function bind(Show $show, \DateTimeImmutable $time): Show
    {
        $reservationSeats = $this->reservationSeatRepository->findFewByShowIdAndTime(
            $show->getId(),
            $time->format('H:i:s')
        );

        $takenSeats = array_reduce(
            $reservationSeats,
            function (ArrayCollection $collection, ReservationSeat $reservationSeat) {
                $collection->add(new TakenSeat($reservationSeat->getSeatRow(), $reservationSeat->getSeatColumn()));

                return $collection;
            },
            new ArrayCollection()
        );

        $show->setTakenSeats($takenSeats);

        return $show;
    }
}
