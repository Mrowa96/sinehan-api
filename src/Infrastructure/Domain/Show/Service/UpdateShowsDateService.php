<?php

namespace Infrastructure\Domain\Show\Service;

use Domain\Show\Entity\Show;
use Domain\Show\Service\UpdateShowsDateServiceInterface;

/**
 * Class UpdateShowsDateService
 * @package Infrastructure\Domain\Show\Service
 */
final class UpdateShowsDateService implements UpdateShowsDateServiceInterface
{
    /**
     * @inheritdoc
     */
    public function update(array &$shows): array
    {
        $this->sortByDate($shows);

        if (!empty($shows) && !empty($shows[0])) {
            $dateDifference = $this->getDateDifference($shows[0], new \DateTime());

            /** @var Show $show */
            foreach ($shows as &$show) {
                $show->setDate($show->getDate()->add($dateDifference));
            }
        }

        return $shows;
    }

    /**
     * @param Show[] $shows
     * @return void
     */
    private function sortByDate(array &$shows): void
    {
        usort($shows, function (Show $a, Show $b) {
            if ($a->getDate() == $b->getDate()) {
                return 0;
            }

            return $a->getDate() < $b->getDate() ? -1 : 1;
        });
    }

    /**
     * @param Show $show
     * @param \DateTime $currentDate
     * @return \DateInterval
     * @throws \Exception
     */
    private function getDateDifference(Show $show, \DateTime $currentDate): \DateInterval
    {
        $dateDifference = $currentDate->diff($show->getDate());

        if ($dateDifference) {
            $dateDifference->invert = 0;
        } else {
            $dateDifference = new \DateInterval('P1D');
        }

        return $dateDifference;
    }
}
