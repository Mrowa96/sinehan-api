<?php

namespace Infrastructure\Domain\Show\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Domain\Event\Entity\Event;
use Domain\Show\Entity\Show;
use Domain\Show\Exception\AddShowException;
use Domain\Show\Exception\CreateShowException;
use Domain\Show\Exception\InvalidShowException;
use Domain\Show\Exception\NotFoundShowException;
use Domain\Show\Factory\ShowFactoryInterface;
use Domain\Show\Repository\ShowRepositoryInterface;
use Domain\Place\Entity\Place;
use Domain\PlaceRoom\Entity\PlaceRoom;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ShowRepository
 * @package Infrastructure\Domain\Show\Repository
 */
final class ShowRepository implements ShowRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var ShowFactoryInterface
     */
    private $showFactory;

    /**
     * ShowRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param ShowFactoryInterface $showFactory
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        ShowFactoryInterface $showFactory
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->showFactory = $showFactory;
    }

    /**
     * @inheritdoc
     */
    public function add(array $data): Show
    {
        try {
            if (!empty($data['eventId'])) {
                $data['event'] = $this->entityManager->getReference(Event::class, $data['eventId']);
            }

            if (!empty($data['placeId'])) {
                $data['place'] = $this->entityManager->getReference(Place::class, $data['placeId']);
            }

            if (!empty($data['roomId'])) {
                $data['room'] = $this->entityManager->getReference(PlaceRoom::class, $data['roomId']);
            }

            $show = $this->showFactory->create($data);

            $errors = $this->validator->validate($show);

            if ($errors->count()) {
                throw new AddShowException($errors);
            }

            $this->entityManager->persist($show);
            $this->entityManager->flush();

            return $show;
        } catch (CreateShowException | ORMException $exception) {
            throw new AddShowException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @inheritdoc
     */
    public function remove(int $id): void
    {
        $show = $this->findOne($id);

        if (!$show) {
            throw new NotFoundShowException("Cannot found show with id ${id}");
        }

        $this->entityManager->remove($show);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function save(Show $show): void
    {
        $errors = $this->validator->validate($show);

        if ($errors->count()) {
            throw new InvalidShowException($errors);
        }

        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function findOne(int $id): ?Show
    {
        try {
            $show = $this->entityManager
                ->createQueryBuilder()
                ->select('s')
                ->from(Show::class, 's')
                ->where('s.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();

            return $show;
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function findAll(): array
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('s')
            ->from(Show::class, 's')
            ->getQuery()
            ->getResult();
    }
}
