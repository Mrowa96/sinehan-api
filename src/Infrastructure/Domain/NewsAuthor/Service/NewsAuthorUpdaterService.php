<?php

namespace Infrastructure\Domain\NewsAuthor\Service;

use Domain\NewsAuthor\Entity\NewsAuthor;
use Domain\NewsAuthor\Service\NewsAuthorUpdaterServiceInterface;

/**
 * Class NewsAuthorUpdaterService
 * @package Infrastructure\Domain\NewsAuthor\Service
 */
final class NewsAuthorUpdaterService implements NewsAuthorUpdaterServiceInterface
{
    /**
     * @inheritdoc
     */
    public function update(NewsAuthor $newsAuthor, array $data): NewsAuthor
    {
        if (array_key_exists('name', $data)) {
            $newsAuthor->setName($data['name']);
        }

        return $newsAuthor;
    }
}
