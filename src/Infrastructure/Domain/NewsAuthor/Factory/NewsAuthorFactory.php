<?php

namespace Infrastructure\Domain\NewsAuthor\Factory;

use Domain\NewsAuthor\Entity\NewsAuthor;
use Domain\NewsAuthor\Factory\NewsAuthorFactoryInterface;
use Domain\NewsAuthor\Exception\CreateNewsAuthorException;

/**
 * Class NewsAuthorFactory
 * @package Infrastructure\Domain\NewsAuthor\Factory
 */
final class NewsAuthorFactory implements NewsAuthorFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(array $data): NewsAuthor
    {
        try {
            $newsAuthor = new NewsAuthor();

            $newsAuthor->setName($data['name']);

            return $newsAuthor;
        } catch (\Exception $exception) {
            throw new CreateNewsAuthorException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
