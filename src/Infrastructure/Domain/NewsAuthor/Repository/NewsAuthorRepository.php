<?php

namespace Infrastructure\Domain\NewsAuthor\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Domain\NewsAuthor\Entity\NewsAuthor;
use Domain\NewsAuthor\Exception\AddNewsAuthorException;
use Domain\NewsAuthor\Exception\CreateNewsAuthorException;
use Domain\NewsAuthor\Exception\NotFoundNewsAuthorException;
use Domain\NewsAuthor\Exception\UpdateNewsAuthorException;
use Domain\NewsAuthor\Factory\NewsAuthorFactoryInterface;
use Domain\NewsAuthor\Repository\NewsAuthorRepositoryInterface;
use Domain\NewsAuthor\Service\NewsAuthorUpdaterServiceInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class NewsAuthorRepository
 * @package Infrastructure\Domain\NewsAuthor\Repository
 */
final class NewsAuthorRepository implements NewsAuthorRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var NewsAuthorFactoryInterface
     */
    private $newsAuthorFactory;

    /**
     * @var NewsAuthorUpdaterServiceInterface
     */
    private $newsAuthorUpdaterService;

    /**
     * NewsAuthorRepository constructor.
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param NewsAuthorFactoryInterface $newsAuthorFactory
     * @param NewsAuthorUpdaterServiceInterface $newsAuthorUpdaterService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        NewsAuthorFactoryInterface $newsAuthorFactory,
        NewsAuthorUpdaterServiceInterface $newsAuthorUpdaterService
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->newsAuthorFactory = $newsAuthorFactory;
        $this->newsAuthorUpdaterService = $newsAuthorUpdaterService;
    }

    /**
     * @inheritdoc
     */
    public function add(array $data): NewsAuthor
    {
        try {
            $newsAuthor = $this->newsAuthorFactory->create($data);

            $errors = $this->validator->validate($newsAuthor);

            if ($errors->count()) {
                throw new AddNewsAuthorException($errors);
            }

            $this->entityManager->persist($newsAuthor);
            $this->entityManager->flush();

            return $newsAuthor;
        } catch (CreateNewsAuthorException | ORMException $exception) {
            throw new AddNewsAuthorException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @inheritdoc
     */
    public function update(int $id, array $data): NewsAuthor
    {
        $newsAuthor = $this->findOne($id);

        if (!$newsAuthor) {
            throw new NotFoundNewsAuthorException("Cannot found news author with id ${id}");
        }

        $newsAuthor = $this->newsAuthorUpdaterService->update($newsAuthor, $data);
        $errors = $this->validator->validate($newsAuthor);

        if ($errors->count()) {
            throw new UpdateNewsAuthorException($errors);
        }

        $this->entityManager->flush();

        return $newsAuthor;
    }

    /**
     * @inheritdoc
     */
    public function remove(int $id): void
    {
        $newsAuthor = $this->findOne($id);

        if (!$newsAuthor) {
            throw new NotFoundNewsAuthorException("Cannot found news author with id ${id}");
        }

        $this->entityManager->remove($newsAuthor);
        $this->entityManager->flush();
    }


    /**
     * @param int $id
     * @return NewsAuthor|null
     */
    public function findOne(int $id): ?NewsAuthor
    {
        try {
            return $this->entityManager
                ->createQueryBuilder()
                ->select('na')
                ->from(NewsAuthor::class, 'na')
                ->where('na.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }
}
