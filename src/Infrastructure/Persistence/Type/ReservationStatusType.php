<?php

namespace Infrastructure\Persistence\Type;

use Domain\Reservation\Enum\ReservationStatus;

/**
 * Class ReservationStatusType
 * @package Infrastructure\Persistence\Type
 */
final class ReservationStatusType extends EnumType
{
    protected $name = 'enum_reservation_status';
    protected $values = [
        ReservationStatus::IN_PROGRESS,
        ReservationStatus::COMPLETE,
    ];
}
