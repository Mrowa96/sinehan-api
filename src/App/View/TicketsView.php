<?php

namespace App\View;

use Domain\Ticket\Entity\Ticket;

/**
 * Class TicketsView
 * @package App\View
 */
final class TicketsView implements \JsonSerializable
{
    /**
     * @var array|Ticket[]
     */
    private $tickets;

    /**
     * TicketsView constructor.
     * @param Ticket[] $tickets
     */
    public function __construct(array $tickets)
    {
        $this->tickets = $tickets;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        return array_map(function (Ticket $ticket) {
            return [
                'id' => $ticket->getId(),
                'name' => $ticket->getName(),
                'price' => $ticket->getPrice(),
            ];
        }, $this->tickets);
    }
}
