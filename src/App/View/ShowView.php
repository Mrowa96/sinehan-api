<?php

namespace App\View;

use Domain\Show\Entity\Show;

/**
 * Class ShowView
 * @package App\View
 */
final class ShowView implements \JsonSerializable
{
    /**
     * @var Show
     */
    private $show;

    /**
     * EventView constructor.
     * @param Show $show
     */
    public function __construct(Show $show)
    {
        $this->show = $show;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->show->getId(),
            'place' => [
                'id' => $this->show->getPlace()->getId(),
                'name' => $this->show->getPlace()->getName(),
                'room' => new PlaceRoomView($this->show->getRoom()),
            ],
            'date' => $this->show->getDate()->format('Y-m-d'),
            'startTimes' => $this->show->getStartTimes()
        ];
    }
}
