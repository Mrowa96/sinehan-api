<?php

namespace App\View;

use Domain\Place\Entity\Place;

/**
 * Class PlacesView
 * @package App\View
 */
final class PlacesView implements \JsonSerializable
{
    /**
     * @var Place[]
     */
    private $places;

    /**
     * PlacesView constructor.
     * @param Place[] $places
     */
    public function __construct(array $places)
    {
        $this->places = $places;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        return array_map(function (Place $place) {
            return new PlaceView($place);
        }, $this->places);
    }
}
