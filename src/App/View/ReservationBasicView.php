<?php

namespace App\View;

use Domain\Reservation\Entity\Reservation;
use Domain\TakenSeat\Model\TakenSeat;

/**
 * Class ReservationBasicView
 * @package App\View
 */
final class ReservationBasicView implements \JsonSerializable
{
    /**
     * @var Reservation
     */
    private $reservation;

    /**
     * ReservationBasicView constructor.
     * @param Reservation $reservation
     */
    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->reservation->getId(),
            'eventId' => $this->reservation->getShow()->getEvent()->getId(),
            'showId' => $this->reservation->getShow()->getId(),
            'date' => $this->reservation->getShow()->getDate()->format('Y-m-d'),
            'startTime' => $this->reservation->getTime()->format('H:i'),
            'place' => [
                'id' => $this->reservation->getShow()->getPlace()->getId(),
                'name' => $this->reservation->getShow()->getPlace()->getName(),
                'room' => new PlaceRoomView($this->reservation->getShow()->getRoom())
            ],
            'takenSeats' => array_map(function (TakenSeat $takenSeat) {
                return [
                    'row' => $takenSeat->getRow(),
                    'column' => $takenSeat->getColumn(),
                ];
            }, $this->reservation->getShow()->getTakenSeats()),
            'status' => $this->reservation->getStatus()
        ];
    }
}
