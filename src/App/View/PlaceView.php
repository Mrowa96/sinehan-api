<?php

namespace App\View;

use Domain\Place\Entity\Place;
use Domain\PlaceRoom\Entity\PlaceRoom;

/**
 * Class PlaceView
 * @package App\View
 */
final class PlaceView implements \JsonSerializable
{
    /**
     * @var Place
     */
    private $place;

    /**
     * PlaceView constructor.
     * @param Place $place
     */
    public function __construct(Place $place)
    {
        $this->place = $place;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->place->getId(),
            'name' => $this->place->getName(),
            'coords' => [
                'latitude' => $this->place->getLatitude(),
                'longitude' => $this->place->getLongitude(),
            ],
            'rooms' => array_map(function (PlaceRoom $placeRoom) {
                return new PlaceRoomView($placeRoom);
            }, $this->place->getRooms())
        ];
    }
}
