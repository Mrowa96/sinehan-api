<?php

namespace App\View;

use Domain\PlaceRoom\Entity\PlaceRoom;

/**
 * Class PlaceRoomView
 * @package App\View
 */
final class PlaceRoomView implements \JsonSerializable
{
    /**
     * @var PlaceRoom
     */
    private $placeRoom;

    /**
     * PlaceRoomView constructor.
     * @param PlaceRoom $placeRoom
     */
    public function __construct(PlaceRoom $placeRoom)
    {
        $this->placeRoom = $placeRoom;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->placeRoom->getId(),
            'name' => $this->placeRoom->getName(),
            'seats' => [
                'rows' => $this->placeRoom->getSeatsRows(),
                'columns' => $this->placeRoom->getSeatsColumns(),
            ]
        ];
    }
}
