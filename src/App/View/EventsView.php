<?php

namespace App\View;

use Domain\Event\Entity\Event;

/**
 * Class EventsView
 * @package App\View
 */
final class EventsView implements \JsonSerializable
{
    /**
     * @var Event[]
     */
    private $events;

    /**
     * EventsView constructor.
     * @param Event[] $events
     */
    public function __construct(array $events)
    {
        $this->events = $events;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        return array_map(function (Event $event) {
            return new EventView($event);
        }, $this->events);
    }
}
