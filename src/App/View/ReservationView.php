<?php

namespace App\View;

use Domain\Reservation\Entity\Reservation;
use Domain\ReservationSeat\Entity\ReservationSeat;
use Domain\ReservationTicket\Entity\ReservationTicket;
use Domain\TakenSeat\Model\TakenSeat;

/**
 * Class ReservationView
 * @package App\View
 */
final class ReservationView implements \JsonSerializable
{
    /**
     * @var Reservation
     */
    private $reservation;

    /**
     * ReservationView constructor.
     * @param Reservation $reservation
     */
    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->reservation->getId(),
            'event' => new EventView($this->reservation->getShow()->getEvent()),
            'date' => $this->reservation->getShow()->getDate()->format('Y-m-d'),
            'time' => $this->reservation->getTime()->format('H:i'),
            'place' => [
                'id' => $this->reservation->getShow()->getPlace()->getId(),
                'name' => $this->reservation->getShow()->getPlace()->getName(),
                'room' => new PlaceRoomView($this->reservation->getShow()->getRoom())
            ],
            'tickets' => array_map(function (ReservationTicket $reservationTicket) {
                return [
                    'name' => $reservationTicket->getTicket()->getName(),
                    'price' => $reservationTicket->getPrice(),
                    'quantity' => $reservationTicket->getQuantity()
                ];
            }, $this->reservation->getTickets()),
            'seats' => array_map(function (ReservationSeat $reservationSeat) {
                return [
                    'row' => $reservationSeat->getSeatRow(),
                    'column' => $reservationSeat->getSeatColumn(),
                ];
            }, $this->reservation->getSeats()),
            'client' => $this->reservation->hasClient() ? [
                    'name' => $this->reservation->getClient()->getName(),
                    'surname' => $this->reservation->getClient()->getSurname(),
                    'email' => $this->reservation->getClient()->getEmail(),
                    'phone' => $this->reservation->getClient()->getPhone(),
                ] : null,
            'dataProcessingAccepted' => $this->reservation->isDataProcessingAccepted(),
            'ipAddress' => $this->reservation->getIpAddress(),
            'status' => $this->reservation->getStatus(),
            'createDate' => $this->reservation->getCreateDate()->format('Y-m-d H:i:s'),
            'updateDate' => $this->reservation->getUpdateDate()->format('Y-m-d H:i:s'),
        ];
    }
}
