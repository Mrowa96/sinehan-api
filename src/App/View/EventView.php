<?php

namespace App\View;

use Domain\Event\Entity\Event;
use Domain\Show\Entity\Show;

/**
 * Class EventView
 * @package App\View
 */
final class EventView implements \JsonSerializable
{
    /**
     * @var Event
     */
    private $event;

    /**
     * EventView constructor.
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->event->getId(),
            'title' => $this->event->getTitle(),
            'originalTitle' => $this->event->getOriginalTitle(),
            'description' => $this->event->getDescription(),
            'directorName' => $this->event->getDirectorName(),
            'production' => [
                'countries' => $this->event->getProductionCountries(),
                'year' => $this->event->getProductionYear(),
            ],
            'ageLimit' => $this->event->getAgeLimit(),
            'releaseDate' => $this->event->getReleaseDate() ?
                $this->event->getReleaseDate()->format('Y-m-d H:m') : null,
            'duration' => $this->event->getDuration(),
            'posterUrl' => $this->event->getPosterUrl(),
            'trailerUrl' => $this->event->getTrailerUrl(),
            'shows' => array_map(function (Show $show) {
                return new ShowView($show);
            }, $this->event->getShows()),
        ];
    }
}
