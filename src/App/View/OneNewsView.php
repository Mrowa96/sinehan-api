<?php

namespace App\View;

use Domain\News\Entity\News;

/**
 * Class OneNewsView
 * @package App\View
 */
final class OneNewsView implements \JsonSerializable
{
    /**
     * @var News
     */
    private $news;

    /**
     * OneNews constructor.
     * @param News $news
     */
    public function __construct(News $news)
    {
        $this->news = $news;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->news->getId(),
            'title' => $this->news->getTitle(),
            'photo' => $this->news->getPhoto(),
            'content' => $this->news->getContent(),
            'createDate' => $this->news->getCreateDate() ? $this->news->getCreateDate()->format('Y-m-d H:m') : null,
            'category' => [
                'id' => $this->news->getCategory()->getId(),
                'name' => $this->news->getCategory()->getName(),
            ],
            'author' => $this->news->hasAuthor() ? [
                'id' => $this->news->getAuthor()->getId(),
                'name' => $this->news->getAuthor()->getName(),
            ] : null,
        ];
    }
}
