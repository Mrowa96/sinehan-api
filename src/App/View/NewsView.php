<?php

namespace App\View;

use Domain\News\Entity\News;

/**
 * Class NewsView
 * @package App\View
 */
final class NewsView implements \JsonSerializable
{
    /**
     * @var News[]
     */
    private $news;

    /**
     * NewsView constructor.
     * @param News[] $news
     */
    public function __construct(array $news)
    {
        $this->news = $news;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        return array_map(function (News $news) {
            return new OneNewsView($news);
        }, $this->news);
    }
}
