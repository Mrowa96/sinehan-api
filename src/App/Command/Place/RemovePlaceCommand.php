<?php

namespace App\Command\Place;

use Domain\Place\Exception\NotFoundPlaceException;
use Domain\Place\Repository\PlaceRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class RemovePlaceCommand
 * @package App\Command\Place
 */
final class RemovePlaceCommand extends Command
{
    /**
     * @var PlaceRepositoryInterface
     */
    private $placeRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * RemovePlaceCommand constructor.
     * @param PlaceRepositoryInterface $placeRepository
     * @param LoggerInterface $logger
     */
    public function __construct(PlaceRepositoryInterface $placeRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->placeRepository = $placeRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:place:remove')
            ->setDescription('Removes one place')
            ->addArgument('id', InputArgument::REQUIRED);
    }


    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->placeRepository->remove($input->getArgument('id'));

            $this->io->success('Place removed.');
        } catch (NotFoundPlaceException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during place removal process. Check logs.');
        }
    }
}
