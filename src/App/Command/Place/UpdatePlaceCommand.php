<?php

namespace App\Command\Place;

use Domain\Place\Exception\NotFoundPlaceException;
use Domain\Place\Repository\PlaceRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class UpdatePlaceCommand
 * @package App\Command\Place
 */
final class UpdatePlaceCommand extends Command
{
    /**
     * @var PlaceRepositoryInterface
     */
    private $placeRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * UpdatePlaceCommand constructor.
     * @param PlaceRepositoryInterface $placeRepository
     * @param LoggerInterface $logger
     */
    public function __construct(PlaceRepositoryInterface $placeRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->placeRepository = $placeRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:place:update')
            ->setDescription('Updates one place')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('name', InputArgument::OPTIONAL)
            ->addArgument('latitude', InputArgument::OPTIONAL)
            ->addArgument('longitude', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Update Place Command Interactive Wizard');

        if (empty($input->getArgument('name'))) {
            $input->setArgument('name', $this->io->ask('Name'));
        }

        if (empty($input->getArgument('latitude'))) {
            $input->setArgument('latitude', $this->io->ask('Latitude'));
        }

        if (empty($input->getArgument('longitude'))) {
            $input->setArgument('longitude', $this->io->ask('Longitude'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = [];

        if (!empty($input->getArgument('name'))) {
            $data['name'] = $input->getArgument('name');
        }

        if (!empty($input->getArgument('latitude'))) {
            $data['latitude'] = $input->getArgument('latitude');
        }

        if (!empty($input->getArgument('longitude'))) {
            $data['longitude'] = $input->getArgument('longitude');
        }

        try {
            $this->placeRepository->update($input->getArgument('id'), $data);

            $this->io->success('Place updated.');
        } catch (NotFoundPlaceException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during place update process. Check logs.');
        }
    }
}
