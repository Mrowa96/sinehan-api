<?php

namespace App\Command\PlaceRoom;

use Domain\PlaceRoom\Exception\NotFoundPlaceRoomException;
use Domain\PlaceRoom\Repository\PlaceRoomRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class UpdatePlaceRoomCommand
 * @package App\Command\PlaceRoom
 */
final class UpdatePlaceRoomCommand extends Command
{
    /**
     * @var PlaceRoomRepositoryInterface
     */
    private $placeRoomRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * UpdatePlaceRoomCommand constructor.
     * @param PlaceRoomRepositoryInterface $placeRoomRepository
     * @param LoggerInterface $logger
     */
    public function __construct(PlaceRoomRepositoryInterface $placeRoomRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->placeRoomRepository = $placeRoomRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:place-room:update')
            ->setDescription('Updates one place room')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('name', InputArgument::OPTIONAL)
            ->addArgument('seatsRows', InputArgument::OPTIONAL)
            ->addArgument('seatsColumns', InputArgument::OPTIONAL)
            ->addArgument('placeId', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Update Place Room Command Interactive Wizard');

        if (empty($input->getArgument('name'))) {
            $input->setArgument('name', $this->io->ask('Name'));
        }

        if (empty($input->getArgument('seatsRows'))) {
            $input->setArgument('seatsRows', $this->io->ask('Seats rows'));
        }

        if (empty($input->getArgument('seatsColumns'))) {
            $input->setArgument('seatsColumns', $this->io->ask('Seats columns'));
        }

        if (empty($input->getArgument('placeId'))) {
            $input->setArgument('placeId', $this->io->ask('Place id'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = [];

        if (!empty($input->getArgument('name'))) {
            $data['name'] = $input->getArgument('name');
        }

        if (!empty($input->getArgument('seatsRows'))) {
            $data['seats_rows'] = $input->getArgument('seatsRows');
        }

        if (!empty($input->getArgument('seatsColumns'))) {
            $data['seats_columns'] = $input->getArgument('seatsColumns');
        }

        if (!empty($input->getArgument('placeId'))) {
            $data['place_id'] = $input->getArgument('placeId');
        }

        try {
            $this->placeRoomRepository->update($input->getArgument('id'), $data);

            $this->io->success('Place room updated.');
        } catch (NotFoundPlaceRoomException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during place room update process. Check logs.');
        }
    }
}
