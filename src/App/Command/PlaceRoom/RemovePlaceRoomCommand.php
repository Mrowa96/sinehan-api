<?php

namespace App\Command\PlaceRoom;

use Domain\PlaceRoom\Exception\NotFoundPlaceRoomException;
use Domain\PlaceRoom\Repository\PlaceRoomRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class RemovePlaceRoomCommand
 * @package App\Command\PlaceRoom
 */
final class RemovePlaceRoomCommand extends Command
{
    /**
     * @var PlaceRoomRepositoryInterface
     */
    private $placeRoomRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * RemovePlaceRoomCommand constructor.
     * @param PlaceRoomRepositoryInterface $placeRoomRepository
     * @param LoggerInterface $logger
     */
    public function __construct(PlaceRoomRepositoryInterface $placeRoomRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->placeRoomRepository = $placeRoomRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:place-room:remove')
            ->setDescription('Removes one placeRoom')
            ->addArgument('id', InputArgument::REQUIRED);
    }


    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->placeRoomRepository->remove($input->getArgument('id'));

            $this->io->success('Place room removed.');
        } catch (NotFoundPlaceRoomException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during place room removal process. Check logs.');
        }
    }
}
