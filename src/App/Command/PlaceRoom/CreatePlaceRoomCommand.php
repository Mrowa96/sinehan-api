<?php

namespace App\Command\PlaceRoom;

use Domain\PlaceRoom\Exception\AddPlaceRoomException;
use Domain\PlaceRoom\Repository\PlaceRoomRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CreatePlaceRoomCommand
 * @package App\Command\PlaceRoom
 */
final class CreatePlaceRoomCommand extends Command
{
    /**
     * @var PlaceRoomRepositoryInterface
     */
    private $placeRoomRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * CreatePlaceRoomCommand constructor.
     * @param PlaceRoomRepositoryInterface $placeRoomRepository
     * @param LoggerInterface $logger
     */
    public function __construct(PlaceRoomRepositoryInterface $placeRoomRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->placeRoomRepository = $placeRoomRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:place-room:create')
            ->setDescription('Creates new place room')
            ->addArgument('name', InputArgument::OPTIONAL)
            ->addArgument('seatsRows', InputArgument::OPTIONAL)
            ->addArgument('seatsColumns', InputArgument::OPTIONAL)
            ->addArgument('placeId', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Create Place Room Command Interactive Wizard');

        if (empty($input->getArgument('name'))) {
            $input->setArgument('name', $this->io->ask('Name'));
        }

        if (empty($input->getArgument('seatsRows'))) {
            $input->setArgument('seatsRows', $this->io->ask('Seats rows'));
        }

        if (empty($input->getArgument('seatsColumns'))) {
            $input->setArgument('seatsColumns', $this->io->ask('Seats columns'));
        }

        if (empty($input->getArgument('placeId'))) {
            $input->setArgument('placeId', $this->io->ask('Place id'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->placeRoomRepository->add([
                'name' => $input->getArgument('name'),
                'seatsRows' => $input->getArgument('seatsRows'),
                'seatsColumns' => $input->getArgument('seatsColumns'),
                'placeId' => $input->getArgument('placeId')
            ]);

            $this->io->success('Place room created.');
        } catch (AddPlaceRoomException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during place room creation. Check logs.');
        }
    }
}
