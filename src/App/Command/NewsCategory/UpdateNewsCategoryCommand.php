<?php

namespace App\Command\NewsCategory;

use Domain\NewsCategory\Exception\NotFoundNewsCategoryException;
use Domain\NewsCategory\Repository\NewsCategoryRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class UpdateNewsCategoryCommand
 * @package App\Command\NewsCategory
 */
final class UpdateNewsCategoryCommand extends Command
{
    /**
     * @var NewsCategoryRepositoryInterface
     */
    private $newsCategoryRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * UpdateNewsCategoryCommand constructor.
     * @param NewsCategoryRepositoryInterface $newsCategoryRepository
     * @param LoggerInterface $logger
     */
    public function __construct(NewsCategoryRepositoryInterface $newsCategoryRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->newsCategoryRepository = $newsCategoryRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:news-category:update')
            ->setDescription('Updates one news category')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('name', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Update News Category Command Interactive Wizard');

        if (empty($input->getArgument('name'))) {
            $input->setArgument('name', $this->io->ask('Name'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = [];

        if (!empty($input->getArgument('name'))) {
            $data['name'] = $input->getArgument('name');
        }

        try {
            $this->newsCategoryRepository->update($input->getArgument('id'), $data);

            $this->io->success('News category updated.');
        } catch (NotFoundNewsCategoryException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during news category update process. Check logs.');
        }
    }
}
