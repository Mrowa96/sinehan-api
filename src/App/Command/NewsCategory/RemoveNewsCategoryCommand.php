<?php

namespace App\Command\NewsCategory;

use Domain\NewsCategory\Exception\NotFoundNewsCategoryException;
use Domain\NewsCategory\Repository\NewsCategoryRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class RemoveNewsCategoryCommand
 * @package App\Command\NewsCategory
 */
final class RemoveNewsCategoryCommand extends Command
{
    /**
     * @var NewsCategoryRepositoryInterface
     */
    private $newsCategoryRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * RemoveNewsCategoryCommand constructor.
     * @param NewsCategoryRepositoryInterface $newsCategoryRepository
     * @param LoggerInterface $logger
     */
    public function __construct(NewsCategoryRepositoryInterface $newsCategoryRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->newsCategoryRepository = $newsCategoryRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:news-category:remove')
            ->setDescription('Removes one news category')
            ->addArgument('id', InputArgument::REQUIRED);
    }


    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->newsCategoryRepository->remove($input->getArgument('id'));

            $this->io->success('News category removed.');
        } catch (NotFoundNewsCategoryException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during news category removal process. Check logs.');
        }
    }
}
