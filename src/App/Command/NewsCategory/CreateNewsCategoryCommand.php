<?php

namespace App\Command\NewsCategory;

use Domain\NewsCategory\Exception\AddNewsCategoryException;
use Domain\NewsCategory\Repository\NewsCategoryRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CreateNewsCategoryCommand
 * @package App\Command\NewsCategory
 */
final class CreateNewsCategoryCommand extends Command
{
    /**
     * @var NewsCategoryRepositoryInterface
     */
    private $newsCategoryRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * CreateNewsCategoryCommand constructor.
     * @param NewsCategoryRepositoryInterface $newsCategoryRepository
     * @param LoggerInterface $logger
     */
    public function __construct(NewsCategoryRepositoryInterface $newsCategoryRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->newsCategoryRepository = $newsCategoryRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:news-category:create')
            ->setDescription('Creates new news category')
            ->addArgument('name', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Create News Category Command Interactive Wizard');

        if (empty($input->getArgument('name'))) {
            $input->setArgument('name', $this->io->ask('name'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->newsCategoryRepository->add(['name' => $input->getArgument('name')]);

            $this->io->success('News category created.');
        } catch (AddNewsCategoryException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during news category creation. Check logs.');
        }
    }
}
