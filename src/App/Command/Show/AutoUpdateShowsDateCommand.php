<?php

namespace App\Command\Show;

use Domain\Show\Exception\InvalidShowException;
use Domain\Show\Exception\NotFoundShowException;
use Domain\Show\Repository\ShowRepositoryInterface;
use Domain\Show\Service\UpdateShowsDateServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class UpdateShowsDateCommand
 * @package App\Command\Show
 */
final class AutoUpdateShowsDateCommand extends Command
{
    /**
     * @var ShowRepositoryInterface
     */
    private $showRepository;

    /**
     * @var UpdateShowsDateServiceInterface
     */
    private $updateShowsDateService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * RemoveShowCommand constructor.
     * @param ShowRepositoryInterface $showRepository
     * @param UpdateShowsDateServiceInterface $updateShowsDateService
     * @param LoggerInterface $logger
     */
    public function __construct(
        ShowRepositoryInterface $showRepository,
        UpdateShowsDateServiceInterface $updateShowsDateService,
        LoggerInterface $logger
    ) {
        parent::__construct();

        $this->showRepository = $showRepository;
        $this->updateShowsDateService = $updateShowsDateService;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:shows:auto-update-date')
            ->setDescription('Automatically updates date for all shows');
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $shows = $this->showRepository->findAll();

            $this->updateShowsDateService->update($shows);

            foreach ($shows as $show) {
                $this->showRepository->save($show);
            }

            $this->io->success('Shows updated.');
        } catch (InvalidShowException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during shows update process. Check logs.');
        }
    }
}
