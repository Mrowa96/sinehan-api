<?php

namespace App\Command\Show;

use Domain\Show\Exception\NotFoundShowException;
use Domain\Show\Repository\ShowRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class RemoveShowCommand
 * @package App\Command\Show
 */
final class RemoveShowCommand extends Command
{
    /**
     * @var ShowRepositoryInterface
     */
    private $showRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * RemoveShowCommand constructor.
     * @param ShowRepositoryInterface $showRepository
     * @param LoggerInterface $logger
     */
    public function __construct(ShowRepositoryInterface $showRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->showRepository = $showRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:show:remove')
            ->setDescription('Removes one show')
            ->addArgument('id', InputArgument::REQUIRED);
    }


    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->showRepository->remove($input->getArgument('id'));

            $this->io->success('Show removed.');
        } catch (NotFoundShowException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during show removal process. Check logs.');
        }
    }
}
