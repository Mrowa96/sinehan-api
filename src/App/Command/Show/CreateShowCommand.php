<?php

namespace App\Command\Show;

use Domain\Show\Exception\AddShowException;
use Domain\Show\Repository\ShowRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CreateShowCommand
 * @package App\Command\Show
 */
final class CreateShowCommand extends Command
{
    /**
     * @var ShowRepositoryInterface
     */
    private $showRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * CreateShowCommand constructor.
     * @param ShowRepositoryInterface $showRepository
     * @param LoggerInterface $logger
     */
    public function __construct(ShowRepositoryInterface $showRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->showRepository = $showRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:show:create')
            ->setDescription('Creates new show')
            ->addArgument('eventId', InputArgument::OPTIONAL)
            ->addArgument('placeId', InputArgument::OPTIONAL)
            ->addArgument('roomId', InputArgument::OPTIONAL)
            ->addArgument('date', InputArgument::OPTIONAL)
            ->addArgument('startTimes', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Create Show Command Interactive Wizard');

        if (empty($input->getArgument('eventId'))) {
            $input->setArgument('eventId', $this->io->ask('Event id'));
        }

        if (empty($input->getArgument('placeId'))) {
            $input->setArgument('placeId', $this->io->ask('Place id'));
        }

        if (empty($input->getArgument('roomId'))) {
            $input->setArgument('roomId', $this->io->ask('Room id'));
        }

        if (empty($input->getArgument('date'))) {
            $input->setArgument('date', $this->io->ask('Date'));
        }

        if (empty($input->getArgument('startTimes'))) {
            $input->setArgument('startTimes', $this->io->ask('Start times (comma separated in format: hh:mm)'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->showRepository->add([
                'eventId' => $input->getArgument('eventId'),
                'placeId' => $input->getArgument('placeId'),
                'roomId' => $input->getArgument('roomId'),
                'date' => $input->getArgument('date'),
                'startTimes' => explode(",", $input->getArgument('startTimes')),
            ]);

            $this->io->success('Show created.');
        } catch (AddShowException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during show creation. Check logs.');
        }
    }
}
