<?php

namespace App\Command\NewsAuthor;

use Domain\NewsAuthor\Exception\NotFoundNewsAuthorException;
use Domain\NewsAuthor\Repository\NewsAuthorRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class RemoveNewsAuthorCommand
 * @package App\Command\NewsAuthor
 */
final class RemoveNewsAuthorCommand extends Command
{
    /**
     * @var NewsAuthorRepositoryInterface
     */
    private $newsAuthorRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * RemoveNewsAuthorCommand constructor.
     * @param NewsAuthorRepositoryInterface $newsAuthorRepository
     * @param LoggerInterface $logger
     */
    public function __construct(NewsAuthorRepositoryInterface $newsAuthorRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->newsAuthorRepository = $newsAuthorRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:news-author:remove')
            ->setDescription('Removes one news author')
            ->addArgument('id', InputArgument::REQUIRED);
    }


    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->newsAuthorRepository->remove($input->getArgument('id'));

            $this->io->success('News author removed.');
        } catch (NotFoundNewsAuthorException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during news author removal process. Check logs.');
        }
    }
}
