<?php

namespace App\Command\NewsAuthor;

use Domain\NewsAuthor\Exception\NotFoundNewsAuthorException;
use Domain\NewsAuthor\Repository\NewsAuthorRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class UpdateNewsAuthorCommand
 * @package App\Command\NewsAuthor
 */
final class UpdateNewsAuthorCommand extends Command
{
    /**
     * @var NewsAuthorRepositoryInterface
     */
    private $newsAuthorRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * UpdateNewsAuthorCommand constructor.
     * @param NewsAuthorRepositoryInterface $newsAuthorRepository
     * @param LoggerInterface $logger
     */
    public function __construct(NewsAuthorRepositoryInterface $newsAuthorRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->newsAuthorRepository = $newsAuthorRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:news-author:update')
            ->setDescription('Updates one news author')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('name', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Update News Author Command Interactive Wizard');

        if (empty($input->getArgument('name'))) {
            $input->setArgument('name', $this->io->ask('Name'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = [];

        if (!empty($input->getArgument('name'))) {
            $data['name'] = $input->getArgument('name');
        }

        try {
            $this->newsAuthorRepository->update($input->getArgument('id'), $data);

            $this->io->success('News author updated.');
        } catch (NotFoundNewsAuthorException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during news author update process. Check logs.');
        }
    }
}
