<?php

namespace App\Command\NewsAuthor;

use Domain\NewsAuthor\Exception\AddNewsAuthorException;
use Domain\NewsAuthor\Repository\NewsAuthorRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CreateNewsAuthorCommand
 * @package App\Command\NewsAuthor
 */
final class CreateNewsAuthorCommand extends Command
{
    /**
     * @var NewsAuthorRepositoryInterface
     */
    private $newsAuthorRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * CreateNewsAuthorCommand constructor.
     * @param NewsAuthorRepositoryInterface $newsAuthorRepository
     * @param LoggerInterface $logger
     */
    public function __construct(NewsAuthorRepositoryInterface $newsAuthorRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->newsAuthorRepository = $newsAuthorRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:news-author:create')
            ->setDescription('Creates new news author')
            ->addArgument('name', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Create News Author Command Interactive Wizard');

        if (empty($input->getArgument('name'))) {
            $input->setArgument('name', $this->io->ask('name'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->newsAuthorRepository->add(['name' => $input->getArgument('name')]);

            $this->io->success('News author created.');
        } catch (AddNewsAuthorException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during news author creation. Check logs.');
        }
    }
}
