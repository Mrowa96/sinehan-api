<?php

namespace App\Command\Ticket;

use Domain\Ticket\Exception\NotFoundTicketException;
use Domain\Ticket\Repository\TicketRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class RemoveTicketCommand
 * @package App\Command\Ticket
 */
final class RemoveTicketCommand extends Command
{
    /**
     * @var TicketRepositoryInterface
     */
    private $ticketRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * RemoveTicketCommand constructor.
     * @param TicketRepositoryInterface $ticketRepository
     * @param LoggerInterface $logger
     */
    public function __construct(TicketRepositoryInterface $ticketRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->ticketRepository = $ticketRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:ticket:remove')
            ->setDescription('Removes one ticket')
            ->addArgument('id', InputArgument::REQUIRED);
    }


    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->ticketRepository->remove($input->getArgument('id'));

            $this->io->success('Ticket removed.');
        } catch (NotFoundTicketException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during ticket removal process. Check logs.');
        }
    }
}
