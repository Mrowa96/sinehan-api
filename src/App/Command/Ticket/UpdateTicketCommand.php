<?php

namespace App\Command\Ticket;

use Domain\Ticket\Exception\NotFoundTicketException;
use Domain\Ticket\Repository\TicketRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class UpdateTicketCommand
 * @package App\Command\Ticket
 */
final class UpdateTicketCommand extends Command
{
    /**
     * @var TicketRepositoryInterface
     */
    private $ticketRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * UpdateTicketCommand constructor.
     * @param TicketRepositoryInterface $ticketRepository
     * @param LoggerInterface $logger
     */
    public function __construct(TicketRepositoryInterface $ticketRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->ticketRepository = $ticketRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:ticket:update')
            ->setDescription('Updates one ticket')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('name', InputArgument::OPTIONAL)
            ->addArgument('price', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Update Ticket Command Interactive Wizard');

        if (empty($input->getArgument('name'))) {
            $input->setArgument('name', $this->io->ask('Name'));
        }

        if (empty($input->getArgument('price'))) {
            $input->setArgument('price', $this->io->ask('Price'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = [];

        if (!empty($input->getArgument('name'))) {
            $data['name'] = $input->getArgument('name');
        }

        if (!empty($input->getArgument('price'))) {
            $data['price'] = $input->getArgument('price');
        }

        try {
            $this->ticketRepository->update($input->getArgument('id'), $data);

            $this->io->success('Ticket updated.');
        } catch (NotFoundTicketException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during ticket update process. Check logs.');
        }
    }
}
