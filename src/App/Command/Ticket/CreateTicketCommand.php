<?php

namespace App\Command\Ticket;

use Domain\Ticket\Exception\AddTicketException;
use Domain\Ticket\Repository\TicketRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CreateTicketCommand
 * @package App\Command\Ticket
 */
final class CreateTicketCommand extends Command
{
    /**
     * @var TicketRepositoryInterface
     */
    private $ticketRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * CreateTicketCommand constructor.
     * @param TicketRepositoryInterface $ticketRepository
     * @param LoggerInterface $logger
     */
    public function __construct(TicketRepositoryInterface $ticketRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->ticketRepository = $ticketRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:ticket:create')
            ->setDescription('Creates new ticket')
            ->addArgument('name', InputArgument::OPTIONAL)
            ->addArgument('price', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Create Ticket Command Interactive Wizard');

        if (empty($input->getArgument('name'))) {
            $input->setArgument('name', $this->io->ask('Name'));
        }

        if (empty($input->getArgument('price'))) {
            $input->setArgument('price', $this->io->ask('Price'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->ticketRepository->add([
                'name' => $input->getArgument('name'),
                'price' => $input->getArgument('price'),
            ]);

            $this->io->success('Ticket created.');
        } catch (AddTicketException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during ticket creation. Check logs.');
        }
    }
}
