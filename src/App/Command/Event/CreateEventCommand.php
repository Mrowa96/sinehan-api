<?php

namespace App\Command\Event;

use Domain\Event\Exception\AddEventException;
use Domain\Event\Repository\EventRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CreateEventCommand
 * @package App\Command\Event
 */
final class CreateEventCommand extends Command
{
    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * CreateEventCommand constructor.
     * @param EventRepositoryInterface $eventRepository
     * @param LoggerInterface $logger
     */
    public function __construct(EventRepositoryInterface $eventRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->eventRepository = $eventRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:event:create')
            ->setDescription('Creates new event')
            ->addArgument('title', InputArgument::OPTIONAL)
            ->addArgument('originalTitle', InputArgument::OPTIONAL)
            ->addArgument('description', InputArgument::OPTIONAL)
            ->addArgument('directorName', InputArgument::OPTIONAL)
            ->addArgument('productionCountries', InputArgument::OPTIONAL)
            ->addArgument('productionYear', InputArgument::OPTIONAL)
            ->addArgument('ageLimit', InputArgument::OPTIONAL)
            ->addArgument('releaseDate', InputArgument::OPTIONAL)
            ->addArgument('duration', InputArgument::OPTIONAL)
            ->addArgument('posterUrl', InputArgument::OPTIONAL)
            ->addArgument('trailerUrl', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Create Event Command Interactive Wizard');

        if (empty($input->getArgument('title'))) {
            $input->setArgument('title', $this->io->ask('Ttile'));
        }

        if (empty($input->getArgument('originalTitle'))) {
            $input->setArgument('originalTitle', $this->io->ask('Original title'));
        }

        if (empty($input->getArgument('description'))) {
            $input->setArgument('description', $this->io->ask('Description'));
        }

        if (empty($input->getArgument('directorName'))) {
            $input->setArgument('directorName', $this->io->ask('Director name'));
        }

        if (empty($input->getArgument('productionCountries'))) {
            $input->setArgument(
                'productionCountries',
                explode(",", $this->io->ask('Production countries (comma separated)'))
            );
        }

        if (empty($input->getArgument('productionYear'))) {
            $input->setArgument('productionYear', $this->io->ask('Production year'));
        }

        if (empty($input->getArgument('ageLimit'))) {
            $input->setArgument('ageLimit', $this->io->ask('Age limit'));
        }

        if (empty($input->getArgument('releaseDate'))) {
            $input->setArgument('releaseDate', $this->io->ask('Release date'));
        }

        if (empty($input->getArgument('duration'))) {
            $input->setArgument('duration', $this->io->ask('Duration'));
        }

        if (empty($input->getArgument('posterUrl'))) {
            $input->setArgument('posterUrl', $this->io->ask('Poster url'));
        }

        if (empty($input->getArgument('trailerUrl'))) {
            $input->setArgument('trailerUrl', $this->io->ask('Trailer url'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->eventRepository->add([
                'title' => $input->getArgument('title'),
                'originalTitle' => $input->getArgument('originalTitle'),
                'description' => $input->getArgument('description'),
                'directorName' => $input->getArgument('directorName'),
                'productionCountries' => $input->getArgument('productionCountries'),
                'productionYear' => $input->getArgument('productionYear'),
                'ageLimit' => $input->getArgument('ageLimit'),
                'releaseDate' => $input->getArgument('releaseDate'),
                'duration' => $input->getArgument('duration'),
                'posterUrl' => $input->getArgument('posterUrl'),
                'trailerUrl' => $input->getArgument('trailerUrl'),
            ]);

            $this->io->success('Event created.');
        } catch (AddEventException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during event creation. Check logs.');
        }
    }
}
