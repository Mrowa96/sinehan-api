<?php

namespace App\Command\Event;

use Domain\Event\Exception\NotFoundEventException;
use Domain\Event\Repository\EventRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class UpdateEventCommand
 * @package App\Command\Event
 */
final class UpdateEventCommand extends Command
{
    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * UpdateEventCommand constructor.
     * @param EventRepositoryInterface $eventRepository
     * @param LoggerInterface $logger
     */
    public function __construct(EventRepositoryInterface $eventRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->eventRepository = $eventRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:event:update')
            ->setDescription('Updates one event')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('title', InputArgument::OPTIONAL)
            ->addArgument('originalTitle', InputArgument::OPTIONAL)
            ->addArgument('description', InputArgument::OPTIONAL)
            ->addArgument('directorName', InputArgument::OPTIONAL)
            ->addArgument('productionCountries', InputArgument::OPTIONAL)
            ->addArgument('productionYear', InputArgument::OPTIONAL)
            ->addArgument('ageLimit', InputArgument::OPTIONAL)
            ->addArgument('releaseDate', InputArgument::OPTIONAL)
            ->addArgument('duration', InputArgument::OPTIONAL)
            ->addArgument('posterUrl', InputArgument::OPTIONAL)
            ->addArgument('trailerUrl', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Update Event Command Interactive Wizard');

        if (empty($input->getArgument('title'))) {
            $input->setArgument('title', $this->io->ask('Ttile'));
        }

        if (empty($input->getArgument('originalTitle'))) {
            $input->setArgument('originalTitle', $this->io->ask('Original title'));
        }

        if (empty($input->getArgument('description'))) {
            $input->setArgument('description', $this->io->ask('Description'));
        }

        if (empty($input->getArgument('directorName'))) {
            $input->setArgument('directorName', $this->io->ask('Director name'));
        }

        if (empty($input->getArgument('productionCountries'))) {
            $input->setArgument('productionCountries', explode(",", $this->io->ask('productionCountries title')));
        }

        if (empty($input->getArgument('productionYear'))) {
            $input->setArgument('productionYear', $this->io->ask('Production year'));
        }

        if (empty($input->getArgument('ageLimit'))) {
            $input->setArgument('ageLimit', $this->io->ask('Age limit'));
        }

        if (empty($input->getArgument('releaseDate'))) {
            $input->setArgument('releaseDate', $this->io->ask('Release date'));
        }

        if (empty($input->getArgument('duration'))) {
            $input->setArgument('duration', $this->io->ask('Duration'));
        }

        if (empty($input->getArgument('posterUrl'))) {
            $input->setArgument('posterUrl', $this->io->ask('Poster url'));
        }

        if (empty($input->getArgument('trailerUrl'))) {
            $input->setArgument('trailerUrl', $this->io->ask('Trailer url'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = [];

        if (!empty($input->getArgument('title'))) {
            $data['title'] = $input->getArgument('title');
        }

        if (!empty($input->getArgument('originalTitle'))) {
            $data['originalTitle'] = $input->getArgument('originalTitle');
        }

        if (!empty($input->getArgument('description'))) {
            $data['description'] = $input->getArgument('description');
        }

        if (!empty($input->getArgument('directorName'))) {
            $data['directorName'] = $input->getArgument('directorName');
        }

        if (!empty($input->getArgument('productionCountries'))) {
            $data['productionCountries'] = $input->getArgument('productionCountries');
        }

        if (!empty($input->getArgument('productionYear'))) {
            $data['productionYear'] = $input->getArgument('productionYear');
        }

        if (!empty($input->getArgument('ageLimit'))) {
            $data['ageLimit'] = $input->getArgument('ageLimit');
        }

        if (!empty($input->getArgument('releaseDate'))) {
            $data['releaseDate'] = $input->getArgument('releaseDate');
        }

        if (!empty($input->getArgument('duration'))) {
            $data['duration'] = $input->getArgument('duration');
        }

        if (!empty($input->getArgument('posterUrl'))) {
            $data['posterUrl'] = $input->getArgument('posterUrl');
        }

        if (!empty($input->getArgument('trailerUrl'))) {
            $data['trailerUrl'] = $input->getArgument('trailerUrl');
        }

        try {
            $this->eventRepository->update($input->getArgument('id'), $data);

            $this->io->success('Event updated.');
        } catch (NotFoundEventException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during event update process. Check logs.');
        }
    }
}
