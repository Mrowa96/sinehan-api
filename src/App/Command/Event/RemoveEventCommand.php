<?php

namespace App\Command\Event;

use Domain\Event\Exception\NotFoundEventException;
use Domain\Event\Repository\EventRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class RemoveEventCommand
 * @package App\Command\Event
 */
final class RemoveEventCommand extends Command
{
    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * RemoveEventCommand constructor.
     * @param EventRepositoryInterface $eventRepository
     * @param LoggerInterface $logger
     */
    public function __construct(EventRepositoryInterface $eventRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->eventRepository = $eventRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:event:remove')
            ->setDescription('Removes one event')
            ->addArgument('id', InputArgument::REQUIRED);
    }


    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->eventRepository->remove($input->getArgument('id'));

            $this->io->success('Event removed.');
        } catch (NotFoundEventException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during event removal process. Check logs.');
        }
    }
}
