<?php

namespace App\Command\News;

use Domain\News\Exception\NotFoundNewsException;
use Domain\News\Repository\NewsRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class RemoveNewsCommand
 * @package App\Command\News
 */
final class RemoveNewsCommand extends Command
{
    /**
     * @var NewsRepositoryInterface
     */
    private $newsRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * RemoveNewsCommand constructor.
     * @param NewsRepositoryInterface $newsRepository
     * @param LoggerInterface $logger
     */
    public function __construct(NewsRepositoryInterface $newsRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->newsRepository = $newsRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:news:remove')
            ->setDescription('Removes one news')
            ->addArgument('id', InputArgument::REQUIRED);
    }


    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->newsRepository->remove($input->getArgument('id'));

            $this->io->success('News removed.');
        } catch (NotFoundNewsException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during news removal process. Check logs.');
        }
    }
}
