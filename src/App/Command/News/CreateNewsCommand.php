<?php

namespace App\Command\News;

use Domain\News\Exception\AddNewsException;
use Domain\News\Repository\NewsRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CreateNewsCommand
 * @package App\Command\News
 */
final class CreateNewsCommand extends Command
{
    /**
     * @var NewsRepositoryInterface
     */
    private $newsRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * CreateNewsCommand constructor.
     * @param NewsRepositoryInterface $newsRepository
     * @param LoggerInterface $logger
     */
    public function __construct(NewsRepositoryInterface $newsRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->newsRepository = $newsRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:news:create')
            ->setDescription('Creates new news')
            ->addArgument('title', InputArgument::OPTIONAL)
            ->addArgument('photo', InputArgument::OPTIONAL)
            ->addArgument('content', InputArgument::OPTIONAL)
            ->addArgument('createDate', InputArgument::OPTIONAL)
            ->addArgument('categoryId', InputArgument::OPTIONAL)
            ->addArgument('authorId', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Create News Command Interactive Wizard');

        if (empty($input->getArgument('title'))) {
            $input->setArgument('title', $this->io->ask('Title'));
        }

        if (empty($input->getArgument('photo'))) {
            $input->setArgument('photo', $this->io->ask('Photo'));
        }

        if (empty($input->getArgument('content'))) {
            $input->setArgument('content', $this->io->ask('Content'));
        }

        if (empty($input->getArgument('createDate'))) {
            $input->setArgument('createDate', $this->io->ask('Create date'));
        }

        if (empty($input->getArgument('categoryId'))) {
            $input->setArgument('categoryId', $this->io->ask('Category id'));
        }

        if (empty($input->getArgument('authorId'))) {
            $input->setArgument('authorId', $this->io->ask('Author id'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->newsRepository->add([
                'title' => $input->getArgument('title'),
                'photo' => $input->getArgument('photo'),
                'content' => $input->getArgument('content'),
                'createDate' => $input->getArgument('createDate'),
                'categoryId' => $input->getArgument('categoryId'),
                'authorId' => $input->getArgument('authorId'),
            ]);

            $this->io->success('News created.');
        } catch (AddNewsException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during news creation. Check logs.');
        }
    }
}
