<?php

namespace App\Command\News;

use Domain\News\Exception\NotFoundNewsException;
use Domain\News\Repository\NewsRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class UpdateNewsCommand
 * @package App\Command\News
 */
final class UpdateNewsCommand extends Command
{
    /**
     * @var NewsRepositoryInterface
     */
    private $newsRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * UpdateNewsCommand constructor.
     * @param NewsRepositoryInterface $newsRepository
     * @param LoggerInterface $logger
     */
    public function __construct(NewsRepositoryInterface $newsRepository, LoggerInterface $logger)
    {
        parent::__construct();

        $this->newsRepository = $newsRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:news:update')
            ->setDescription('Updates one news')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('title', InputArgument::OPTIONAL)
            ->addArgument('photo', InputArgument::OPTIONAL)
            ->addArgument('content', InputArgument::OPTIONAL)
            ->addArgument('createDate', InputArgument::OPTIONAL)
            ->addArgument('categoryId', InputArgument::OPTIONAL)
            ->addArgument('authorId', InputArgument::OPTIONAL);
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Update News Command Interactive Wizard');

        if (empty($input->getArgument('title'))) {
            $input->setArgument('title', $this->io->ask('Title'));
        }

        if (empty($input->getArgument('photo'))) {
            $input->setArgument('photo', $this->io->ask('Photo'));
        }

        if (empty($input->getArgument('content'))) {
            $input->setArgument('content', $this->io->ask('Content'));
        }

        if (empty($input->getArgument('createDate'))) {
            $input->setArgument('createDate', $this->io->ask('Create date'));
        }

        if (empty($input->getArgument('categoryId'))) {
            $input->setArgument('categoryId', $this->io->ask('Category id'));
        }

        if (empty($input->getArgument('authorId'))) {
            $input->setArgument('authorId', $this->io->ask('Author id'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = [];

        if (!empty($input->getArgument('title'))) {
            $data['title'] = $input->getArgument('title');
        }

        if (!empty($input->getArgument('photo'))) {
            $data['photo'] = $input->getArgument('photo');
        }

        if (!empty($input->getArgument('content'))) {
            $data['content'] = $input->getArgument('content');
        }

        if (!empty($input->getArgument('createDate'))) {
            $data['createDate'] = $input->getArgument('createDate');
        }

        if (!empty($input->getArgument('categoryId'))) {
            $data['categoryId'] = $input->getArgument('categoryId');
        }

        if (!empty($input->getArgument('authorId'))) {
            $data['authorId'] = $input->getArgument('authorId');
        }

        try {
            $this->newsRepository->update($input->getArgument('id'), $data);

            $this->io->success('News updated.');
        } catch (NotFoundNewsException $exception) {
            $this->logger->error($exception);

            $this->io->error('Error during news update process. Check logs.');
        }
    }
}
