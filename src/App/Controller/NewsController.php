<?php

namespace App\Controller;

use App\View\NewsView;
use App\View\OneNewsView;
use Domain\News\Repository\NewsRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class NewsController
 * @package App\Controller
 */
final class NewsController
{
    /**
     * @var NewsRepositoryInterface
     */
    private $newsRepository;

    /**
     * PlaceController constructor.
     * @param NewsRepositoryInterface $newsRepository
     */
    public function __construct(NewsRepositoryInterface $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * @Route("/news/{id}", name="one news", methods={"GET"}, requirements={"id": "\d+"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns one news",
     * )
     * @param int $id
     * @return JsonResponse
     */
    public function getOne(int $id): JsonResponse
    {
        $news = $this->newsRepository->findOne($id);

        if (!$news) {
            throw new NotFoundHttpException('News with given id does not exists.');
        }

        return new JsonResponse(new OneNewsView($news), JsonResponse::HTTP_OK);
    }

    /**
     * @Route("/news", name="news list", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns news list",
     * )
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $collection = $this->newsRepository->findAll();

        $response = new JsonResponse(new NewsView($collection->getItems()), JsonResponse::HTTP_OK);
        $response->headers->set('X-Total-Count', $collection->getTotalQuantity());

        return $response;
    }
}
