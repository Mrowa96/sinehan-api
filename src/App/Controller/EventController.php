<?php

namespace App\Controller;

use App\View\EventsView;
use App\View\EventView;
use Domain\Event\Repository\EventRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class EventController
 * @package App\Controller
 */
final class EventController
{
    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * EventController constructor.
     * @param EventRepositoryInterface $eventRepository
     */
    public function __construct(EventRepositoryInterface $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * @Route("/event/{id}", name="one event", methods={"GET"}, requirements={"id": "\d+"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns one event"
     * )
     * @param int $id
     * @return JsonResponse
     */
    public function getOne(int $id): JsonResponse
    {
        $event = $this->eventRepository->findOne($id);

        if (!$event) {
            throw new NotFoundHttpException('Event with given id does not exists.');
        }

        return new JsonResponse(new EventView($event), JsonResponse::HTTP_OK);
    }

    /**
     * @Route("/event", name="event list", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns event list",
     * )
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $collection = $this->eventRepository->findAll();

        $response = new JsonResponse(new EventsView($collection->getItems()), JsonResponse::HTTP_OK);
        $response->headers->set('X-Total-Count', $collection->getTotalQuantity());

        return $response;
    }
}
