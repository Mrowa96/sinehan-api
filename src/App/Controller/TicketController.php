<?php

namespace App\Controller;

use App\View\TicketsView;
use Domain\Ticket\Repository\TicketRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class TicketController
 * @package App\Controller
 */
final class TicketController
{
    /**
     * @var TicketRepositoryInterface
     */
    private $ticketRepository;

    /**
     * TicketController constructor.
     * @param TicketRepositoryInterface $ticketRepository
     */
    public function __construct(TicketRepositoryInterface $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    /**
     * @Route("/ticket", name="tickets list", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns tickets list"
     * )
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $data = $this->ticketRepository->findAll();

        return new JsonResponse(new TicketsView($data), JsonResponse::HTTP_OK);
    }
}
