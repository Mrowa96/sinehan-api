<?php

namespace App\Controller;

use App\View\ReservationBasicView;
use App\View\ReservationView;
use Domain\Reservation\Exception\CreateReservationException;
use Domain\Reservation\Exception\InvalidReservationException;
use Domain\Reservation\Exception\ReservationUpdateFailure;
use Domain\Reservation\Factory\ReservationFactoryInterface;
use Domain\Reservation\Repository\ReservationRepositoryInterface;
use Domain\Reservation\Service\NewReservationValidatorServiceInterface;
use Domain\Reservation\Service\ReservationUpdaterServiceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class ReservationController
 * @package App\Controller
 */
final class ReservationController
{
    /**
     * @var ReservationFactoryInterface
     */
    private $reservationFactory;

    /**
     * @var NewReservationValidatorServiceInterface
     */
    private $newReservationValidatorService;

    /**
     * @var ReservationRepositoryInterface
     */
    private $reservationRepository;

    /**
     * @var ReservationUpdaterServiceInterface
     */
    private $reservationUpdaterService;

    /**
     * ReservationController constructor.
     * @param ReservationFactoryInterface $reservationFactory
     * @param NewReservationValidatorServiceInterface $newReservationValidatorService
     * @param ReservationRepositoryInterface $reservationRepository
     * @param ReservationUpdaterServiceInterface $reservationUpdaterService
     */
    public function __construct(
        ReservationFactoryInterface $reservationFactory,
        NewReservationValidatorServiceInterface $newReservationValidatorService,
        ReservationRepositoryInterface $reservationRepository,
        ReservationUpdaterServiceInterface $reservationUpdaterService
    ) {
        $this->reservationFactory = $reservationFactory;
        $this->newReservationValidatorService = $newReservationValidatorService;
        $this->reservationRepository = $reservationRepository;
        $this->reservationUpdaterService = $reservationUpdaterService;
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="View reservation details - extended version"
     * )
     * @param string $id
     * @return JsonResponse
     */
    public function getOneExtended(string $id): JsonResponse
    {
        $reservation = $this->reservationRepository->findOne($id);

        if (!$reservation) {
            throw new NotFoundHttpException('Reservation with given id does not exists.');
        }

        return new JsonResponse(new ReservationView($reservation), JsonResponse::HTTP_OK);
    }

    /**
     * @Route("/reservation/{id}", name="one reservation", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="View reservation details"
     * )
     * @param string $id
     * @return JsonResponse
     */
    public function getOne(string $id): JsonResponse
    {
        $reservation = $this->reservationRepository->findOne($id);

        if (!$reservation) {
            throw new NotFoundHttpException('Reservation with given id does not exists.');
        }

        return new JsonResponse(new ReservationBasicView($reservation), JsonResponse::HTTP_OK);
    }

    /**
     * @Route("/reservation", name="Create reservation", methods={"POST"})
     * @SWG\Response(
     *     response=201,
     *     description="Returns new reservation basic object"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $data = array_merge(
            json_decode($request->getContent(), true),
            ['ipAddress' => $request->getClientIp()]
        );

        if ($this->newReservationValidatorService->validate($data)) {
            try {
                $reservation = $this->reservationFactory->create($data);

                $this->reservationRepository->saveOne($reservation, true);

                return new JsonResponse(new ReservationBasicView($reservation), JsonResponse::HTTP_CREATED);
            } catch (CreateReservationException | InvalidReservationException $exception) {
                throw new BadRequestHttpException($exception->getMessage());
            }
        }

        throw new BadRequestHttpException($this->newReservationValidatorService->getErrorsAsString());
    }

    /**
     * @Route("/reservation/{id}", name="Update reservation", methods={"PUT"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns nothing except status"
     * )
     * @param string $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update(string $id, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $reservation = $this->reservationRepository->findOne($id);

        if (!$reservation) {
            throw new NotFoundHttpException('Reservation with given id does not exists.');
        }

        try {
            $this->reservationUpdaterService->update($reservation, $data);

            return new JsonResponse(null, JsonResponse::HTTP_OK);
        } catch (ReservationUpdateFailure $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }
    }

    /**
     * @Route("/reservation/{id}", name="Delete reservation", methods={"DELETE"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns nothing except status"
     * )
     * @param string $id
     * @return JsonResponse
     */
    public function delete(string $id): JsonResponse
    {
        $reservation = $this->reservationRepository->findOne($id);

        if (!$reservation) {
            throw new NotFoundHttpException('Reservation with given id does not exists.');
        }

        try {
            $this->reservationRepository->deleteOne($reservation);

            return new JsonResponse(null, JsonResponse::HTTP_OK);
        } catch (ReservationUpdateFailure $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }
    }
}
