<?php

namespace App\Controller;

use App\View\PlacesView;
use Domain\Place\Repository\PlaceRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Domain\Place\Entity\Place;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PlaceController
 * @package App\Controller
 */
final class PlaceController
{
    /**
     * @var PlaceRepositoryInterface
     */
    private $placeRepository;

    /**
     * PlaceController constructor.
     * @param PlaceRepositoryInterface $placeRepository
     */
    public function __construct(PlaceRepositoryInterface $placeRepository)
    {
        $this->placeRepository = $placeRepository;
    }

    /**
     * @Route("/place", name="places list", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns places list",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Place::class))
     *     )
     * )
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $data = $this->placeRepository->findAll();

        return new JsonResponse(new PlacesView($data), JsonResponse::HTTP_OK);
    }
}
