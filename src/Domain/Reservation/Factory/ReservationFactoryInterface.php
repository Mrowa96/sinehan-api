<?php

namespace Domain\Reservation\Factory;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Exception\CreateReservationException;

/**
 * Interface ReservationFactoryInterface
 * @package Domain\Reservation\Factory
 */
interface ReservationFactoryInterface
{
    /**
     * @param array $data
     * @return Reservation
     * @throws CreateReservationException
     */
    public function create(array $data): Reservation;
}
