<?php

namespace Domain\Reservation\Exception;

/**
 * Class CreateReservationException
 * @package Domain\Reservation\Exception
 */
final class CreateReservationException extends \RuntimeException
{

}
