<?php

namespace Domain\Reservation\Exception;

/**
 * Class ReservationUpdateFailure
 * @package Domain\Reservation\Exception
 */
final class ReservationUpdateFailure extends \RuntimeException
{

}
