<?php

namespace Domain\Reservation\Exception;

/**
 * Class ReservationTicketsUpdateFailure
 * @package Domain\Reservation\Exception
 */
final class ReservationTicketsUpdateFailure extends \RuntimeException
{

}
