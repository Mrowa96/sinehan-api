<?php

namespace Domain\Reservation\Exception;

/**
 * Class ReservationSeatsUpdateFailure
 * @package Domain\Reservation\Exception
 */
final class ReservationSeatsUpdateFailure extends \RuntimeException
{

}
