<?php

namespace Domain\Reservation\Exception;

/**
 * Class InvalidReservationException
 * @package Domain\Reservation\Exception
 */
final class InvalidReservationException extends \RuntimeException
{

}
