<?php

namespace Domain\Reservation\Exception;

/**
 * Class ReservationClientUpdateFailure
 * @package Domain\Reservation\Exception
 */
final class ReservationClientUpdateFailure extends \RuntimeException
{

}
