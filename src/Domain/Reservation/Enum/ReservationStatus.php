<?php

namespace Domain\Reservation\Enum;

/**
 * Class ReservationStatus
 * @package Domain\Reservation\Enum
 */
final class ReservationStatus
{
    public const IN_PROGRESS = 'IN_PROGRESS';
    public const COMPLETE = 'COMPLETE';
}
