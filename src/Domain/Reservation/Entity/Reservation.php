<?php

namespace Domain\Reservation\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Domain\EntityInterface;
use Domain\Reservation\Enum\ReservationStatus;
use Domain\ReservationClient\Entity\ReservationClient;
use Domain\ReservationSeat\Entity\ReservationSeat;
use Domain\ReservationTicket\Entity\ReservationTicket;
use Domain\Show\Entity\Show;

/**
 * Class Reservation
 * @package Domain\Reservation\Entity
 */
class Reservation implements EntityInterface
{
    /**
     * @var string|null
     */
    private $id;

    /**
     * @var Show|null
     */
    private $show;

    /**
     * @var ReservationTicket[]
     */
    private $tickets;

    /**
     * @var ReservationSeat[]
     */
    private $seats;

    /**
     * @var ReservationClient|null
     */
    private $client;

    /**
     * @var string|null
     */
    private $ipAddress;

    /**
     * @var \DateTimeImmutable|null
     */
    private $time;

    /**
     * @var bool
     */
    private $dataProcessingAccepted;

    /**
     * @var \DateTimeImmutable
     */
    private $createDate;

    /**
     * @var \DateTimeImmutable
     */
    private $updateDate;

    /**
     * @var string
     */
    private $status;

    /**
     * Reservation constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->seats = new ArrayCollection();
        $this->tickets = new ArrayCollection();
        $this->dataProcessingAccepted = false;
        $this->createDate = new \DateTimeImmutable();
        $this->updateDate = new \DateTimeImmutable();
        $this->status = ReservationStatus::IN_PROGRESS;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param Show $show
     */
    public function setShow(Show $show): void
    {
        $this->show = $show;
    }

    /**
     * @return Show|null
     */
    public function getShow(): ?Show
    {
        return $this->show;
    }

    /**
     * @return ReservationTicket[]
     */
    public function getTickets(): array
    {
        return $this->tickets->toArray();
    }

    /**
     * @return bool
     */
    public function hasTickets(): bool
    {
        return !empty($this->getTickets());
    }

    /**
     * @return ReservationSeat[]
     */
    public function getSeats(): array
    {
        return $this->seats->toArray();
    }

    /**
     * @return bool
     */
    public function hasSeats(): bool
    {
        return !empty($this->getSeats());
    }

    /**
     * @return ReservationClient|null
     */
    public function getClient(): ?ReservationClient
    {
        return $this->client;
    }

    /**
     * @return bool
     */
    public function hasClient(): bool
    {
        return !is_null($this->client);
    }

    /**
     * @return bool
     */
    public function hasClientWithInformation(): bool
    {
        return $this->getClient()->isComplete();
    }

    /**
     * @return string|null
     */
    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress(string $ipAddress): void
    {
        $this->ipAddress = $ipAddress;
    }

    /**
     * @param \DateTimeImmutable $time
     */
    public function setTime(\DateTimeImmutable $time): void
    {
        $this->time = $time;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getTime(): ?\DateTimeImmutable
    {
        return $this->time;
    }

    /**
     * @param bool $dataProcessingAccepted
     */
    public function setDataProcessingAccepted(bool $dataProcessingAccepted): void
    {
        $this->dataProcessingAccepted = $dataProcessingAccepted;
    }

    /**
     * @return bool
     */
    public function isDataProcessingAccepted(): bool
    {
        return $this->dataProcessingAccepted;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreateDate(): \DateTimeImmutable
    {
        return $this->createDate;
    }

    /**
     * @param \DateTimeImmutable $updateDate
     */
    public function setUpdateDate(\DateTimeImmutable $updateDate): void
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getUpdateDate(): \DateTimeImmutable
    {
        return $this->updateDate;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->id ?? '';
    }
}
