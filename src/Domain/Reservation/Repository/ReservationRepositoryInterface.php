<?php

namespace Domain\Reservation\Repository;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Exception\InvalidReservationException;

/**
 * Interface ReservationRepositoryInterface
 * @package Domain\Reservation\Repository
 */
interface ReservationRepositoryInterface
{
    /**
     * @param Reservation $reservation
     * @param bool $isNew
     */
    public function saveOne(Reservation $reservation, bool $isNew = false): void;

    /**
     * @param Reservation[] $reservations
     * @throws InvalidReservationException
     */
    public function saveMany(array $reservations): void;

    /**
     * @param Reservation $reservation
     */
    public function deleteOne(Reservation $reservation): void;

    /**
     * @param string $id
     * @return Reservation|null
     */
    public function findOne(string $id): ?Reservation;

    /**
     * @param string $ipAddress
     * @param \DateTime $date
     * @return Reservation[]
     */
    public function findFewByIpAddressAndCreateDate(string $ipAddress, \DateTime $date): array;
}
