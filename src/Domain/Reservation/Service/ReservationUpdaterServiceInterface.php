<?php

namespace Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Exception\ReservationUpdateFailure;

/**
 * Interface ReservationUpdaterServiceInterface
 * @package Domain\Reservation\Service
 */
interface ReservationUpdaterServiceInterface
{
    /**
     * @param Reservation $reservation
     * @param array $data
     * @throws ReservationUpdateFailure
     */
    public function update(Reservation $reservation, array $data): void;
}
