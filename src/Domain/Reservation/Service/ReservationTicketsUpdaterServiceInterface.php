<?php

namespace Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Exception\ReservationTicketsUpdateFailure;

/**
 * Interface ReservationTicketsUpdaterServiceInterface
 * @package Domain\Reservation\Service
 */
interface ReservationTicketsUpdaterServiceInterface
{
    /**
     * @param Reservation $reservation
     * @param array $ticketsData
     * @throws ReservationTicketsUpdateFailure
     */
    public function update(Reservation $reservation, array $ticketsData): void;
}
