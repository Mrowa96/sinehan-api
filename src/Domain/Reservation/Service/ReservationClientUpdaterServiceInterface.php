<?php

namespace Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Exception\ReservationClientUpdateFailure;

/**
 * Interface ReservationClientUpdaterServiceInterface
 * @package Domain\Reservation\Service
 */
interface ReservationClientUpdaterServiceInterface
{
    /**
     * @param Reservation $reservation
     * @param array $clientData
     * @throws ReservationClientUpdateFailure
     */
    public function update(Reservation $reservation, array $clientData);
}
