<?php

namespace Domain\Reservation\Service;

/**
 * Interface NewReservationValidatorServiceInterface
 * @package Domain\Reservation\Service
 */
interface NewReservationValidatorServiceInterface
{
    /**
     * @param array $data
     * @return bool
     */
    public function validate(array $data): bool;

    /**
     * @return bool
     */
    public function hasErrors(): bool;

    /**
     * @return string
     */
    public function getErrorsAsString(): string;
}
