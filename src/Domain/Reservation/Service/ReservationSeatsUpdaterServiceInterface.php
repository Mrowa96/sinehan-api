<?php

namespace Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Exception\ReservationSeatsUpdateFailure;

/**
 * Interface ReservationSeatsUpdaterServiceInterface
 * @package Domain\Reservation\Service
 */
interface ReservationSeatsUpdaterServiceInterface
{
    /**
     * @param Reservation $reservation
     * @param array $seatsData
     * @throws ReservationSeatsUpdateFailure
     */
    public function update(Reservation $reservation, array $seatsData);
}
