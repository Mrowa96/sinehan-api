<?php

namespace Domain\PlaceRoom\Repository;

use Domain\PlaceRoom\Entity\PlaceRoom;
use Domain\PlaceRoom\Exception\AddPlaceRoomException;
use Domain\PlaceRoom\Exception\NotFoundPlaceRoomException;

/**
 * Interface PlaceRoomRepositoryInterface
 * @package Domain\PlaceRoom\Repository
 */
interface PlaceRoomRepositoryInterface
{
    /**
     * @param array $data
     * @return PlaceRoom
     * @throws AddPlaceRoomException
     */
    public function add(array $data): PlaceRoom;

    /**
     * @param int $id
     * @param array $data
     * @return PlaceRoom
     * @throws NotFoundPlaceRoomException
     */
    public function update(int $id, array $data): PlaceRoom;

    /**
     * @param int $id
     */
    public function remove(int $id): void;
}
