<?php

namespace Domain\PlaceRoom\Entity;

use Domain\EntityInterface;
use Domain\Place\Entity\Place;

/**
 * Class PlaceRoom
 * @package Domain\PlaceRoom\Entity
 */
class PlaceRoom implements EntityInterface
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var int|null
     */
    private $seatsRows;

    /**
     * @var int|null
     */
    private $seatsColumns;

    /**
     * @var Place|null
     */
    private $place;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getSeatsRows(): ?int
    {
        return $this->seatsRows;
    }

    /**
     * @param int $seatsRows
     */
    public function setSeatsRows(int $seatsRows): void
    {
        $this->seatsRows = $seatsRows;
    }

    /**
     * @return int|null
     */
    public function getSeatsColumns(): ?int
    {
        return $this->seatsColumns;
    }

    /**
     * @param int $seatsColumns
     */
    public function setSeatsColumns(int $seatsColumns): void
    {
        $this->seatsColumns = $seatsColumns;
    }

    /**
     * @param Place $place
     */
    public function setPlace(Place $place): void
    {
        $this->place = $place;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name ?? '';
    }
}
