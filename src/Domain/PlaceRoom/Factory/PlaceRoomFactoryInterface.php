<?php

namespace Domain\PlaceRoom\Factory;

use Domain\PlaceRoom\Entity\PlaceRoom;
use Domain\PlaceRoom\Exception\CreatePlaceRoomException;

/**
 * Interface PlaceRoomFactoryInterface
 * @package Domain\PlaceRoom\Factory
 */
interface PlaceRoomFactoryInterface
{
    /**
     * @param array $data
     * @return PlaceRoom
     * @throws CreatePlaceRoomException
     */
    public function create(array $data): PlaceRoom;
}
