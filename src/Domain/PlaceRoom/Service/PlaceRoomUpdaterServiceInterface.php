<?php

namespace Domain\PlaceRoom\Service;

use Domain\PlaceRoom\Entity\PlaceRoom;

/**
 * Interface PlaceRoomUpdaterServiceInterface
 * @package Domain\PlaceRoom\Service
 */
interface PlaceRoomUpdaterServiceInterface
{
    /**
     * @param PlaceRoom $room
     * @param array $data
     * @return PlaceRoom
     */
    public function update(PlaceRoom $room, array $data): PlaceRoom;
}
