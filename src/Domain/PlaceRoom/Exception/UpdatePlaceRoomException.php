<?php

namespace Domain\PlaceRoom\Exception;

/**
 * Class UpdatePlaceRoomException
 * @package Domain\PlaceRoom\Exception
 */
final class UpdatePlaceRoomException extends \RuntimeException
{

}
