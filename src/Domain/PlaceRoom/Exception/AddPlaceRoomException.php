<?php

namespace Domain\PlaceRoom\Exception;

/**
 * Class AddPlaceRoomException
 * @package Domain\PlaceRoom\Exception
 */
final class AddPlaceRoomException extends \RuntimeException
{

}
