<?php

namespace Domain\PlaceRoom\Exception;

/**
 * Class CreatePlaceRoomException
 * @package Domain\PlaceRoom\Exception
 */
final class CreatePlaceRoomException extends \RuntimeException
{

}
