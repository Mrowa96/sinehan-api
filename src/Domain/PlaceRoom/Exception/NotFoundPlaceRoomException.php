<?php

namespace Domain\PlaceRoom\Exception;

/**
 * Class NotFoundPlaceRoomException
 * @package Domain\PlaceRoom\Exception
 */
final class NotFoundPlaceRoomException extends \RuntimeException
{

}
