<?php

namespace Domain\Event\Exception;

/**
 * Class AddEventException
 * @package Domain\Event\Exception
 */
final class AddEventException extends \RuntimeException
{

}
