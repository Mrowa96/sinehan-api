<?php

namespace Domain\Event\Exception;

/**
 * Class UpdateEventException
 * @package Domain\Event\Exception
 */
final class UpdateEventException extends \RuntimeException
{

}
