<?php

namespace Domain\Event\Exception;

/**
 * Class CreateEventException
 * @package Domain\Event\Exception
 */
final class CreateEventException extends \RuntimeException
{

}
