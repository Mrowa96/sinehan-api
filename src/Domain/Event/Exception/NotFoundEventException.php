<?php

namespace Domain\Event\Exception;

/**
 * Class NotFoundEventException
 * @package Domain\Event\Exception
 */
final class NotFoundEventException extends \RuntimeException
{

}
