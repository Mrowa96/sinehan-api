<?php

namespace Domain\Event\Collection;

use Domain\EntityCollection;
use Domain\Event\Entity\Event;

/**
 * Class EventCollection
 * @package Domain\Event\Collection
 */
final class EventCollection extends EntityCollection
{
    /**
     * @return Event[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
