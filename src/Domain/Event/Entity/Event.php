<?php

namespace Domain\Event\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Domain\EntityInterface;
use Domain\Show\Entity\Show;

/**
 * Class Event
 * @package Domain\Event\Entity
 */
class Event implements EntityInterface
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var string|null
     */
    private $originalTitle;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $directorName;

    /**
     * @var string[]
     */
    private $productionCountries;

    /**
     * @var int|null
     */
    private $productionYear;

    /**
     * @var int|null
     */
    private $ageLimit;

    /**
     * @var \DateTime|null
     */
    private $releaseDate;

    /**
     * @var int|null
     */
    private $duration;

    /**
     * @var string|null
     */
    private $posterUrl;

    /**
     * @var string|null
     */
    private $trailerUrl;

    /**
     * @var Show[]
     */
    private $shows;

    /**
     * Event constructor.
     */
    public function __construct()
    {
        $this->shows = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getOriginalTitle(): ?string
    {
        return $this->originalTitle;
    }

    /**
     * @param string $originalTitle
     */
    public function setOriginalTitle(string $originalTitle): void
    {
        $this->originalTitle = $originalTitle;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getDirectorName(): ?string
    {
        return $this->directorName;
    }

    /**
     * @param string $directorName
     */
    public function setDirectorName(string $directorName): void
    {
        $this->directorName = $directorName;
    }

    /**
     * @return string[]
     */
    public function getProductionCountries(): array
    {
        return $this->productionCountries;
    }

    /**
     * @param string[] $productionCountries
     */
    public function setProductionCountries(array $productionCountries): void
    {
        $this->productionCountries = $productionCountries;
    }

    /**
     * @return int|null
     */
    public function getProductionYear(): ?int
    {
        return $this->productionYear;
    }

    /**
     * @param int $productionYear
     */
    public function setProductionYear(int $productionYear): void
    {
        $this->productionYear = $productionYear;
    }

    /**
     * @return int|null
     */
    public function getAgeLimit(): ?int
    {
        return $this->ageLimit;
    }

    /**
     * @param int $ageLimit
     */
    public function setAgeLimit(int $ageLimit): void
    {
        $this->ageLimit = $ageLimit;
    }

    /**
     * @return \DateTime|null
     */
    public function getReleaseDate(): ?\DateTime
    {
        return $this->releaseDate;
    }

    /**
     * @param \DateTime $releaseDate
     */
    public function setReleaseDate(\DateTime $releaseDate): void
    {
        $this->releaseDate = $releaseDate;
    }

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string|null
     */
    public function getPosterUrl(): ?string
    {
        return $this->posterUrl;
    }

    /**
     * @param string $posterUrl
     */
    public function setPosterUrl(string $posterUrl): void
    {
        $this->posterUrl = $posterUrl;
    }

    /**
     * @return string|null
     */
    public function getTrailerUrl(): ?string
    {
        return $this->trailerUrl;
    }

    /**
     * @param string $trailerUrl
     */
    public function setTrailerUrl(string $trailerUrl): void
    {
        $this->trailerUrl = $trailerUrl;
    }

    /**
     * @return Show[]
     */
    public function getShows(): array
    {
        return $this->shows->toArray();
    }

    /**
     * @param Show[] $shows
     */
    public function setShows(array $shows): void
    {
        $this->shows = $shows;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->title ?? '';
    }
}
