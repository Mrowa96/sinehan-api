<?php

namespace Domain\Event\Factory;

use Domain\Event\Entity\Event;
use Domain\Event\Exception\CreateEventException;

/**
 * Interface EventFactoryInterface
 * @package Domain\Event\Factory
 */
interface EventFactoryInterface
{
    /**
     * @param array $data
     * @return Event
     * @throws CreateEventException
     */
    public function create(array $data): Event;
}
