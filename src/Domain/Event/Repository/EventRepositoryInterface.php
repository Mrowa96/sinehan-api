<?php

namespace Domain\Event\Repository;

use Domain\Event\Collection\EventCollection;
use Domain\Event\Entity\Event;
use Domain\Event\Exception\AddEventException;
use Domain\Event\Exception\NotFoundEventException;

/**
 * Interface EventRepositoryInterface
 * @package Domain\Event\Repository
 */
interface EventRepositoryInterface
{
    /**
     * @param array $data
     * @return Event
     * @throws AddEventException
     */
    public function add(array $data): Event;

    /**
     * @param int $id
     * @param array $data
     * @return Event
     * @throws NotFoundEventException
     */
    public function update(int $id, array $data): Event;

    /**
     * @param int $id
     */
    public function remove(int $id): void;

    /**
     * @param int $id
     * @return Event|null
     */
    public function findOne(int $id): ?Event;

    /**
     * @return EventCollection
     */
    public function findAll(): EventCollection;
}
