<?php

namespace Domain\Event\Service;

use Domain\Event\Entity\Event;

/**
 * Interface EventUpdaterServiceInterface
 * @package Domain\Event\Service
 */
interface EventUpdaterServiceInterface
{
    /**
     * @param Event $event
     * @param array $data
     * @return Event
     */
    public function update(Event $event, array $data): Event;
}
