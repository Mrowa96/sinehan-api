<?php

namespace Domain\News\Service;

use Domain\News\Entity\News;

/**
 * Interface NewsUpdaterServiceInterface
 * @package Domain\News\Service
 */
interface NewsUpdaterServiceInterface
{
    /**
     * @param News $news
     * @param array $data
     * @return News
     */
    public function update(News $news, array $data): News;
}
