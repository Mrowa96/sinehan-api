<?php

namespace Domain\News\Repository;

use Domain\News\Collection\NewsCollection;
use Domain\News\Entity\News;
use Domain\News\Exception\AddNewsException;
use Domain\News\Exception\NotFoundNewsException;

/**
 * Interface NewsRepositoryInterface
 * @package Domain\News\Repository
 */
interface NewsRepositoryInterface
{
    /**
     * @param array $data
     * @return News
     * @throws AddNewsException
     */
    public function add(array $data): News;

    /**
     * @param int $id
     * @param array $data
     * @return News
     * @throws NotFoundNewsException
     */
    public function update(int $id, array $data): News;

    /**
     * @param int $id
     */
    public function remove(int $id): void;

    /**
     * @param int $id
     * @return News|null
     */
    public function findOne(int $id): ?News;

    /**
     * @return NewsCollection
     */
    public function findAll(): NewsCollection;
}
