<?php

namespace Domain\News\Exception;

/**
 * Class UpdateNewsException
 * @package Domain\News\Exception
 */
final class UpdateNewsException extends \RuntimeException
{

}
