<?php

namespace Domain\News\Exception;

/**
 * Class NotFoundNewsException
 * @package Domain\News\Exception
 */
final class NotFoundNewsException extends \RuntimeException
{

}
