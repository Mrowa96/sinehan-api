<?php

namespace Domain\News\Exception;

/**
 * Class CreateNewsException
 * @package Domain\News\Exception
 */
final class CreateNewsException extends \RuntimeException
{

}
