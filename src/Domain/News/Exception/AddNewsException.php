<?php

namespace Domain\News\Exception;

/**
 * Class AddNewsException
 * @package Domain\News\Exception
 */
final class AddNewsException extends \RuntimeException
{

}
