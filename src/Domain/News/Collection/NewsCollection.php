<?php

namespace Domain\News\Collection;

use Domain\EntityCollection;
use Domain\News\Entity\News;

/**
 * Class NewsCollection
 * @package Domain\News\Collection
 */
final class NewsCollection extends EntityCollection
{
    /**
     * @return News[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
