<?php

namespace Domain\News\Factory;

use Domain\News\Entity\News;
use Domain\News\Exception\CreateNewsException;

/**
 * Interface NewsFactoryInterface
 * @package Domain\News\Factory
 */
interface NewsFactoryInterface
{
    /**
     * @param array $data
     * @return News
     * @throws CreateNewsException
     */
    public function create(array $data): News;
}
