<?php

namespace Domain\News\Entity;

use Domain\EntityInterface;
use Domain\NewsAuthor\Entity\NewsAuthor;
use Domain\NewsCategory\Entity\NewsCategory;

/**
 * Class News
 * @package Domain\News\Entity
 */
class News implements EntityInterface
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var string|null
     */
    private $photo;

    /**
     * @var string|null
     */
    private $content;

    /**
     * @var \DateTime|null
     */
    private $createDate;

    /**
     * @var NewsCategory|null
     */
    private $category;

    /**
     * @var NewsAuthor|null
     */
    private $author;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto(string $photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreateDate(): ?\DateTime
    {
        return $this->createDate;
    }

    /**
     * @param \DateTime $createDate
     */
    public function setCreateDate(\DateTime $createDate): void
    {
        $this->createDate = $createDate;
    }

    /**
     * @return NewsCategory|null
     */
    public function getCategory(): ?NewsCategory
    {
        return $this->category;
    }

    /**
     * @param NewsCategory $category
     */
    public function setCategory(NewsCategory $category): void
    {
        $this->category = $category;
    }

    /**
     * @return NewsAuthor|null
     */
    public function getAuthor(): ?NewsAuthor
    {
        return $this->author;
    }

    /**
     * @param NewsAuthor $author
     */
    public function setAuthor(NewsAuthor $author): void
    {
        $this->author = $author;
    }

    /**
     * @return bool
     */
    public function hasAuthor(): bool
    {
        return !empty($this->author);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->title ?? '';
    }
}
