<?php

namespace Domain\ReservationSeat\Exception;

/**
 * Class CreateReservationSeatException
 * @package Domain\ReservationSeat\Exception
 */
final class CreateReservationSeatException extends \RuntimeException
{

}
