<?php

namespace Domain\ReservationSeat\Exception;

/**
 * Class InvalidReservationSeatException
 * @package Domain\ReservationSeat\Exception
 */
final class InvalidReservationSeatException extends \LogicException
{

}
