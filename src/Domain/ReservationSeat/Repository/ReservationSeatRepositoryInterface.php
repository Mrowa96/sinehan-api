<?php

namespace Domain\ReservationSeat\Repository;

use Domain\ReservationSeat\Entity\ReservationSeat;
use Domain\ReservationSeat\Exception\InvalidReservationSeatException;

/**
 * Interface ReservationSeatRepositoryInterface
 * @package Domain\ReservationSeat\Repository
 */
interface ReservationSeatRepositoryInterface
{
    /**
     * @param string $reservationId
     * @param int $showId
     * @param string $time
     * @param int $row
     * @param int $column
     * @return ReservationSeat|null
     */
    public function findOneByParameters(
        string $reservationId,
        int $showId,
        string $time,
        int $row,
        int $column
    ): ?ReservationSeat;

    /**
     * @param string $reservationId
     * @return ReservationSeat[]
     */
    public function findFewByReservationId(string $reservationId): array;

    /**
     * @param int $showId
     * @param string $time
     * @return ReservationSeat[]
     */
    public function findFewByShowIdAndTime(int $showId, string $time): array;

    /**
     * @param ReservationSeat[] $reservationSeats
     * @param bool $isNew
     * @throws InvalidReservationSeatException
     */
    public function saveFew(array $reservationSeats, bool $isNew = false): void;

    /**
     * @param int[] $ids
     */
    public function removeFew(array $ids): void;

    /**
     * @param string $reservationId
     */
    public function removeFewByReservationId(string $reservationId): void;
}
