<?php

namespace Domain\ReservationSeat\Factory;

use Domain\ReservationSeat\Entity\ReservationSeat;
use Domain\ReservationSeat\Exception\CreateReservationSeatException;

/**
 * Interface ReservationSeatFactoryInterface
 * @package Domain\ReservationSeat\Factory
 */
interface ReservationSeatFactoryInterface
{
    /**
     * @param array $data
     * @return ReservationSeat
     * @throws CreateReservationSeatException
     */
    public function create(array $data): ReservationSeat;
}
