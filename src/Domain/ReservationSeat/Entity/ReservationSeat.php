<?php

namespace Domain\ReservationSeat\Entity;

use Domain\EntityInterface;
use Domain\Reservation\Entity\Reservation;

/**
 * Class ReservationSeat
 * @package Domain\ReservationSeat\Entity
 */
class ReservationSeat implements EntityInterface
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var Reservation|null
     */
    private $reservation;

    /**
     * @var int|null
     */
    private $seatColumn;

    /**
     * @var int|null
     */
    private $seatRow;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Reservation|null
     */
    public function getReservation(): ?Reservation
    {
        return $this->reservation;
    }

    /**
     * @param Reservation $reservation
     */
    public function setReservation(Reservation $reservation): void
    {
        $this->reservation = $reservation;
    }

    /**
     * @return int|null
     */
    public function getSeatColumn(): ?int
    {
        return $this->seatColumn;
    }

    /**
     * @param int $seatColumn
     */
    public function setSeatColumn(int $seatColumn): void
    {
        $this->seatColumn = $seatColumn;
    }

    /**
     * @return int|null
     */
    public function getSeatRow(): ?int
    {
        return $this->seatRow;
    }

    /**
     * @param int $seatRow
     */
    public function setSeatRow(int $seatRow): void
    {
        $this->seatRow = $seatRow;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return 'C_' . $this->seatColumn . 'R_' . $this->seatColumn;
    }
}
