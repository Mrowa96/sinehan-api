<?php

namespace Domain;

/**
 * Class EntityCollection
 * @package Domain
 */
abstract class EntityCollection
{
    /**
     * @var EntityInterface[]
     */
    protected $items;

    /**
     * @var int
     */
    protected $totalQuantity;

    /**
     * EntityCollection constructor.
     * @param EntityInterface[] $items
     * @param int $totalQuantity
     */
    public function __construct(array $items, int $totalQuantity)
    {
        $this->items = $items;
        $this->totalQuantity = $totalQuantity;
    }

    /**
     * @return EntityInterface[]
     */
    abstract public function getItems(): array;

    /**
     * @return int
     */
    public function getTotalQuantity(): int
    {
        return $this->totalQuantity;
    }
}
