<?php

namespace Domain\TakenSeat\Model;

/**
 * Class TakenSeat
 * @package Domain\TakenSeat\Model
 */
final class TakenSeat
{
    /**
     * @var int
     */
    private $row;

    /**
     * @var int
     */
    private $column;

    /**
     * TakenSeat constructor.
     * @param int $row
     * @param int $column
     */
    public function __construct(int $row, int $column)
    {
        $this->row = $row;
        $this->column = $column;
    }

    /**
     * @return int
     */
    public function getRow(): int
    {
        return $this->row;
    }

    /**
     * @return int
     */
    public function getColumn(): int
    {
        return $this->column;
    }
}
