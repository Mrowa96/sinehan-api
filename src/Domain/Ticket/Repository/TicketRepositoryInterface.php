<?php

namespace Domain\Ticket\Repository;

use Domain\Ticket\Entity\Ticket;
use Domain\Ticket\Exception\AddTicketException;
use Domain\Ticket\Exception\NotFoundTicketException;

/**
 * Interface TicketRepositoryInterface
 * @package Domain\Ticket\Repository
 */
interface TicketRepositoryInterface
{
    /**
     * @param array $data
     * @return Ticket
     * @throws AddTicketException
     */
    public function add(array $data): Ticket;

    /**
     * @param int $id
     * @param array $data
     * @return Ticket
     * @throws NotFoundTicketException
     */
    public function update(int $id, array $data): Ticket;

    /**
     * @param int $id
     */
    public function remove(int $id): void;

    /**
     * @param int $id
     * @return Ticket|null
     */
    public function findOne(int $id): ?Ticket;

    /**
     * @return Ticket[]
     */
    public function findAll(): array;
}
