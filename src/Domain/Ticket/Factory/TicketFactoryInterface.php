<?php

namespace Domain\Ticket\Factory;

use Domain\Ticket\Entity\Ticket;
use Domain\Ticket\Exception\CreateTicketException;

/**
 * Interface TicketFactoryInterface
 * @package Domain\Ticket\Factory
 */
interface TicketFactoryInterface
{
    /**
     * @param array $data
     * @return Ticket
     * @throws CreateTicketException
     */
    public function create(array $data): Ticket;
}
