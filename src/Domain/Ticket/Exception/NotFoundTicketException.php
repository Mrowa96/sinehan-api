<?php

namespace Domain\Ticket\Exception;

/**
 * Class NotFoundTicketException
 * @package Domain\Ticket\Exception
 */
final class NotFoundTicketException extends \RuntimeException
{

}
