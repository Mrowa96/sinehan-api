<?php

namespace Domain\Ticket\Exception;

/**
 * Class UpdateTicketException
 * @package Domain\Ticket\Exception
 */
final class UpdateTicketException extends \RuntimeException
{

}
