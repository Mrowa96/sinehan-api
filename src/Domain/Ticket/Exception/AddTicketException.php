<?php

namespace Domain\Ticket\Exception;

/**
 * Class AddTicketException
 * @package Domain\Ticket\Exception
 */
final class AddTicketException extends \RuntimeException
{

}
