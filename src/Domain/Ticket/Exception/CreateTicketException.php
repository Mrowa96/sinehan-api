<?php

namespace Domain\Ticket\Exception;

/**
 * Class CreateTicketException
 * @package Domain\Ticket\Exception
 */
final class CreateTicketException extends \RuntimeException
{

}
