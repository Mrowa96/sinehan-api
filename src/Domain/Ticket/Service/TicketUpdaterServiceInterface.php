<?php

namespace Domain\Ticket\Service;

use Domain\Ticket\Entity\Ticket;

/**
 * Interface TicketUpdaterServiceInterface
 * @package Domain\Ticket\Service
 */
interface TicketUpdaterServiceInterface
{
    /**
     * @param Ticket $ticket
     * @param array $data
     * @return Ticket
     */
    public function update(Ticket $ticket, array $data): Ticket;
}
