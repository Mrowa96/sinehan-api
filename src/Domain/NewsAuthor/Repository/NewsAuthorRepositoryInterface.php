<?php

namespace Domain\NewsAuthor\Repository;

use Domain\NewsAuthor\Entity\NewsAuthor;
use Domain\NewsAuthor\Exception\AddNewsAuthorException;
use Domain\NewsAuthor\Exception\NotFoundNewsAuthorException;

/**
 * Interface NewsAuthorRepositoryInterface
 * @package Domain\NewsAuthor\Repository
 */
interface NewsAuthorRepositoryInterface
{
    /**
     * @param array $data
     * @return NewsAuthor
     * @throws AddNewsAuthorException
     */
    public function add(array $data): NewsAuthor;

    /**
     * @param int $id
     * @param array $data
     * @return NewsAuthor
     * @throws NotFoundNewsAuthorException
     */
    public function update(int $id, array $data): NewsAuthor;

    /**
     * @param int $id
     */
    public function remove(int $id): void;
}
