<?php

namespace Domain\NewsAuthor\Service;

use Domain\NewsAuthor\Entity\NewsAuthor;

/**
 * Interface NewsAuthorUpdaterServiceInterface
 * @package Domain\NewsAuthor\Service
 */
interface NewsAuthorUpdaterServiceInterface
{
    /**
     * @param NewsAuthor $newsAuthor
     * @param array $data
     * @return NewsAuthor
     */
    public function update(NewsAuthor $newsAuthor, array $data): NewsAuthor;
}
