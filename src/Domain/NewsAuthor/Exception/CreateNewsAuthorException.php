<?php

namespace Domain\NewsAuthor\Exception;

/**
 * Class CreateNewsAuthorException
 * @package Domain\NewsAuthor\Exception
 */
final class CreateNewsAuthorException extends \RuntimeException
{

}
