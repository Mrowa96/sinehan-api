<?php

namespace Domain\NewsAuthor\Exception;

/**
 * Class AddNewsAuthorException
 * @package Domain\NewsAuthor\Exception
 */
final class AddNewsAuthorException extends \RuntimeException
{

}
