<?php

namespace Domain\NewsAuthor\Exception;

/**
 * Class NotFoundNewsAuthorException
 * @package Domain\NewsAuthor\Exception
 */
final class NotFoundNewsAuthorException extends \RuntimeException
{

}
