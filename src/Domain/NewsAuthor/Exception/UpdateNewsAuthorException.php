<?php

namespace Domain\NewsAuthor\Exception;

/**
 * Class UpdateNewsAuthorException
 * @package Domain\NewsAuthor\Exception
 */
final class UpdateNewsAuthorException extends \RuntimeException
{

}
