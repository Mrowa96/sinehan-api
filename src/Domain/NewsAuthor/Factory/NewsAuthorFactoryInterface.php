<?php

namespace Domain\NewsAuthor\Factory;

use Domain\NewsAuthor\Entity\NewsAuthor;
use Domain\NewsAuthor\Exception\CreateNewsAuthorException;

/**
 * Interface NewsAuthorFactoryInterface
 * @package Domain\NewsAuthor\Factory
 */
interface NewsAuthorFactoryInterface
{
    /**
     * @param array $data
     * @return NewsAuthor
     * @throws CreateNewsAuthorException
     */
    public function create(array $data): NewsAuthor;
}
