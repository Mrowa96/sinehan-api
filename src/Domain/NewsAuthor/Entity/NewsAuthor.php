<?php

namespace Domain\NewsAuthor\Entity;

use Domain\EntityInterface;

/**
 * Class NewsAuthor
 * @package Domain\NewsAuthor\Entity
 */
class NewsAuthor implements EntityInterface
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name ?? '';
    }
}
