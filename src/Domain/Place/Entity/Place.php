<?php

namespace Domain\Place\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Domain\EntityInterface;
use Domain\PlaceRoom\Entity\PlaceRoom;

/**
 * Class Place
 * @package Domain\Place\Entity
 */
class Place implements EntityInterface
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var float|null
     */
    private $latitude;

    /**
     * @var float|null
     */
    private $longitude;

    /**
     * @var PlaceRoom[]
     */
    private $rooms;

    /**
     * Place constructor.
     */
    public function __construct()
    {
        $this->rooms = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return PlaceRoom[]
     */
    public function getRooms(): array
    {
        return $this->rooms->toArray();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name ?? '';
    }
}
