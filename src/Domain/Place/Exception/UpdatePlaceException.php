<?php

namespace Domain\Place\Exception;

/**
 * Class UpdatePlaceException
 * @package Domain\Place\Exception
 */
final class UpdatePlaceException extends \RuntimeException
{

}
