<?php

namespace Domain\Place\Exception;

/**
 * Class CreatePlaceException
 * @package Domain\Place\Exception
 */
final class CreatePlaceException extends \RuntimeException
{

}
