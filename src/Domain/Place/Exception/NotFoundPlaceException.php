<?php

namespace Domain\Place\Exception;

/**
 * Class NotFoundPlaceException
 * @package Domain\Place\Exception
 */
final class NotFoundPlaceException extends \RuntimeException
{

}
