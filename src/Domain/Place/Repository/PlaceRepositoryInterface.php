<?php

namespace Domain\Place\Repository;

use Domain\Place\Entity\Place;
use Domain\Place\Exception\AddPlaceException;
use Domain\Place\Exception\NotFoundPlaceException;

/**
 * Interface PlaceRepositoryInterface
 * @package Domain\Place\Repository
 */
interface PlaceRepositoryInterface
{
    /**
     * @param array $data
     * @return Place
     * @throws AddPlaceException
     */
    public function add(array $data): Place;

    /**
     * @param int $id
     * @param array $data
     * @return Place
     * @throws NotFoundPlaceException
     */
    public function update(int $id, array $data): Place;

    /**
     * @param int $id
     */
    public function remove(int $id): void;

    /**
     * @param int $id
     * @return Place|null
     */
    public function findOne(int $id): ?Place;

    /**
     * @return Place[]
     */
    public function findAll(): array;
}
