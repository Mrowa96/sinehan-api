<?php

namespace Domain\Place\Factory;

use Domain\Place\Entity\Place;
use Domain\Place\Exception\CreatePlaceException;

/**
 * Interface PlaceFactoryInterface
 * @package Domain\Place\Factory
 */
interface PlaceFactoryInterface
{
    /**
     * @param array $data
     * @return Place
     * @throws CreatePlaceException
     */
    public function create(array $data): Place;
}
