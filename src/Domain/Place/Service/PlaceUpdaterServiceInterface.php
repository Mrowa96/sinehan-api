<?php

namespace Domain\Place\Service;

use Domain\Place\Entity\Place;

/**
 * Interface PlaceUpdaterServiceInterface
 * @package Domain\Place\Service
 */
interface PlaceUpdaterServiceInterface
{
    /**
     * @param Place $place
     * @param array $data
     * @return Place
     */
    public function update(Place $place, array $data): Place;
}
