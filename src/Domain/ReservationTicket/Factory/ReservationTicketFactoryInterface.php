<?php

namespace Domain\ReservationTicket\Factory;

use Domain\ReservationTicket\Entity\ReservationTicket;
use Domain\ReservationTicket\Exception\CreateReservationTicketException;

/**
 * Interface ReservationTicketFactoryInterface
 * @package Domain\ReservationTicket\Factory
 */
interface ReservationTicketFactoryInterface
{
    /**
     * @param array $data
     * @return ReservationTicket
     * @throws CreateReservationTicketException
     */
    public function create(array $data): ReservationTicket;
}
