<?php

namespace Domain\ReservationTicket\Entity;

use Domain\EntityInterface;
use Domain\Reservation\Entity\Reservation;
use Domain\Ticket\Entity\Ticket;

/**
 * Class ReservationTicket
 * @package Domain\ReservationTicket\Entity
 */
class ReservationTicket implements EntityInterface
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var Reservation|null
     */
    private $reservation;

    /**
     * @var Ticket|null
     */
    private $ticket;

    /**
     * @var float|null
     */
    private $price;

    /**
     * @var int|null
     */
    private $quantity;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Reservation|null
     */
    public function getReservation(): ?Reservation
    {
        return $this->reservation;
    }

    /**
     * @param Reservation $reservation
     */
    public function setReservation(Reservation $reservation): void
    {
        $this->reservation = $reservation;
    }

    /**
     * @return Ticket|null
     */
    public function getTicket(): ?Ticket
    {
        return $this->ticket;
    }

    /**
     * @param Ticket $ticket
     */
    public function setTicket(Ticket $ticket): void
    {
        $this->ticket = $ticket;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->ticket ? $this->ticket->getName() : '';
    }
}
