<?php

namespace Domain\ReservationTicket\Repository;

use Domain\ReservationTicket\Entity\ReservationTicket;
use Domain\ReservationTicket\Exception\InvalidReservationTicketException;

/**
 * Interface ReservationTicketRepositoryInterface
 * @package Domain\ReservationTicket\Repository
 */
interface ReservationTicketRepositoryInterface
{
    /**
     * @param string $reservationId
     * @return ReservationTicket[]
     */
    public function findFewByReservationId(string $reservationId): array;

    /**
     * @param ReservationTicket $reservationTicket
     * @param bool $isNew
     * @throws InvalidReservationTicketException
     */
    public function saveOne(ReservationTicket $reservationTicket, bool $isNew = false): void;

    /**
     * @param int[] $ids
     */
    public function removeFew(array $ids): void;
}
