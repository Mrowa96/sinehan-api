<?php

namespace Domain\ReservationTicket\Exception;

/**
 * Class InvalidReservationTicketException
 * @package Domain\ReservationTicket\Exception
 */
final class InvalidReservationTicketException extends \LogicException
{

}
