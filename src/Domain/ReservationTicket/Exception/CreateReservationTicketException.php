<?php

namespace Domain\ReservationTicket\Exception;

/**
 * Class CreateReservationTicketException
 * @package Domain\ReservationTicket\Exception
 */
final class CreateReservationTicketException extends \RuntimeException
{

}
