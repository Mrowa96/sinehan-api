<?php

namespace Domain\NewsCategory\Repository;

use Domain\NewsCategory\Entity\NewsCategory;
use Domain\NewsCategory\Exception\AddNewsCategoryException;
use Domain\NewsCategory\Exception\NotFoundNewsCategoryException;

/**
 * Interface NewsCategoryRepositoryInterface
 * @package Domain\NewsCategory\Repository
 */
interface NewsCategoryRepositoryInterface
{
    /**
     * @param array $data
     * @return NewsCategory
     * @throws AddNewsCategoryException
     */
    public function add(array $data): NewsCategory;

    /**
     * @param int $id
     * @param array $data
     * @return NewsCategory
     * @throws NotFoundNewsCategoryException
     */
    public function update(int $id, array $data): NewsCategory;

    /**
     * @param int $id
     */
    public function remove(int $id): void;
}
