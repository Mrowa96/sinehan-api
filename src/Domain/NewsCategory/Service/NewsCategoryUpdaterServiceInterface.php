<?php

namespace Domain\NewsCategory\Service;

use Domain\NewsCategory\Entity\NewsCategory;

/**
 * Interface NewsUpdaterServiceInterface
 * @package Domain\NewsCategory\Service
 */
interface NewsCategoryUpdaterServiceInterface
{
    /**
     * @param NewsCategory $newsCategory
     * @param array $data
     * @return NewsCategory
     */
    public function update(NewsCategory $newsCategory, array $data): NewsCategory;
}
