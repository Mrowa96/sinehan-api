<?php

namespace Domain\NewsCategory\Exception;

/**
 * Class CreateNewsCategoryException
 * @package Domain\NewsCategory\Exception
 */
final class CreateNewsCategoryException extends \RuntimeException
{

}
