<?php

namespace Domain\NewsCategory\Exception;

/**
 * Class AddNewsCategoryException
 * @package Domain\NewsCategory\Exception
 */
final class AddNewsCategoryException extends \RuntimeException
{

}
