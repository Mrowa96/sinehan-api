<?php

namespace Domain\NewsCategory\Exception;

/**
 * Class UpdateNewsCategoryException
 * @package Domain\NewsCategory\Exception
 */
final class UpdateNewsCategoryException extends \RuntimeException
{

}
