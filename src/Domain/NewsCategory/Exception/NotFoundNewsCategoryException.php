<?php

namespace Domain\NewsCategory\Exception;

/**
 * Class NotFoundNewsCategoryException
 * @package Domain\NewsCategory\Exception
 */
final class NotFoundNewsCategoryException extends \RuntimeException
{

}
