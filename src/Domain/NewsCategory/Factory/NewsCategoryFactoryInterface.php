<?php

namespace Domain\NewsCategory\Factory;

use Domain\NewsCategory\Entity\NewsCategory;
use Domain\NewsCategory\Exception\CreateNewsCategoryException;

/**
 * Interface NewsCategoryFactoryInterface
 * @package Domain\NewsCategory\Factory
 */
interface NewsCategoryFactoryInterface
{
    /**
     * @param array $data
     * @return NewsCategory
     * @throws CreateNewsCategoryException
     */
    public function create(array $data): NewsCategory;
}
