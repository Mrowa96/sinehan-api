<?php

namespace Domain\Show\Service;

use Domain\Show\Entity\Show;

/**
 * Interface BindTakenSeatsToShowServiceInterface
 * @package Domain\Show\Service
 */
interface BindTakenSeatsToShowServiceInterface
{
    /**
     * @param Show $show
     * @param \DateTimeImmutable $time
     * @return Show
     */
    public function bind(Show $show, \DateTimeImmutable $time): Show;
}
