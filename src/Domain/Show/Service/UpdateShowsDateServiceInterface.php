<?php

namespace Domain\Show\Service;

use Domain\Show\Entity\Show;

/**
 * Interface UpdateShowsDateServiceInterface
 * @package Domain\Show\Service
 */
interface UpdateShowsDateServiceInterface
{
    /**
     * @param Show[] $shows
     * @return Show[]
     */
    public function update(array &$shows): array;
}
