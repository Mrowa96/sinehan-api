<?php

namespace Domain\Show\Exception;

/**
 * Class NotFoundShowException
 * @package Domain\Show\Exception
 */
final class NotFoundShowException extends \RuntimeException
{

}
