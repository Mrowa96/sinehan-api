<?php

namespace Domain\Show\Exception;

/**
 * Class InvalidShowException
 * @package Domain\Show\Exception
 */
final class InvalidShowException extends \RuntimeException
{

}
