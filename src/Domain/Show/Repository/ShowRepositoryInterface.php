<?php

namespace Domain\Show\Repository;

use Domain\Show\Entity\Show;
use Domain\Show\Exception\AddShowException;
use Domain\Show\Exception\InvalidShowException;

/**
 * Interface ShowRepositoryInterface
 * @package Domain\Show\Repository
 */
interface ShowRepositoryInterface
{
    /**
     * @param array $data
     * @return Show
     * @throws AddShowException
     */
    public function add(array $data): Show;

    /**
     * @param int $id
     */
    public function remove(int $id): void;

    /**
     * @param Show $show
     * @throws InvalidShowException
     */
    public function save(Show $show): void;

    /**
     * @param int $id
     * @return Show|null
     */
    public function findOne(int $id): ?Show;

    /**
     * @return Show[]
     */
    public function findAll(): array;
}
