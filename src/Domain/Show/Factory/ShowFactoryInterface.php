<?php

namespace Domain\Show\Factory;

use Domain\Show\Entity\Show;
use Domain\Show\Exception\CreateShowException;

/**
 * Interface ShowFactoryInterface
 * @package Domain\Show\Factory
 */
interface ShowFactoryInterface
{
    /**
     * @param array $data
     * @return Show
     * @throws CreateShowException
     */
    public function create(array $data): Show;
}
