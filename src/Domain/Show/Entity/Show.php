<?php

namespace Domain\Show\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Domain\EntityInterface;
use Domain\Event\Entity\Event;
use Domain\Place\Entity\Place;
use Domain\PlaceRoom\Entity\PlaceRoom;
use Domain\TakenSeat\Model\TakenSeat;

/**
 * Class Show
 * @package Domain\Show\Entity
 */
class Show implements EntityInterface
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var Event|null
     */
    private $event;

    /**
     * @var Place|null
     */
    private $place;

    /**
     * @var PlaceRoom|null
     */
    private $room;

    /**
     * @var TakenSeat[]
     */
    private $takenSeats;

    /**
     * @var \DateTime|null
     */
    private $date;

    /**
     * @var string[]|null
     */
    private $startTimes;

    /**
     * Show constructor.
     */
    public function __construct()
    {
        $this->takenSeats = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Event|null
     */
    public function getEvent(): ?Event
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent(Event $event): void
    {
        $this->event = $event;
    }

    /**
     * @return Place|null
     */
    public function getPlace(): ?Place
    {
        return $this->place;
    }

    /**
     * @param Place $place
     */
    public function setPlace(Place $place): void
    {
        $this->place = $place;
    }

    /**
     * @return TakenSeat[]
     */
    public function getTakenSeats(): array
    {
        return $this->takenSeats->toArray();
    }

    /**
     * @param ArrayCollection $takenSeats
     */
    public function setTakenSeats(ArrayCollection $takenSeats): void
    {
        $this->takenSeats = $takenSeats;
    }

    /**
     * @return bool
     */
    public function hasPlace(): bool
    {
        return !empty($this->place);
    }

    /**
     * @return PlaceRoom|null
     */
    public function getRoom(): ?PlaceRoom
    {
        return $this->room;
    }

    /**
     * @param PlaceRoom $room
     */
    public function setRoom(PlaceRoom $room): void
    {
        $this->room = $room;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = clone $date;
    }

    /**
     * @return null|string[]
     */
    public function getStartTimes(): ?array
    {
        return $this->startTimes;
    }

    /**
     * @param string[] $startTimes
     */
    public function setStartTimes(array $startTimes): void
    {
        $this->startTimes = $startTimes;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->id ?? '';
    }
}
