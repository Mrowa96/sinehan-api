<?php

namespace Domain\ReservationClient\Exception;

/**
 * Class InvalidReservationClientException
 * @package Domain\ReservationClient\Exception
 */
final class InvalidReservationClientException extends \LogicException
{

}
