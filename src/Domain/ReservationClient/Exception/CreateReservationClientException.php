<?php

namespace Domain\ReservationClient\Exception;

/**
 * Class CreateReservationClientException
 * @package Domain\ReservationClient\Exception
 */
final class CreateReservationClientException extends \RuntimeException
{

}
