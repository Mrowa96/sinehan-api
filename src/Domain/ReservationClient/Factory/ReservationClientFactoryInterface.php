<?php

namespace Domain\ReservationClient\Factory;

use Domain\ReservationClient\Entity\ReservationClient;
use Domain\ReservationClient\Exception\CreateReservationClientException;

/**
 * Interface ReservationClientFactoryInterface
 * @package Domain\ReservationClient\Factory
 */
interface ReservationClientFactoryInterface
{
    /**
     * @param array $data
     * @return ReservationClient
     * @throws CreateReservationClientException
     */
    public function create(array $data): ReservationClient;
}
