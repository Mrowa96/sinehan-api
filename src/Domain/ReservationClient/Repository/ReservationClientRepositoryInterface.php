<?php

namespace Domain\ReservationClient\Repository;

use Domain\ReservationClient\Entity\ReservationClient;
use Domain\ReservationClient\Exception\InvalidReservationClientException;

/**
 * Interface ReservationClientRepositoryInterface
 * @package Domain\ReservationClient\Repository
 */
interface ReservationClientRepositoryInterface
{
    /**
     * @param ReservationClient $reservationClient
     * @param bool $isNew
     * @throws InvalidReservationClientException
     */
    public function saveOne(ReservationClient $reservationClient, bool $isNew = false): void;

    /**
     * @param string $reservationId
     */
    public function clearOneByReservationId(string $reservationId): void;
}
