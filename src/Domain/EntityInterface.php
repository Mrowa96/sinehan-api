<?php

namespace Domain;

/**
 * Interface EntityInterface
 * @package Domain
 */
interface EntityInterface
{
    /**
     * @return string
     */
    public function __toString(): string;
}
