# Sinehan API

## Description
REST Api for [sinehan](https://gitlab.com/Mrowa96/sinehan) project.

## Requirements
- php >= 7.3
- mysql >= 5.7

## Installation for development
Create database.
Copy .env.dist file, save as .env and change values.
After that, execute these commands:
- php composer.phar install
- php bin/console doctrine:schema:create
- php bin/console doctrine:schema:update --force
- php bin/console doctrine:migrations:migrate
- php server:run
