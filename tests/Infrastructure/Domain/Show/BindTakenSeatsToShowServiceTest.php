<?php

namespace App\Tests\Infrastructure\Domain\Show;

use Doctrine\Common\Collections\ArrayCollection;
use Domain\ReservationSeat\Entity\ReservationSeat;
use Domain\ReservationSeat\Repository\ReservationSeatRepositoryInterface;
use Domain\Show\Entity\Show;
use Domain\TakenSeat\Model\TakenSeat;
use Infrastructure\Domain\Show\Service\BindTakenSeatsToShowService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class BindTakenSeatsToShowServiceTest
 * @package App\Tests\Infrastructure\Domain\Show
 */
class BindTakenSeatsToShowServiceTest extends TestCase
{
    /**
     * @var MockObject|ReservationSeatRepositoryInterface
     */
    private $reservationSeatRepository;

    /**
     * @var BindTakenSeatsToShowService
     */
    private $service;

    public function setUp()
    {
        $this->reservationSeatRepository = $this->createMock(ReservationSeatRepositoryInterface::class);
        $this->service = new BindTakenSeatsToShowService($this->reservationSeatRepository);
    }

    /**
     * @test
     */
    public function shouldAddTakenSeatsToShow()
    {
        $reservationSeat = new ReservationSeat();
        $reservationSeat->setSeatRow(1);
        $reservationSeat->setSeatColumn(5);

        $this->reservationSeatRepository->expects($this->once())->method('findFewByShowIdAndTime')
            ->with(1)
            ->willReturn([$reservationSeat]);

        $expectedResults = new ArrayCollection([new TakenSeat(1, 5)]);

        $show = $this->createMock(Show::class);
        $show->expects($this->once())->method('getId')->willReturn(1);
        $show->expects($this->once())->method('setTakenSeats')->with($expectedResults);

        $this->service->bind($show, new \DateTimeImmutable());
    }
}
