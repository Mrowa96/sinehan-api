<?php

namespace App\Tests\Infrastructure\Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Exception\ReservationClientUpdateFailure;
use Domain\ReservationClient\Entity\ReservationClient;
use Domain\ReservationClient\Factory\ReservationClientFactoryInterface;
use Domain\ReservationClient\Repository\ReservationClientRepositoryInterface;
use Infrastructure\Domain\Reservation\Service\ReservationClientUpdaterService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * Class ReservationClientUpdaterServiceTest
 * @package App\Tests\Infrastructure\Domain\Reservation\Service
 */
class ReservationClientUpdaterServiceTest extends TestCase
{
    /**
     * @var MockObject|ReservationClientRepositoryInterface
     */
    private $reservationClientRepository;

    /**
     * @var MockObject|ReservationClientFactoryInterface
     */
    private $reservationClientFactory;

    /**
     * @var MockObject|LoggerInterface
     */
    private $logger;

    /**
     * @var MockObject|Reservation
     */
    private $reservation;

    /**
     * @var ReservationClientUpdaterService
     */
    private $service;

    public function setUp()
    {
        $this->reservationClientRepository = $this->createMock(ReservationClientRepositoryInterface::class);
        $this->reservationClientFactory = $this->createMock(ReservationClientFactoryInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);

        $this->service = new ReservationClientUpdaterService(
            $this->reservationClientRepository,
            $this->reservationClientFactory,
            $this->logger
        );

        $this->reservation = $this->createMock(Reservation::class);
        $this->reservation->expects($this->any())->method('getId')->willReturn('id');
    }

    /**
     * @test
     */
    public function shouldThrowExceptionIfSomeIndexIsNotAvailableOnData()
    {
        $this->expectException(ReservationClientUpdateFailure::class);

        $reservationClient = $this->createMock(ReservationClient::class);

        $this->reservation->expects($this->once())->method('hasClient')->willReturn(true);
        $this->reservation->expects($this->once())->method('getClient')->willReturn($reservationClient);

        $data = ['name' => 'name', 'surname' => 'surname', 'email' => 'email'];

        $this->service->update($this->reservation, $data);
    }

    /**
     * @test
     */
    public function shouldCreateNewEntityIfReservationHasNotGotClient()
    {
        $reservationClient = $this->createMock(ReservationClient::class);

        $this->reservation->expects($this->once())->method('hasClient')->willReturn(false);
        $this->reservationClientFactory->expects($this->once())->method('create')->willReturn($reservationClient);
        $this->reservationClientRepository->expects($this->once())->method('saveOne');

        $data = ['name' => 'name', 'surname' => 'surname', 'email' => 'email', 'phone' => 'phone'];

        $this->service->update($this->reservation, $data);
    }

    /**
     * @test
     */
    public function shouldUpdateExistingEntityIfReservationHasGotClient()
    {
        $reservationClient = $this->createMock(ReservationClient::class);
        $reservationClient->expects($this->once())->method('setEmail')->with('email');
        $reservationClient->expects($this->once())->method('setName')->with('name');
        $reservationClient->expects($this->once())->method('setSurname')->with('surname');
        $reservationClient->expects($this->once())->method('setPhone')->with('phone');

        $this->reservation->expects($this->once())->method('hasClient')->willReturn(true);
        $this->reservation->expects($this->once())->method('getClient')->willReturn($reservationClient);

        $this->reservationClientRepository->expects($this->once())->method('saveOne');

        $data = ['name' => 'name', 'surname' => 'surname', 'email' => 'email', 'phone' => 'phone'];

        $this->service->update($this->reservation, $data);
    }
}
