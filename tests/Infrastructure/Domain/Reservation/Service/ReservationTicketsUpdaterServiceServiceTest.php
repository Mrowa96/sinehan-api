<?php

namespace App\Tests\Infrastructure\Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\ReservationTicket\Entity\ReservationTicket;
use Domain\ReservationTicket\Factory\ReservationTicketFactoryInterface;
use Domain\ReservationTicket\Repository\ReservationTicketRepositoryInterface;
use Domain\Ticket\Entity\Ticket;
use Domain\Ticket\Repository\TicketRepositoryInterface;
use Infrastructure\Domain\Reservation\Service\ReservationTicketsUpdaterService;
use Infrastructure\Domain\Reservation\Service\ReservationUpdaterService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * Class ReservationTicketsUpdaterServiceServiceTest
 * @package App\Tests\Infrastructure\Domain\Reservation\Service
 */
class ReservationTicketsUpdaterServiceServiceTest extends TestCase
{
    /**
     * @var ReservationTicketRepositoryInterface|MockObject
     */
    private $reservationTicketRepository;

    /**
     * @var ReservationTicketFactoryInterface|MockObject
     */
    private $reservationTicketFactory;

    /**
     * @var TicketRepositoryInterface|MockObject
     */
    private $ticketRepository;

    /**
     * @var LoggerInterface|MockObject
     */
    private $logger;

    /**
     * @var MockObject|Reservation
     */
    private $reservation;

    /**
     * @var ReservationTicketsUpdaterService
     */
    private $service;

    public function setUp()
    {
        $this->reservationTicketRepository = $this->createMock(ReservationTicketRepositoryInterface::class);
        $this->reservationTicketFactory = $this->createMock(ReservationTicketFactoryInterface::class);
        $this->ticketRepository = $this->createMock(TicketRepositoryInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);

        $this->reservation = $this->createMock(Reservation::class);
        $this->reservation->expects($this->any())->method('getId')->willReturn('some_id');

        $this->service = new ReservationTicketsUpdaterService(
            $this->reservationTicketRepository,
            $this->reservationTicketFactory,
            $this->ticketRepository,
            $this->logger
        );
    }

    /**
     * @test
     */
    public function shouldAddOneTicketAndNoTicketsExists()
    {
        $newReservationTicket = $this->createMock(ReservationTicket::class);
        $ticket = $this->createMock(Ticket::class);
        $ticket->expects($this->once())->method('getPrice')->willReturn(29.99);

        $this->reservationTicketRepository->expects($this->once())
            ->method('findFewByReservationId')->willReturn([]);
        $this->ticketRepository->expects($this->once())
            ->method('findOne')->with(1)->willReturn($ticket);
        $this->reservationTicketFactory->expects($this->once())
            ->method('create')->willReturn($newReservationTicket);
        $this->reservationTicketRepository->expects($this->once())
            ->method('saveOne')->with($newReservationTicket);
        $this->reservationTicketRepository->expects($this->never())
            ->method('removeFew');

        $this->service->update($this->reservation, [['id' => 1, 'quantity' => 2]]);
    }

    /**
     * @test
     */
    public function shouldAddFewTicketAndNoTicketsExists()
    {
        $data = [
            ['id' => 1, 'quantity' => 2],
            ['id' => 2, 'quantity' => 3]
        ];
        $newReservationTicket = $this->createMock(ReservationTicket::class);

        $ticket = $this->createMock(Ticket::class);
        $ticket->expects($this->once())->method('getPrice')->willReturn(29.99);
        $ticket2 = $this->createMock(Ticket::class);
        $ticket2->expects($this->once())->method('getPrice')->willReturn(19.99);

        $this->reservationTicketRepository->expects($this->once())
            ->method('findFewByReservationId')->willReturn([]);

        $this->ticketRepository->expects($this->at(0))
            ->method('findOne')->with(1)->willReturn($ticket);

        $this->ticketRepository->expects($this->at(1))
            ->method('findOne')->with(2)->willReturn($ticket2);

        $this->reservationTicketRepository->expects($this->exactly(2))
            ->method('saveOne');

        $this->reservationTicketFactory->expects($this->exactly(2))
            ->method('create')->willReturn($newReservationTicket);

        $this->reservationTicketRepository->expects($this->never())
            ->method('removeFew');

        $this->service->update($this->reservation, $data);
    }
}
