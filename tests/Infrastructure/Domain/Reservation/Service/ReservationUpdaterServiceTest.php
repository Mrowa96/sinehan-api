<?php

namespace App\Tests\Infrastructure\Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Enum\ReservationStatus;
use Domain\Reservation\Exception\ReservationUpdateFailure;
use Domain\Reservation\Repository\ReservationRepositoryInterface;
use Domain\Reservation\Service\ReservationClientUpdaterServiceInterface;
use Domain\Reservation\Service\ReservationSeatsUpdaterServiceInterface;
use Domain\Reservation\Service\ReservationTicketsUpdaterServiceInterface;
use Domain\ReservationClient\Repository\ReservationClientRepositoryInterface;
use Domain\ReservationSeat\Entity\ReservationSeat;
use Domain\ReservationSeat\Repository\ReservationSeatRepositoryInterface;
use Domain\ReservationTicket\Entity\ReservationTicket;
use Domain\ReservationTicket\Repository\ReservationTicketRepositoryInterface;
use Infrastructure\Domain\Reservation\Service\ReservationUpdaterService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class ReservationUpdaterServiceTest
 * @package App\Tests\Infrastructure\Domain\Reservation\Service
 */
class ReservationUpdaterServiceTest extends TestCase
{
    /**
     * @var MockObject|ReservationTicketsUpdaterServiceInterface
     */
    private $ticketsUpdater;

    /**
     * @var MockObject|ReservationSeatsUpdaterServiceInterface
     */
    private $seatsUpdater;

    /**
     * @var MockObject|ReservationClientUpdaterServiceInterface
     */
    private $clientUpdater;

    /**
     * @var MockObject|ReservationRepositoryInterface
     */
    private $reservationRepository;

    /**
     * @var MockObject|ReservationSeatRepositoryInterface
     */
    private $reservationSeatRepository;

    /**
     * @var MockObject|ReservationTicketRepositoryInterface
     */
    private $reservationTicketRepository;

    /**
     * @var MockObject|ReservationClientRepositoryInterface
     */
    private $reservationClientRepository;

    /**
     * @var MockObject|Reservation
     */
    private $reservation;

    /**
     * @var ReservationUpdaterService
     */
    private $service;

    public function setUp()
    {
        $this->ticketsUpdater = $this->createMock(ReservationTicketsUpdaterServiceInterface::class);
        $this->seatsUpdater = $this->createMock(ReservationSeatsUpdaterServiceInterface::class);
        $this->clientUpdater = $this->createMock(ReservationClientUpdaterServiceInterface::class);
        $this->reservationRepository = $this->createMock(ReservationRepositoryInterface::class);
        $this->reservationSeatRepository = $this->createMock(ReservationSeatRepositoryInterface::class);
        $this->reservationTicketRepository = $this->createMock(ReservationTicketRepositoryInterface::class);
        $this->reservationClientRepository = $this->createMock(ReservationClientRepositoryInterface::class);

        $this->reservation = $this->createMock(Reservation::class);
        $this->reservation->expects($this->any())->method('getId')->willReturn('some_id');

        $this->service = new ReservationUpdaterService(
            $this->ticketsUpdater,
            $this->seatsUpdater,
            $this->clientUpdater,
            $this->reservationRepository,
            $this->reservationSeatRepository,
            $this->reservationTicketRepository,
            $this->reservationClientRepository
        );
    }

    /**
     * @test
     */
    public function shouldInvokeUpdateMethodOnTicketsUpdater()
    {
        $this->ticketsUpdater->expects($this->exactly(2))->method('update');
        $this->reservationSeatRepository->expects($this->exactly(2))->method('removeFewByReservationId');

        $this->service->update($this->reservation, ['tickets' => ['a' => 'b']]);
        $this->service->update($this->reservation, ['tickets' => []]);
    }

    /**
     * @test
     */
    public function shouldNotInvokeUpdateMethodOnTicketsUpdater()
    {
        $this->ticketsUpdater->expects($this->never())->method('update');

        $this->service->update($this->reservation, []);
    }

    /**
     * @test
     */
    public function shouldInvokeUpdateMethodOnSeatsUpdater()
    {
        $reservationTicket = $this->createMock(ReservationTicket::class);
        $reservationTicket->expects($this->once())->method('getQuantity')->willReturn(2);

        $this->reservationTicketRepository->expects($this->once())->method('findFewByReservationId')
            ->willReturn([$reservationTicket]);
        $this->seatsUpdater->expects($this->once())->method('update');

        $this->service->update($this->reservation, ['seats' => ['a' => 'b']]);
    }

    /**
     * @test
     */
    public function shouldThrowExceptionIfThereIsLessTicketsThanSeats()
    {
        $this->expectException(ReservationUpdateFailure::class);

        $reservationTicket = $this->createMock(ReservationTicket::class);
        $reservationTicket->expects($this->once())->method('getQuantity')->willReturn(1);

        $this->reservationTicketRepository->expects($this->once())->method('findFewByReservationId')
            ->willReturn([$reservationTicket]);

        $this->service->update($this->reservation, ['seats' => ['a' => 'b', 'b' => 'c']]);
    }

    /**
     * @test
     */
    public function shouldNotInvokeUpdateMethodOnSeatsUpdater()
    {
        $this->seatsUpdater->expects($this->never())->method('update');

        $this->service->update($this->reservation, []);
    }

    /**
     * @test
     */
    public function shouldInvokeUpdateMethodOnClientUpdater()
    {
        $this->reservation->expects($this->any())->method('isDataProcessingAccepted')->willReturn(true);
        $this->clientUpdater->expects($this->exactly(2))->method('update');

        $this->service->update($this->reservation, ['clientData' => ['name' => 'name']]);
        $this->service->update($this->reservation, ['clientData' => []]);
    }

    /**
     * @test
     */
    public function shouldThrowExceptionIfDataProcessingIsNotAcceptedDuringClientDataUpdate()
    {
        $this->expectException(ReservationUpdateFailure::class);

        $this->reservation->expects($this->once())->method('isDataProcessingAccepted')->willReturn(false);

        $this->service->update($this->reservation, ['clientData' => ['name' => 'name']]);
    }

    /**
     * @test
     */
    public function shouldNotInvokeUpdateMethodOnClientUpdater()
    {
        $this->clientUpdater->expects($this->never())->method('update');

        $this->service->update($this->reservation, []);
    }

    /**
     * @test
     */
    public function shouldClearClientDataIfDataProcessingIsNotAcceptedDuringAgreementsUpdate()
    {
        $this->reservationClientRepository->expects($this->once())->method('clearOneByReservationId');
        $this->reservation->expects($this->once())->method('setDataProcessingAccepted')->with(false);

        $this->service->update($this->reservation, ['agreements' => ['dataProcessing' => false]]);
    }

    /**
     * @test
     */
    public function shouldNotClearClientDataIfDataProcessingIsAcceptedDuringAgreementsUpdate()
    {
        $this->reservation->expects($this->once())->method('setDataProcessingAccepted')->with(true);

        $this->service->update($this->reservation, ['agreements' => ['dataProcessing' => true]]);
    }

    /**
     * @test
     */
    public function shouldInvokeSaveOneMethodOnReservationRepository()
    {
        $this->reservationRepository->expects($this->once())->method('saveOne');

        $this->service->update($this->reservation, ['agreements' => ['dataProcessing' => true]]);
    }

    /**
     * @test
     */
    public function shouldSetCompleteStatus()
    {
        $this->reservation->expects($this->once())->method('hasTickets')->willReturn(true);
        $this->reservation->expects($this->once())->method('hasSeats')->willReturn(true);
        $this->reservation->expects($this->once())->method('isDataProcessingAccepted')->willReturn(true);
        $this->reservation->expects($this->once())->method('hasClient')->willReturn(true);
        $this->reservation->expects($this->once())->method('hasClientWithInformation')->willReturn(true);

        $this->reservation->expects($this->once())->method('setStatus')->with(ReservationStatus::COMPLETE);
        $this->reservationRepository->expects($this->once())->method('saveOne');

        $this->service->update($this->reservation, ['complete' => true]);
    }

    /**
     * @test
     */
    public function shouldThrowExceptionDuringCompleteWhenReservationHasNotGotTickets()
    {
        $this->expectException(ReservationUpdateFailure::class);

        $this->reservation->expects($this->once())->method('hasTickets')->willReturn(false);

        $this->service->update($this->reservation, ['complete' => true]);
    }

    /**
     * @test
     */
    public function shouldThrowExceptionDuringCompleteWhenReservationHasNotGotSeats()
    {
        $this->expectException(ReservationUpdateFailure::class);

        $this->reservation->expects($this->once())->method('hasTickets')->willReturn(true);
        $this->reservation->expects($this->once())->method('hasSeats')->willReturn(false);

        $this->service->update($this->reservation, ['complete' => true]);
    }

    /**
     * @test
     */
    public function shouldThrowExceptionDuringCompleteWhenReservationDataProcessingIsNotAccepted()
    {
        $this->expectException(ReservationUpdateFailure::class);

        $this->reservation->expects($this->once())->method('hasTickets')->willReturn(true);
        $this->reservation->expects($this->once())->method('hasSeats')->willReturn(true);
        $this->reservation->expects($this->once())->method('isDataProcessingAccepted')->willReturn(false);

        $this->service->update($this->reservation, ['complete' => true]);
    }

    /**
     * @test
     */
    public function shouldThrowExceptionDuringCompleteWhenReservationHasNotGotClient()
    {
        $this->expectException(ReservationUpdateFailure::class);

        $this->reservation->expects($this->once())->method('hasTickets')->willReturn(true);
        $this->reservation->expects($this->once())->method('hasSeats')->willReturn(true);
        $this->reservation->expects($this->once())->method('isDataProcessingAccepted')->willReturn(true);
        $this->reservation->expects($this->once())->method('hasClient')->willReturn(false);

        $this->service->update($this->reservation, ['complete' => true]);
    }


    /**
     * @test
     */
    public function shouldThrowExceptionDuringCompleteWhenReservationHasNotGotClientWithCompleteInformation()
    {
        $this->expectException(ReservationUpdateFailure::class);

        $this->reservation->expects($this->once())->method('hasTickets')->willReturn(true);
        $this->reservation->expects($this->once())->method('hasSeats')->willReturn(true);
        $this->reservation->expects($this->once())->method('isDataProcessingAccepted')->willReturn(true);
        $this->reservation->expects($this->once())->method('hasClient')->willReturn(true);
        $this->reservation->expects($this->once())->method('hasClientWithInformation')->willReturn(false);

        $this->service->update($this->reservation, ['complete' => true]);
    }

    /**
     * @test
     */
    public function shouldThrowExceptionIfReservationIsComplete()
    {
        $this->expectException(ReservationUpdateFailure::class);

        $this->reservation->expects($this->once())->method('getStatus')->willReturn(ReservationStatus::COMPLETE);

        $this->service->update($this->reservation, ['complete' => true]);
    }
}
