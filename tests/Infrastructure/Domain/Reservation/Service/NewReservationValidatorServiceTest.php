<?php

namespace App\Tests\Infrastructure\Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Repository\ReservationRepositoryInterface;
use Domain\Show\Entity\Show;
use Domain\Show\Repository\ShowRepositoryInterface;
use Infrastructure\Domain\Reservation\Service\NewReservationValidatorService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class NewReservationValidatorServiceTest
 * @package App\Tests\Infrastructure\Domain\Reservation\Service
 */
class NewReservationValidatorServiceTest extends TestCase
{
    /**
     * @var MockObject|ShowRepositoryInterface
     */
    private $showRepository;

    /**
     * @var MockObject|ReservationRepositoryInterface
     */
    private $reservationRepository;

    /**
     * @var NewReservationValidatorService
     */
    private $service;

    public function setUp()
    {
        $this->showRepository = $this->createMock(ShowRepositoryInterface::class);
        $this->reservationRepository = $this->createMock(ReservationRepositoryInterface::class);

        $this->service = new NewReservationValidatorService($this->showRepository, $this->reservationRepository);
    }

    /**
     * @test
     */
    public function shouldPassWithoutErrors()
    {
        $show = $this->createMock(Show::class);
        $reservationMock = $this->createMock(Reservation::class);

        $show->expects($this->once())->method('getStartTimes')->willReturn(['11:00', '20:00']);
        $show->expects($this->once())->method('getDate')->willReturn((new \DateTime('2018-11-11')));

        $this->showRepository->expects($this->once())->method('findOne')->with(1)->willReturn($show);
        $this->reservationRepository->expects($this->once())->method('findFewByIpAddressAndCreateDate')->willReturn([
            $reservationMock,
            $reservationMock
        ]);

        $data = [
            'ipAddress' => '127.0.0.1',
            'showId' => 1,
            'time' => '11:00',
            'currentDate' => new \DateTime('2018-11-10 08:00:00')
        ];

        $this->assertEquals($this->service->validate($data), true);
        $this->assertEquals($this->service->hasErrors(), false);
    }

    /**
     * @test
     */
    public function shouldFailIfShowDoesNotExist()
    {
        $this->showRepository->expects($this->once())->method('findOne')->with(1)->willReturn(null);

        $data = [
            'showId' => 1,
        ];

        $this->assertEquals($this->service->validate($data), false);
    }

    /**
     * @test
     */
    public function shouldFailIfShowHasNotGotGivenStartTime()
    {
        $show = $this->createMock(Show::class);
        $show->expects($this->once())->method('getStartTimes')->willReturn(['11:00', '20:00']);
        $show->expects($this->once())->method('getDate')->willReturn((new \DateTime('2018-11-11')));

        $this->showRepository->expects($this->once())->method('findOne')->with(1)->willReturn($show);

        $data = [
            'ipAddress' => '127.0.0.1',
            'showId' => 1,
            'time' => '12:00',
            'currentDate' => new \DateTime('2018-11-10 08:00:00')
        ];

        $this->assertEquals($this->service->validate($data), false);
    }

    /**
     * @test
     */
    public function shouldFailIfShowDateIsFromPast()
    {
        $show = $this->createMock(Show::class);
        $show->expects($this->once())->method('getDate')->willReturn((new \DateTime('2018-11-11')));
        $show->expects($this->once())->method('getStartTimes')->willReturn(['11:00', '20:00']);

        $this->showRepository->expects($this->once())->method('findOne')->with(1)->willReturn($show);

        $data = [
            'ipAddress' => '127.0.0.1',
            'showId' => 1,
            'time' => '11:00',
            'currentDate' => new \DateTime('2018-11-11 12:00:00')
        ];

        $this->assertEquals($this->service->validate($data), false);
    }

    /**
     * @test
     */
    public function shouldFailIfIsLessThanThirtyMinutesBeforeShowStart()
    {
        $show = $this->createMock(Show::class);
        $show->expects($this->once())->method('getDate')->willReturn((new \DateTime('2018-11-11')));
        $show->expects($this->once())->method('getStartTimes')->willReturn(['11:00', '20:00']);

        $this->showRepository->expects($this->once())->method('findOne')->with(1)->willReturn($show);

        $data = [
            'ipAddress' => '127.0.0.1',
            'showId' => 1,
            'time' => '11:00',
            'currentDate' => new \DateTime('2018-11-11 10:45:00')
        ];

        $this->assertEquals($this->service->validate($data), false);
    }

    /**
     * @test
     */
    public function shouldFailIfThereAreMoreThanThreeReservationsFromIpCurrentDay()
    {
        $show = $this->createMock(Show::class);
        $reservationMock = $this->createMock(Reservation::class);

        $show->expects($this->once())->method('getDate')->willReturn((new \DateTime('2018-11-11')));
        $show->expects($this->once())->method('getStartTimes')->willReturn(['11:00', '20:00']);

        $this->showRepository->expects($this->once())->method('findOne')->with(1)->willReturn($show);
        $this->reservationRepository->expects($this->once())->method('findFewByIpAddressAndCreateDate')->willReturn([
            $reservationMock,
            $reservationMock,
            $reservationMock,
            $reservationMock
        ]);

        $data = [
            'ipAddress' => '127.0.0.1',
            'showId' => 1,
            'time' => '11:00',
            'currentDate' => new \DateTime('2018-11-10 08:00:00')
        ];

        $this->assertEquals($this->service->validate($data), false);
    }
}
