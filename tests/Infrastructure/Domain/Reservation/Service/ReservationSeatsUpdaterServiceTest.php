<?php

namespace App\Tests\Infrastructure\Domain\Reservation\Service;

use Domain\Reservation\Entity\Reservation;
use Domain\Reservation\Exception\ReservationSeatsUpdateFailure;
use Domain\ReservationSeat\Entity\ReservationSeat;
use Domain\ReservationSeat\Factory\ReservationSeatFactoryInterface;
use Domain\ReservationSeat\Repository\ReservationSeatRepositoryInterface;
use Domain\Show\Entity\Show;
use Infrastructure\Domain\Reservation\Service\ReservationSeatsUpdaterService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * Class ReservationSeatsUpdaterServiceTest
 * @package App\Tests\Infrastructure\Domain\Reservation\Service
 */
class ReservationSeatsUpdaterServiceTest extends TestCase
{
    /**
     * @var MockObject|ReservationSeatRepositoryInterface
     */
    private $reservationSeatRepository;

    /**
     * @var MockObject|ReservationSeatFactoryInterface
     */
    private $reservationSeatFactory;

    /**
     * @var MockObject|LoggerInterface
     */
    private $logger;

    /**
     * @var ReservationSeatsUpdaterService
     */
    private $service;

    private $reservation;

    public function setUp()
    {
        $this->reservationSeatRepository = $this->createMock(ReservationSeatRepositoryInterface::class);
        $this->reservationSeatFactory = $this->createMock(ReservationSeatFactoryInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);

        $this->service = new ReservationSeatsUpdaterService(
            $this->reservationSeatRepository,
            $this->reservationSeatFactory,
            $this->logger
        );

        $show = $this->createMock(Show::class);
        $show->expects($this->any())->method('getId')->willReturn(1);

        $this->reservation = $this->createMock(Reservation::class);
        $this->reservation->expects($this->any())->method('getId')->willReturn('id');
        $this->reservation->expects($this->any())->method('getShow')->willReturn($show);
        $this->reservation->expects($this->any())->method('getTime')->willReturn(new \DateTimeImmutable());
    }

    /**
     * @test
     */
    public function shouldThrowExceptionIfSeatIsTaken()
    {
        $this->expectException(ReservationSeatsUpdateFailure::class);

        $data = [
            ['row' => 1, 'column' => 1],
            ['row' => 1, 'column' => 2],
        ];

        $this->reservationSeatRepository->expects($this->exactly(2))->method('findOneByParameters')
            ->willReturn(null, new ReservationSeat());

        $this->service->update($this->reservation, $data);
    }
}
