<?php

namespace App\Tests\Infrastructure\Domain\Reservation\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Reservation\Enum\ReservationStatus;
use Domain\Reservation\Exception\CreateReservationException;
use Domain\Show\Entity\Show;
use Infrastructure\Domain\Reservation\Factory\ReservationFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class ReservationFactoryTest
 * @package App\Tests\Infrastructure\Domain\Reservation\Factory
 */
class ReservationFactoryTest extends TestCase
{
    public function testCreateMethodWithPositiveResult()
    {
        $show = $this->createMock(Show::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->once())->method('getReference')->willReturn($show);

        $data = [
            'ipAddress' => '127.0.0.1',
            'showId' => 1,
            'time' => '20:00'
        ];

        $factory = new ReservationFactory($entityManager);

        $reservation = $factory->create($data);

        $this->assertEquals($reservation->getId(), null);
        $this->assertEquals($reservation->getShow(), $show);
        $this->assertEquals($reservation->getTime()->format('H:i:s'), '20:00:00');
        $this->assertEquals($reservation->getStatus(), ReservationStatus::IN_PROGRESS);
        $this->assertEquals($reservation->getIpAddress(), '127.0.0.1');
        $this->assertInstanceOf(\DateTimeImmutable::class, $reservation->getCreateDate());
        $this->assertInstanceOf(\DateTimeImmutable::class, $reservation->getUpdateDate());
    }

    public function testCreateMethodWithMissingShowId()
    {
        $this->expectException(CreateReservationException::class);

        $entityManager = $this->createMock(EntityManagerInterface::class);

        $data = [
            'time' => '20:00'
        ];

        $factory = new ReservationFactory($entityManager);

        $factory->create($data);
    }

    public function testCreateMethodWithMissingTime()
    {
        $this->expectException(CreateReservationException::class);

        $show = $this->createMock(Show::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->once())->method('getReference')->willReturn($show);

        $data = [
            'showId' => '1'
        ];

        $factory = new ReservationFactory($entityManager);

        $factory->create($data);
    }

    public function testCreateMethodWithInvalidTime()
    {
        $this->expectException(CreateReservationException::class);

        $show = $this->createMock(Show::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->once())->method('getReference')->willReturn($show);

        $data = [
            'showId' => '1',
            'time' => 'x y z'
        ];

        $factory = new ReservationFactory($entityManager);

        $factory->create($data);
    }
}
